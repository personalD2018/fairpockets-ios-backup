//
//  SaveReminderDataClass.swift
//
//  Created by AB on 09/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class SaveReminderDataClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let message = "message"
    static let code = "code"
    static let pdfLink = "pdf_link"
  }

  // MARK: Properties
  public var message: String?
  public var code: Int?
  public var pdfLink: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    message <- map[SerializationKeys.message]
    code <- map[SerializationKeys.code]
    pdfLink <- map[SerializationKeys.pdfLink]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = pdfLink { dictionary[SerializationKeys.pdfLink] = value }
    return dictionary
  }

}
