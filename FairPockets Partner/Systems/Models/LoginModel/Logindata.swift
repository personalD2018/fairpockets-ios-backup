//
//  Logindata.swift
//
//  Created by AB on 03/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Logindata: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userType = "user_type"
    static let userId = "user_id"
    static let builder_id = "builder_id"
  }

  // MARK: Properties
  public var userType: String?
  public var userId: String?
  public var builder_id: String?
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    userType <- map[SerializationKeys.userType]
    userId <- map[SerializationKeys.userId]
    builder_id <- map[SerializationKeys.builder_id]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userType { dictionary[SerializationKeys.userType] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = builder_id { dictionary[SerializationKeys.builder_id] = value }
    return dictionary
  }

}
