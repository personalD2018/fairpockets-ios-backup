//
//  DiscountArray.swift
//
//  Created by AB on 09/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class DiscountArray: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let basePriceBefore = "base_price_before"
    static let gstInputCreditAmount = "gstInputCreditAmount"
    static let basePriceUpdated = "base_price_updated"
  }

  // MARK: Properties
  public var basePriceBefore: Int?
  public var gstInputCreditAmount: Int?
  public var basePriceUpdated: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    basePriceBefore <- map[SerializationKeys.basePriceBefore]
    gstInputCreditAmount <- map[SerializationKeys.gstInputCreditAmount]
    basePriceUpdated <- map[SerializationKeys.basePriceUpdated]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = basePriceBefore { dictionary[SerializationKeys.basePriceBefore] = value }
    if let value = gstInputCreditAmount { dictionary[SerializationKeys.gstInputCreditAmount] = value }
    if let value = basePriceUpdated { dictionary[SerializationKeys.basePriceUpdated] = value }
    return dictionary
  }

}
