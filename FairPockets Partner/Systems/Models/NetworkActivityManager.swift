//
//  NetworkActivityManager.swift
//  WebServicesDemo
//
//  Created by AB on 7/27/16.
//  Copyright © 2018 abhay. All rights reserved.
//

import Foundation
import UIKit

class NetworkActivityManager {
    
    //  MARK: - Singleton
    static let sharedInstance = NetworkActivityManager()
    
    
    //  MARK: - Properties
    var busy = false {
        didSet {
            reactToNetwokManagerState()
        }
    }
    
    
    /**
     Handles UI Interactions.
     If Activity is in process, UI is blocked until completion
     Called when property observer identifies change in state "true / false" for "busy" variable
     */
    private func reactToNetwokManagerState() {
        //NOTE: You can customize this function to make your app behave as you wish while network activity is in progress.
        
        //NOTE: This is temporary demo solution. You should not use this in production level application as posibilities of parallel networkism is high and any instance can change this states. So other services and UI might affect.
        
        if busy {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            UIApplication.shared.beginIgnoringInteractionEvents()
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    /*
    class func json_key_check_text(dict: NSDictionary, key : String) -> String
    {
        var str_value = "*"
        //print(dict)
        
        if (dict[key] != nil) {
            
            str_value = String(format: "%@", "\(dict[key]!)")
            if(str_value=="<null>")
            {
                str_value = String(format: "")
            }
        }
        
        return str_value
    }
    
    class func json_key_check_array(dict: NSDictionary, key : String) -> Bool
    {
        
        //let arr = ["abc.png"]
        //print("arr\(arr)")
        //print(dict)
        if (dict[key] != nil)
        {
            if (dict[key]!.isKindOfClass(NSArray))
            {
                return true;
            }
        }
        
        return false
    }
    
    
    
    class func json_key_check_dictionary(dict: NSDictionary, key : String) -> Bool
    {
        if (dict[key] != nil)
        {
            if (dict[key]!.isKindOfClass(NSDictionary))
            {
                return true;
            }
        }
        
        return false
    }
    */
    
    
}
