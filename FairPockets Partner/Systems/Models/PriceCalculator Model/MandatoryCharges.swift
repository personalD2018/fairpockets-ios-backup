//
//  MandatoryCharges.swift
//
//  Created by AB on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class MandatoryCharges: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let chargeValue = "charge_value"
    static let unitType = "unit_type"
    static let chargeTitle = "charge_title"
  }

  // MARK: Properties
  public var chargeValue: String?
  public var unitType: String?
  public var chargeTitle: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    chargeValue <- map[SerializationKeys.chargeValue]
    unitType <- map[SerializationKeys.unitType]
    chargeTitle <- map[SerializationKeys.chargeTitle]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = chargeValue { dictionary[SerializationKeys.chargeValue] = value }
    if let value = unitType { dictionary[SerializationKeys.unitType] = value }
    if let value = chargeTitle { dictionary[SerializationKeys.chargeTitle] = value }
    return dictionary
  }

}
