//
//  PropertyDetail.swift
//
//  Created by AB on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class PropertyDetail: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let propertyArea = "property_area"
    static let registrationCharges = "registration_charges"
    static let stampDuty = "stamp_duty"
    static let gstOnOthers = "gst_on_others"
    static let gstOnBase = "gst_on_base"
    static let baseRate = "base_rate"
    static let propertyPossessionChargeTotal = "property_possession_charge_total"
    static let propertyTerraceValueTotal = "property_terrace_value_total"
    static let propertyLawnValueTotal = "property_lawn_value_total"
  }

  // MARK: Properties
  public var propertyArea: String?
  public var registrationCharges: String?
  public var stampDuty: String?
  public var gstOnOthers: String?
  public var gstOnBase: String?
  public var baseRate: String?
  public var propertyPossessionChargeTotal: String?
  public var propertyTerraceValueTotal: Int?
  public var propertyLawnValueTotal: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    propertyArea <- map[SerializationKeys.propertyArea]
    registrationCharges <- map[SerializationKeys.registrationCharges]
    stampDuty <- map[SerializationKeys.stampDuty]
    gstOnOthers <- map[SerializationKeys.gstOnOthers]
    gstOnBase <- map[SerializationKeys.gstOnBase]
    baseRate <- map[SerializationKeys.baseRate]
    propertyPossessionChargeTotal <- map[SerializationKeys.propertyPossessionChargeTotal]
    propertyTerraceValueTotal <- map[SerializationKeys.propertyTerraceValueTotal]
    propertyLawnValueTotal <- map[SerializationKeys.propertyLawnValueTotal]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = propertyArea { dictionary[SerializationKeys.propertyArea] = value }
    if let value = registrationCharges { dictionary[SerializationKeys.registrationCharges] = value }
    if let value = stampDuty { dictionary[SerializationKeys.stampDuty] = value }
    if let value = gstOnOthers { dictionary[SerializationKeys.gstOnOthers] = value }
    if let value = gstOnBase { dictionary[SerializationKeys.gstOnBase] = value }
    if let value = baseRate { dictionary[SerializationKeys.baseRate] = value }
    if let value = propertyPossessionChargeTotal { dictionary[SerializationKeys.propertyPossessionChargeTotal] = value }
    if let value = propertyTerraceValueTotal { dictionary[SerializationKeys.propertyTerraceValueTotal] = value }
    if let value = propertyLawnValueTotal { dictionary[SerializationKeys.propertyLawnValueTotal] = value }
    return dictionary
  }

}
