//
//  ChartData.swift
//
//  Created by AB on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ChartData: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let stampPlusRegistration = "stamp_plus_registration"
    static let additionalCharges = "additional_charges"
    static let gstTotal = "gst_total"
    static let baseCharges = "base_charges"
   // static let baseCharges = "base_charges"

  }

  // MARK: Properties
  public var stampPlusRegistration: Int?
  public var additionalCharges: Int?
  public var gstTotal: Int?
  public var baseCharges: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    stampPlusRegistration <- map[SerializationKeys.stampPlusRegistration]
    additionalCharges <- map[SerializationKeys.additionalCharges]
    gstTotal <- map[SerializationKeys.gstTotal]
    baseCharges <- map[SerializationKeys.baseCharges]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = stampPlusRegistration { dictionary[SerializationKeys.stampPlusRegistration] = value }
    if let value = additionalCharges { dictionary[SerializationKeys.additionalCharges] = value }
    if let value = gstTotal { dictionary[SerializationKeys.gstTotal] = value }
    if let value = baseCharges { dictionary[SerializationKeys.baseCharges] = value }
    return dictionary
  }

}
