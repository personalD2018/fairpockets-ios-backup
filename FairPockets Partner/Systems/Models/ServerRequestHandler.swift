//
//  ServerRequestHandler.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 15/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
class ServerRequestHandler: NSObject {
static let sharedappController = ServerRequestHandler()
    
    var builderMsgCount:Int! = Int()
    var myClientCount:Int! = Int()
    var priceListCount:Int! = Int()
    var brochureCount:Int! = Int()
    var defaultHomeLoader:Int! = Int()
    //getMessage Notifications
    func getMyClientNotification(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostMyClientNotification, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    func getMessageNotification(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostMessageNotification, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.getMessageNotification",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    func getMessageNotificationRead(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostMessageNotificationRead, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.getMessageNotificationRead",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    func getInvProjectList(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        
       // SVProgressHUD.show()
      //  let Url = BASE_URL + "account/doUserLogin/"
      //  let params: Parameters = ["user_type": "sales", "user_id":UserId, "builder_id":Builder_id]
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostInvProjectListURl, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString

                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                  //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    //
    
    
    func getPriceList(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostPriceListssURl, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    //
    //new Scope
    func getUserAccessAfterEmailLogin(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostUserExistingStatusURl, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    func getBrochureList(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostBrocureListsURl, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    //getimages
    func getimagesList(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostImagesListsURl, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    //getMessage
    func getBuilderMessageList(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostBuilderMessageListsURl, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    // Row Read
    func getBuilderMessageRowRead(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostMessageRowRead, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    //aBasePostMyClientRowRead
    func getBuilderMyClientRowRead(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostMyClientRowRead, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    //get images by projectID
    func getimagesbyprojectID(para:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostprojectImagesURl, method: .post, parameters: para, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    
    func getInvBuilderList(param:Parameters ,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        
        // SVProgressHUD.show()
        //  let Url = BASE_URL + "account/doUserLogin/"
      //  let params: Parameters = ["user_type": "sales", "user_id":UserId, "builder_id":Builder_id]
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostInvBuilderListURl, method: .post, parameters: param, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    func getInvTowerList(param:Parameters,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        
        // SVProgressHUD.show()
        //  let Url = BASE_URL + "account/doUserLogin/"
       // let params: Parameters = ["user_type": "sales", "user_id":UserId, "builder_id":Builder_id]
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostInvTowerListURl, method: .post, parameters: param, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Projectresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    func getInvWingList(param:Parameters,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        
        // SVProgressHUD.show()
        //  let Url = BASE_URL + "account/doUserLogin/"
        // let params: Parameters = ["user_type": "sales", "user_id":UserId, "builder_id":Builder_id]
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostInvWingListURl, method: .post, parameters: param, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Wingresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    //aBasePostInvDetailsURl
    
    
    func getInvDetails(param:Parameters,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
  
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostInvDetailsURl, method: .post, parameters: param, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Detailsresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    func getInvStatus(param:Parameters,withHandler handler: @escaping (_ status: Int, _ data: NSObject) -> Void) {
        
        var dict=NSDictionary()
        
        Alamofire.request(aBasePostInvStatusURl, method: .post, parameters: param, encoding: URLEncoding.httpBody)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    //    SVProgressHUD.dismiss()
                    print("response.Detailsresult",response.result.value)
                    dict = response.result.value as! NSDictionary
                    let status = dict.value(forKey: "message") as? NSString
                    
                    if (status == "Success")
                    {
                        handler(1 , dict)
                    }
                    else
                    {
                        handler(0 , dict)
                    }
                case .failure(let error):
                    print(error)
                    //  SVProgressHUD.dismiss()
                    handler(0 , dict)
                    
                }
        }
    }
    
    func getDetail(url:String, param:Parameters,withHandler handler: @escaping (_ status: Int, _ data: Any?) -> Void) {
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: authHeader())
            .responseJSON { response in
                switch response.result {
                case .success:
                    print("response.Detailsresult \(response.result.value!)")
                    if let dict = response.result.value, let status = (dict as AnyObject).value(forKey: "message") as? String  {
                        if (status.caseInsensitiveCompare("success") == ComparisonResult.orderedSame ) {
                            handler(1 , dict)
                        } else {
                            handler(0 , dict)
                        }
                    }
                    
                case .failure(let error):
                    print(error)
                    handler(0 , nil)
            }
        }
    }
    
    private func authHeader() -> [String : String] {
        return [
            // "X-Authorization": "\(kBaseApiKey)",
            //"Content-Type": "application/x-www-form-urlencoded",
            "Content-Type": "application/x-www-form-urlencoded",
            //"Accept-Language":"\(language)"
        ]
    }
}
