//
//  MyClientListClass.swift
//
//  Created by AB on 04/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class MyClientListClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let clientdata = "data"
    static let code = "code"
    static let message = "message"
  }

  // MARK: Properties
  public var clientdata: [Clientdata]?
  public var code: Int?
  public var message: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    clientdata <- map[SerializationKeys.clientdata]
    code <- map[SerializationKeys.code]
    message <- map[SerializationKeys.message]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = clientdata { dictionary[SerializationKeys.clientdata] = value.map { $0.dictionaryRepresentation() } }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    return dictionary
  }

}
