//
//  CommonDateTimeVC.swift
//  HappyLiving
//
//  Created by Haresh on 11/23/17.
//  Copyright © 2017 Haresh. All rights reserved.
//

import UIKit


import UIKit

public protocol DateTimePickerDelegate
{
    func setDateandTime(dateValue : Date,type : Int)
}

class CommonDateTimeVC: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var dateTimePicker: UIDatePicker!
    
    @IBOutlet var btnCancel: UIButton!
    //MARK: - Variable
    
    var pickerDelegate : DateTimePickerDelegate?
    
    var controlType : Int = 0
    var setMinimumDate : Date?
    var setMaximumDate : Date = Date()
    
    var isSetMinimumDate : Bool = false
    var isComeFromPool : Bool = false
    var isSetMaximumDate : Bool = false
    var isTimerOpen:Bool = false
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  setMinimumDate != nil {
            dateTimePicker.minimumDate = setMinimumDate!
        }
        
        if controlType == 0
        {
            dateTimePicker.datePickerMode = .time
        }
        else
        {
            dateTimePicker.datePickerMode = .date
        }
        
        
        if isComeFromPool
        {
            if isSetMinimumDate == false
            {
                dateTimePicker.minimumDate = Date()
            }
        }
        else
        {
            if isSetMaximumDate
            {
                dateTimePicker.maximumDate = setMaximumDate
            }else{
                dateTimePicker.minimumDate = setMaximumDate
            }
        }
        
        if(isTimerOpen)
        {
            btnCancel.isHidden = true
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnDoneTapped(_ sender: UIButton)
    {
        
        self.dismiss(animated: true, completion: nil)
        
        pickerDelegate?.setDateandTime(dateValue: dateTimePicker.date, type: controlType)
    }
    
    
    @IBAction func btnCancelTapped(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
}

