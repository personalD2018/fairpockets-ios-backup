//
//  SideMenuViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 04/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

var selectedSideMenuIndex = 0

class SideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tblSideMenu: UITableView!
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var vwFortableHeader: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    var strMyClientCount:Int! = Int()
    var strMessageCount:Int! = Int()
    var strBrochureCount:Int! = Int()
     var strPriceListCount:Int! = Int()
     var strBuilderMsgCount:Int! = Int()
    //var sideMenulblArray = ["Home","My Client","Notifications","Builder Message","Price List","Brochure","Images"]
    var sideMenulblArray = ["Home","My Client","Builder Message","Price List","Brochure","Images", "Videos"]
//    var sideMenuiconArray = ["ic_home_sidemenu","ic_my_client_sidemenu","ic_notification_sidemenu","ic_home_sidemenu","ic_my_client_sidemenu","ic_notification_sidemenu","ic_notification_sidemenu"]
    var sideMenuiconArray = ["ic_home_sidemenu","ic_my_client_sidemenu","builder_message_icon","price_list_icon","brochure_icon","images_icon", "ic_video"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        tblSideMenu.delegate = self
//        tblSideMenu.dataSource = self
        tblSideMenu.register(UINib(nibName: "SideMenuTableCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableCell")
        tblSideMenu.tableHeaderView = vwFortableHeader
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        
    }
    @objc func loadList(){
        //load data here
//        self.tblSideMenu.delegate = self
//        self.tblSideMenu.dataSource = self
//        tblSideMenu.register(UINib(nibName: "SideMenuTableCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableCell")
//        tblSideMenu.tableHeaderView = vwFortableHeader
         self.tblSideMenu.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       print("viewWill Appear")
      self.loadData()
       

     //  self.tblSideMenu.reloadData()
    }
    func loadData(){
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        ServerRequestHandler.sharedappController.getMyClientNotification(para: para, withHandler: {(_ status: Int, _ data: NSObject) -> Void in
            if status == 1
            {
                if let dict = (data) as? NSDictionary {
                    if let result = dict.value(forKey: "appuser_client_notification_list") as? NSArray {
                        print("result",result)
                        if let dict = result.object(at: 0) as? NSDictionary{
                            ServerRequestHandler.sharedappController.myClientCount = dict.value(forKey: "counts") as? Int
                        }
                        //  strMyClientCount
                        //                        self.tblSideMenu.reloadData()
                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                    }
                }
            }
            else
            {
                
            }
        })
        
        var param = [String:AnyObject]()
        param["user_id"] = UserId as AnyObject
        if (UserType == "Sales")
        {
            param["user_type"] = UserType.lowercased() as AnyObject
        }
        else
        {
            param["user_type"] = UserType as AnyObject
        }
        ServerRequestHandler.sharedappController.getMessageNotification(para: param, withHandler: {(_ status: Int, _ data: NSObject) -> Void in
            if status == 1
            {
                if let dict = (data) as? NSDictionary {
                    //                    if let result = dict.value(forKey: "appuser_client_notification_list") as? NSArray {
                    //                        print("result",result)
                    //                        if let dict = result.object(at: 0) as? NSDictionary{
                    //                            self.strMessageCount = dict.value(forKey: "counts") as? Int
                    //                        }
                    //                        //  strMyClientCount
                    //
                    //                    }
                    
                    if let arrBrochure = dict.value(forKey: "brochure_list") as? NSArray{
                        if let dict = arrBrochure.object(at: 0) as? NSDictionary{
                            if let count = dict.value(forKey: "counts") as? String{
                              ServerRequestHandler.sharedappController.brochureCount = Int(count)
                                
                            }
                        }
                    }
                    if let arrBuilder = dict.value(forKey: "builder_msg_list") as? NSArray{
                        
                        if let dict = arrBuilder.object(at: 0) as? NSDictionary{
                            if let count = dict.value(forKey: "counts") as? Int64{
                              //  self.strBuilderMsgCount = Int(count)
                                ServerRequestHandler.sharedappController.builderMsgCount = Int(count)
                            }
                        }
                    }
                    
                    if let arrPriceList = dict.value(forKey: "price_list") as? NSArray{
                        if let dict = arrPriceList.object(at: 0) as? NSDictionary{
                            if let count = dict.value(forKey: "counts") as? String{
                               ServerRequestHandler.sharedappController.priceListCount = Int(count)
                            }
                        }
                    }
                    
                }
                //  self.tblSideMenu.reloadData()
                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
            }
            else
            {
                
            }
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: - Tableview datasource method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return sideMenulblArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSideMenu.dequeueReusableCell(withIdentifier: "SideMenuTableCell", for: indexPath) as! SideMenuTableCell
        cell.lblCount.layer.cornerRadius = 15
        cell.lblCount.layer.masksToBounds = true
        cell.lblSideMenu.text = sideMenulblArray[indexPath.row]
        cell.imgSideMenu.image = UIImage(named: "\(sideMenuiconArray[indexPath.row])")
        
        if(selectedSideMenuIndex == indexPath.row)
        {
            cell.backView.backgroundColor = kDefaultDropDownColor
        }else{
            cell.backView.backgroundColor = .clear
        }
        if (indexPath.row == 0)
        {
            cell.lblCount.isHidden = true
        }
        if (indexPath.row == 1){
             if ServerRequestHandler.sharedappController.myClientCount>0 {
            cell.lblCount.isHidden = false
            cell.lblCount.text = String(ServerRequestHandler.sharedappController.myClientCount)
            }else{
                 cell.lblCount.isHidden = true
            }
        }
        if (indexPath.row == 2){
         
             if ServerRequestHandler.sharedappController.builderMsgCount>0 {
            cell.lblCount.isHidden = false
            cell.lblCount.text = String(ServerRequestHandler.sharedappController.builderMsgCount)
             }else{
                cell.lblCount.isHidden = true
            }
        }
        if (indexPath.row == 3){
            if ServerRequestHandler.sharedappController.priceListCount>0 {
            cell.lblCount.isHidden = false
            cell.lblCount.text = String(ServerRequestHandler.sharedappController.priceListCount)
            }else{
                cell.lblCount.isHidden = true
            }
        }
        if (indexPath.row == 4){
            if ServerRequestHandler.sharedappController.brochureCount > 0 {
            cell.lblCount.isHidden = false
            cell.lblCount.text = String(ServerRequestHandler.sharedappController.brochureCount)  //
            }else{
                cell.lblCount.isHidden = true
            }
        }
        if (indexPath.row == 5 || indexPath.row == 6){
             cell.lblCount.isHidden = true
        }
        
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mainViewController = sideMenuController!
        let navigationController = mainViewController.rootViewController as! NavigationController
        selectedSideMenuIndex = indexPath.row
        tblSideMenu.reloadData()
        
        if indexPath.row == 0 {//home
            if (ServerRequestHandler.sharedappController.defaultHomeLoader == 100){
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
             //   let homevc = self.storyboard?.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
//            navigationController.setViewControllers([homevc,homeVC], animated: false)
                 navigationController.setViewControllers([homeVC], animated: false)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
                
            }else{
                let home = self.storyboard?.instantiateViewController(withIdentifier: "InventoryDetailsVC") as! InventoryDetailsVC
            //    let homevc = self.storyboard?.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
                navigationController.setViewControllers([home], animated: false)
             
                 mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            
        } else if indexPath.row == 1 {//My Client
            
            let MyClientsVC = self.storyboard?.instantiateViewController(withIdentifier: "MyClientsListViewController") as! MyClientsListViewController
            navigationController.setViewControllers([MyClientsVC], animated: false)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            
        }else if indexPath.row == 2 {//Builder Message
            let builderMessageVC = self.storyboard?.instantiateViewController(withIdentifier: "BuilderMessageVC") as! BuilderMessageVC
            builderMessageVC.countVal = self.strBuilderMsgCount
            navigationController.setViewControllers([builderMessageVC], animated: false)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            
        } else if indexPath.row == 3 {//PriceList
            //pritam
            let PriceListVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceListViewController") as! PriceListViewController
            PriceListVC.countVal = self.strPriceListCount
            navigationController.setViewControllers([PriceListVC], animated: false)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            
            
        }else if indexPath.row == 4 {//Brochure
            //pritam
            let BrocherVC = self.storyboard?.instantiateViewController(withIdentifier: "BrochureVC") as! BrochureVC
            BrocherVC.countVal = self.strBrochureCount
            navigationController.setViewControllers([BrocherVC], animated: false)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
        } else if indexPath.row == 5 {//Images
            let imageVC = self.storyboard?.instantiateViewController(withIdentifier: "ImagesVC") as! ImagesVC
            navigationController.setViewControllers([imageVC], animated: false)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
        } else if indexPath.row == 6 {//Videos
            let vc = VideoVC.instantiate(fromAppStoryboard: .Main)
            navigationController.setViewControllers([vc], animated: false)
            mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
        }
    }
}
