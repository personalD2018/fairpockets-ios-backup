//
//  ImageGalleryVC.swift
//  Fairpockets Partner
//
//  Created by Pritam Dey on 26/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SkyFloatingLabelTextField
import SwiftyJSON
import SDWebImage

class ImageGalleryVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,NVActivityIndicatorViewable,UITextFieldDelegate,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var imageCollectionView: UICollectionView!
    var imagesArr:NSArray = NSArray()
    var _selectedCells : NSMutableArray = []
    var SelectedImages: NSMutableArray = []
    var projectId:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "ClientHistory_key"))
        imageCollectionView.allowsMultipleSelection=true
        
        let btn1 = UIButton()
        btn1.setImage(UIImage(named: "share_white"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.tintColor = UIColor.white
        btn1.addTarget(self, action: #selector(self.shareImageButton(_:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        getAllProjectImages()
    }
    
    @objc func shareImageButton(_ sender: UIButton) {
        // set up activity view controller
        let imageToShare:NSMutableArray = SelectedImages
        let activityViewController = UIActivityViewController(activityItems: imageToShare as! [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    

    
    func getAllProjectImages()
    {
        var para = [String:AnyObject]()
        para["project_id"] = projectId as AnyObject
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            // self.ProjectList.removeAll()
            self.imagesArr = NSArray()
            
            
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            ServerRequestHandler.sharedappController.getimagesbyprojectID(para: para,withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                self.stopAnimating()
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self.imagesArr = result
                            self.imageCollectionView.reloadData()
                        }
                    }
                }
                
            })
            
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         let cell = self.imageCollectionView.dequeueReusableCell(withReuseIdentifier: "ImageGalleryCell", for: indexPath) as! ImageGalleryCell
        cell.cellBackgroundView.backgroundColor = UIColor.white
        if let imageName:NSDictionary = self.imagesArr[indexPath.row] as? NSDictionary{
        let imgDic = JSON(imageName)
            let imageName = imgDic["ProjectImage"]["image_name"].stringValue
            let imageUrl = "http://www.fairpockets.com/" + imageName
            cell.projectImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder.png"))
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width : CGFloat =  (UIScreen.main.bounds.size.width-40)/2 //  (self.imageCollectionView.frame.size.width-8)/2
        let height : CGFloat = width-20
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 10
        
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var cell:ImageGalleryCell = collectionView.cellForItem(at: indexPath) as! ImageGalleryCell
        if cell.isSelected == true {
            cell.cellBackgroundView.backgroundColor = UIColor.blue
            _selectedCells.add(indexPath)
            self.SelectedImages.add(cell.projectImageView.image)
        }
        else{
             cell.cellBackgroundView.backgroundColor = UIColor.white
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    var cell:ImageGalleryCell = collectionView.cellForItem(at: indexPath) as! ImageGalleryCell
        if cell.isSelected == true {
            cell.cellBackgroundView.backgroundColor = UIColor.blue
        }
        else{
            cell.cellBackgroundView.backgroundColor = UIColor.white
             _selectedCells.remove(indexPath)
         self.SelectedImages.remove(cell.projectImageView.image)
        }
       
     
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
