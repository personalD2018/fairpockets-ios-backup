
//
//  StatusDetailViewController.swift
//  Fairpockets Partner
//
//  Created by Ashim Samanta on 23/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class StatusDetailViewController: UIViewController {

    var updatedBy:String = ""
    var updatedOn:String = ""
    
    @IBOutlet weak var lblStatusUpdatedBy: UILabel!
    @IBOutlet weak var lblStatusUpdatedOn: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblStatusUpdatedBy.text = self.updatedBy
        self.lblStatusUpdatedOn.text = self.updatedOn
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
