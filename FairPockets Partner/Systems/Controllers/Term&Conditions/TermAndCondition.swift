//
//  TermAndCondition.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 06/08/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class TermAndCondition: UIViewController,UIWebViewDelegate,NVActivityIndicatorViewable {
     @IBOutlet var pdfWebView: UIWebView!
     var keys = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        if (keys == "TC"){
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "ViewTC"))  
        if let url = Bundle.main.url(forResource: "terms_and_condition", withExtension: "html") {
            pdfWebView.loadRequest(URLRequest(url: url))
        }
        }
        else{
            setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "ViewPP"))
            if let url = Bundle.main.url(forResource: "privacy_policy", withExtension: "html") {
                pdfWebView.loadRequest(URLRequest(url: url))
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        //Page is loaded do what you want
        self.stopAnimating()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
