//
//  LoginViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 02/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class LoginViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable {

    //#MARK:- Outlets
    @IBOutlet var lblTitleBuilder: UILabel!
    @IBOutlet var lblMobile: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btnSignIn: xUIButton!
    @IBOutlet var btnTermNCondition:UIButton!
    @IBOutlet var btnPrivacyPoloicy:UIButton!
    @IBOutlet var btnAgree:UIButton!
    //#MARK:- Declaration
    
    
    
    //#MARK:- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.isNavigationBarHidden = true
        [txtEmail,txtPassword].forEach { (text) in
            text!.delegate = self
        }
        setUpText()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnAgree.setImage(UIImage(named: "ic_check_box_white.png"), for: .normal)
        UserDefaults.standard.set("1", forKey: "token")
        UserDefaults.standard.synchronize()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //#MARK:- Member Function
    
    func setUpText()
    {
        lblTitleBuilder.text = mapping.string(forKey: "Builder-Broker_Management_key")
        lblMobile.text = mapping.string(forKey: "MOBILE_Capital_key")
        lblEmail.text = mapping.string(forKey: "EMAIL_key")
        
        txtEmail.placeholder = mapping.string(forKey: "Email_key")
        txtPassword.placeholder = mapping.string(forKey: "Password_key")
        btnSignIn.setTitle(mapping.string(forKey: "SignIn_key"), for: .normal)
        
        let attribTermsAndCondition : [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.font : UIFont.italicSystemFont(ofSize: 18.0),
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
        let attributeString = NSMutableAttributedString(string: "Terms & Conditions",
                                                        attributes: attribTermsAndCondition)
        btnTermNCondition.setAttributedTitle(attributeString, for: .normal)
    }
    
    //#MARK:- Button Action
    
    @IBAction func btnMobileAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnEmailAction(_ sender: UIButton) {
        
    }
    @IBAction func btnAgree(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if(sender.isSelected == true)
        {
            btnAgree.setImage(UIImage(named: "ic_check_box_outline_blank_white.png"), for: .normal)
             UserDefaults.standard.set("0", forKey: "token")
             UserDefaults.standard.synchronize()
        }else{
            btnAgree.setImage(UIImage(named: "ic_check_box_white.png"), for: .normal)
            UserDefaults.standard.set("1", forKey: "token")
            UserDefaults.standard.synchronize()
        }
    }
    @IBAction func btnSignInAction(_ sender: xUIButton) {
        
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: mapping.string(forKey: "Enter_email_address_key"))
            return
        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: mapping.string(forKey: "Please_enter_valid_email_address_key"))
            return
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: mapping.string(forKey: "Please_enter_password_key"))
            return
        }
        let val = UserDefaults.standard.value(forKey: "token") as? String
        if (val == "1"){
        Login()
        }else{
           makeToast(strMessage: mapping.string(forKey: "TermsAndConditions"))
            return
        }
        
    }
    @IBAction func TermsNconditions(_ sender: UIButton) {
        let pdfView = self.storyboard!.instantiateViewController(withIdentifier: "TermAndCondition") as! TermAndCondition
        pdfView.keys = "TC"
        self.navigationController?.pushViewController(pdfView, animated: true)
    }
    @IBAction func PrivacyPolicy(_ sender: UIButton) {
        let pdfView = self.storyboard!.instantiateViewController(withIdentifier: "TermAndCondition") as! TermAndCondition
        pdfView.keys = "PP"
        self.navigationController?.pushViewController(pdfView, animated: true)
    }
    //#MARK:- API Calling
    func Login()
    {
        var para = [String:AnyObject]()
        para["email"] = txtEmail.text! as AnyObject
        para["password"] = txtPassword.text! as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetSignInManager().getSignIn(param: para) { [weak self] LoginData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard LoginData == nil else{
                    if(LoginData!.code == 1)
                    {
                        let data = LoginData!.logindata!
                        var LoginResponse = [String:AnyObject]()
                        LoginResponse["UserId"] = data.userId! as AnyObject
                        LoginResponse["UserType"] = data.userType! as AnyObject
                        LoginResponse["Builder_id"] = data.builder_id! as AnyObject
                        userdefualts.removeObject(forKey: "LoginData")
                        userdefualts.set(LoginResponse, forKey: "LoginData")
                        userdefualts.synchronize()
                        UserType = data.userType!
                        UserId = data.userId!
                        Builder_id = data.builder_id!
                        UserIsLogin = true
                        //after next Api Call
                        let HomeVC = self?.storyboard?.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
                        let navigationController = NavigationController(rootViewController: HomeVC)
                        
                        let mainViewController = MainViewController()
                        mainViewController.rootViewController = navigationController
                        mainViewController.setup(type: UInt(1))
                        
                        navigationController.isNavigationBarHidden = true
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = mainViewController
                    }else{
                        self!.makeToast(strMessage: LoginData!.message!)
                    }
                    
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
        
    }
    
}
