//
//  DetailsCell.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 22/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class DetailsCell: UITableViewCell {
    @IBOutlet weak var lblFlatNo:UILabel!
    @IBOutlet weak var lblArea:UILabel!
    @IBOutlet weak var lblConfig:UILabel!
    @IBOutlet weak var lblStatus:UILabel!
    @IBOutlet weak var lblPosition:UILabel!
    @IBOutlet weak var btnInfo: UIButton!
    
    typealias _handler = (Int) -> Void
    
    var infoHandler:_handler?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func registerInfoHandler(info handler:@escaping _handler) {
        self.infoHandler = handler;
    }
    @IBAction func showInfo(_ sender: Any) {
        if let infHndlr = self.infoHandler {
            infHndlr(self.btnInfo.tag);
        }
    }
}
