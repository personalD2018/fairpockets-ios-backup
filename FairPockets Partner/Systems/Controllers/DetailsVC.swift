//
//  DetailsVC.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 22/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class DetailsVC: UIViewController, UITableViewDelegate,UITableViewDataSource, NVActivityIndicatorViewable {
    var arrDetails:NSArray = NSArray()
    var params = [String:AnyObject]()
    var invStatus: Any?
    var shouldEdit: Bool = false;
    var selectedIndex: Int = -1;
    var selectedIndexPath: IndexPath = IndexPath(row: -1, section: -1);
//    var firstLabel: UILabel?
    var selectedSection: Int = -999
    var cell:SectionCell = SectionCell()
    @IBOutlet weak var tableDetails:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: "Inventory Details")
        self.tableDetails.reloadData()
    }
    
    @objc func expandCell(sender:UIButton)  {
       
//        if self.selectedSection == sender.tag{
//            self.selectedSection = -999
//
//        }else{
//             self.selectedSection = sender.tag
//        }
//        tableDetails.reloadData()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        cell = tableView.dequeueReusableCell(withIdentifier: "SectionCell") as! SectionCell
        if(self.arrDetails.count>0){
            if let floorDict = self.arrDetails.object(at: section) as? NSDictionary{
                if  let floordata = floorDict.value(forKey: "floor_id") as? String {
                cell.lblTitle.text = "Floor: " +  floordata
                    cell.btnEnter.tag = section
                    cell.btnEnter.addTarget(self, action: #selector(self.expandCell(sender:)), for: .touchUpInside)
                    cell.btnEnter.isSelected = false
                    if (self.selectedSection == section){
                     cell.btnEnter.isSelected = !cell.btnEnter.isSelected
                    }else{
                       cell.btnEnter.isSelected = false
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if self.arrDetails.count > 0 {
            if let dict = arrDetails[section] as? NSDictionary {
                if let arr = dict.value(forKey: "floor_data") as? NSArray {
                    return arr.count + 1
                }
            }
        }
      return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        return 45
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:DetailsCell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell") as! DetailsCell
        if (indexPath.row == 0) {
           let cellOne:HeaderCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
            cellOne.backgroundColor = UIColor.white
            
            return cellOne
        }
        
        if self.arrDetails.count > 0 {
            
            if let dict = arrDetails[indexPath.section] as? NSDictionary {
               if let arr = dict.value(forKey: "floor_data") as? NSArray {
                        if let subDict = arr.object(at: indexPath.row - 1) as? NSDictionary{
                        cell.lblArea.text = subDict.value(forKey: "area") as? String
                        cell.lblFlatNo.text = subDict.value(forKey: "flat_no") as? String
                        cell.lblConfig.text = subDict.value(forKey: "config") as? String
                        cell.lblPosition.text = subDict.value(forKey: "position") as? String
                        cell.btnInfo.alpha = 0
                        cell.btnInfo.tag = -1;
                        if let st = subDict.value(forKey: "status_code") as? String, let invSt = invStatus as? NSNumber {
                            if (shouldEdit && st == "1" && invSt == 8) {
                                cell.btnInfo.setImage(UIImage(named: "edit_icon"), for: .normal)
                                cell.btnInfo.alpha = 1
                                cell.btnInfo.tag = 100+indexPath.row;
                            }
                        }
                            
                        if let st = subDict.value(forKey: "status_code") as? String, st == "3" {
                            cell.btnInfo.setImage(UIImage(named: "icon_info"), for: .normal)
                            cell.btnInfo.alpha = 1
                            cell.btnInfo.tag = 100+indexPath.row;
                        }
                        
                        if let strStatus = subDict.value(forKey: "status_code") as? String, let status = Int(strStatus) {
                            switch status {
                                case 1:
                                    cell.lblStatus.text = "Available"
                                    cell.backgroundColor = UIColor(red:220.0/255.0, green: 238.0/255.0, blue: 214.0/255.0, alpha: 1.0)
                                
                                case 2:
                                    cell.lblStatus.text = "Not Available"
                                    cell.backgroundColor = UIColor(red:239.0/255.0, green: 219.0/255.0, blue: 220.0/255.0, alpha: 1.0)
                                
                                case 3:
                                    cell.lblStatus.text = "On-Hold"
                                    cell.backgroundColor = UIColor(rgb: 0xF8C471)
                                
                                case 4:
                                    cell.lblStatus.text = "Not Available"
                                    cell.backgroundColor = UIColor(red:239.0/255.0, green: 219.0/255.0, blue: 220.0/255.0, alpha: 1.0)
                                default: break
                            }
                        }
                    }
                }
            }
        }
        
        cell.registerInfoHandler { [weak self] (index) in
            let indx = index - 100
            print("index == \(indx)");
            self?.selectedIndex = indx;
            self?.selectedIndexPath = indexPath
            if let dict = self?.arrDetails[indexPath.section] as? NSDictionary {
                if let arr = dict.value(forKey: "floor_data") as? NSArray, let floor_id =  dict.value(forKey: "floor_id") {
                    if let subDict = arr.object(at: indx - 1) as? NSDictionary {
                        if let st = subDict.value(forKey: "status_code") as? String, let invSt = self?.invStatus as? NSNumber {
                            if ((self?.shouldEdit)! && st == "1" && invSt == 8) {
                                self?.showModal(dict: subDict, floor_id: floor_id as! String);
                                return;
                            }
                        }
                    }
                }
                
                if let arr = dict.value(forKey: "floor_data") as? NSArray {
                    if let subDict = arr.object(at: indx - 1) as? NSDictionary{
                        var edittedBy:String = ""
                        var editedOn:String = ""
                        if let editedBy = subDict.value(forKey: "editedBy") as? String {
                             edittedBy = editedBy
                        }
                        if let updated_date = subDict.value(forKey: "updated_date") as? String {
                            editedOn = updated_date
                        }
                        
                        self?.showStatusModal(updatedBy: edittedBy, updatedOn: editedOn)
                    }
                }
            }
        }
        
        return cell
    }
    
    func showStatusModal(updatedBy:String, updatedOn: String) {
        let vc: StatusDetailViewController = StatusDetailViewController.instantiate(fromAppStoryboard: .Main)
        vc.updatedBy = updatedBy
        vc.updatedOn = updatedOn
        vc.view.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        vc.modalPresentationStyle = .overFullScreen;
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil);
    }
    
    func showModal(dict: NSDictionary, floor_id: String) {
        
        let vc: EditInventoryViewController = EditInventoryViewController.instantiate(fromAppStoryboard: .Main);
        vc.dict = dict
        vc.params = self.params
        vc.floor_id = floor_id
        vc.setComplitionHandler { [weak self] (val) in
            if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
                self?.startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
                NetworkActivityManager.sharedInstance.busy = true
                
                ServerRequestHandler.sharedappController.getInvDetails(param: (self?.params)! ,withHandler: { [weak self] (_ status: Int, _ data: NSObject) -> Void in
                    NetworkActivityManager.sharedInstance.busy = false
                    self!.stopAnimating()
                    if status == 1  {
                        if let dict = (data) as? NSDictionary {
                            if let result = dict.value(forKey: "data") as? NSArray {
                                self?.arrDetails = result;
                                self?.tableDetails.reloadData();
                            }
                        }
                    }
                })
            } else {
                self?.makeToast(strMessage: NointernetConnection!)
            }
        }
        self.definesPresentationContext = true; //self is presenting view controller
        vc.view.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        vc.modalPresentationStyle = .overFullScreen;
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil);
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrDetails.count
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        firstLabel?.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
