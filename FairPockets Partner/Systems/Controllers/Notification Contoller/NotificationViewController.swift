//
//  NotificationViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 04/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class NotificationViewController: UIViewController,NVActivityIndicatorViewable {

    //#MARK:- Outlets
    
    @IBOutlet var tbl_NotificationList: UITableView!
    @IBOutlet var lblNoData: UILabel!
    
    //#MARK:- Declaration
    
    var NotificationList = [Notificationdata]()
    
    //#MARK:- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar(titleText: mapping.string(forKey: "Builder-Broker_Management_key"))
        
        lblNoData.text = mapping.string(forKey: "NoNotification_key")
        self.tbl_NotificationList.dataSource = self
        self.tbl_NotificationList.delegate = self
        tbl_NotificationList.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        tbl_NotificationList.tableFooterView = UIView()
        tbl_NotificationList.estimatedRowHeight = 78
        tbl_NotificationList.rowHeight = UITableViewAutomaticDimension
        lblNoData.isHidden = true
        getNotificationList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //#MARK:- Member Function
    @IBAction func Sumbit_Action(_ sender: Any) {
    }
    
    
    //#MARK:- API Calling
    
    func getNotificationList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetNotificationListManager().getNotificationList(param: para) { [weak self] NotificationData in
                guard NotificationData == nil else{
                    if(NotificationData!.code == 1)
                    {
                        NotificationData!.notificationdata!.forEach { notification in
                            self!.NotificationList.append(notification)
                        }
                        self!.NotificationList.forEach { notification in
                            if (notification.reminderDate! != "")
                            {                                
                                let ReminderInDate = self?.stringTodate(Formatter: "dd-MM-yyyy", strDate: notification.reminderDate!)
                                
                                print("ReminderDate : \(ReminderInDate)")
                                
                                let calendar = Calendar.current
                                
                                let year = calendar.component(.year, from: ReminderInDate ?? Date())
                                let month = calendar.component(.month, from: ReminderInDate ?? Date())
                                let day = calendar.component(.day, from: ReminderInDate ?? Date())
                                
                                appDelegate?.locallyNotification(year: year, month: month, day: day)
                                
                            }
                        }
                        
                    
                        self!.lblNoData.isHidden = true
                        self!.tbl_NotificationList.reloadData()
                        return
                        
                    }else{
                        self!.makeToast(strMessage: NotificationData!.message!)
                        self!.lblNoData.isHidden = false
                    }
                    self!.lblNoData.isHidden = false
                    self!.tbl_NotificationList.reloadData()
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
}
extension NotificationViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.NotificationList.count > 0){
            return self.NotificationList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
        cell.tag = indexPath.row
        if(self.NotificationList.count > 0){
            cell.setData(theModel: self.NotificationList[indexPath.row])
        }
        return cell
        
    }
    
}



