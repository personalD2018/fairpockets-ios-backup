//
//  MyClientsListViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 04/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import NVActivityIndicatorView
import SkyFloatingLabelTextField

struct SortBy {
    public static let reminderDate = 0
    public static let createdDate = 1
}

class MyClientsListViewController: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {
    //#MARK:- Outlets
    
    @IBOutlet var txtSearch: SkyFloatingLabelTextField!
    @IBOutlet var tbl_ClientList: UITableView!
    
    @IBOutlet var lblNoData: UILabel!
    @IBOutlet weak var lblNoOfClients: UILabel!
    @IBOutlet weak var lblSort: UILabel!
    
    var dropDownSortBy = DropDown()
    var sortByList = ["Reminder Date", "Created Date"]
    var selectedSortId = -1
    
    //#MARK:- Declaration
    var MyClientList = [Clientdata]()
    var isSearch = false
    var MyClientSearchList = [Clientdata]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar(titleText: mapping.string(forKey: "Builder-Broker_Management_key"))
        self.txtSearch.toolbarPlaceholder = mapping.string(forKey: "SearchClient_key")
        lblNoData.text = mapping.string(forKey: "NoClientHistory_key")
        self.tbl_ClientList.dataSource = self
        self.tbl_ClientList.delegate = self
        self.txtSearch.delegate = self
        tbl_ClientList.register(UINib(nibName: "ClientListTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientListTableViewCell")
        tbl_ClientList.tableFooterView = UIView()
        tbl_ClientList.estimatedRowHeight = 78
        tbl_ClientList.rowHeight = UITableViewAutomaticDimension
        lblNoData.isHidden = true
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getClientList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configDropdown(dropdown: self.dropDownSortBy, sender: lblSort.superview!)
    }
    
    //#MARK:- Member Function
    @IBAction func sortByAction(_ sender: Any) {
        self.showSortDropDown()
    }
    
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width + 60
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
    
    func showSortDropDown() {
        self.dropDownSortBy.dataSource = self.sortByList
        self.dropDownSortBy.show()
        self.dropDownSortBy.selectionAction = { [weak self] (index,item) in
            self?.lblSort.text = item
            self?.selectedSortId = index
            self?.applySortWithSortIndex(index: index)
        }
    }
    
    func applySortWithSortIndex(index: Int) {
        let dateFormatter = DateFormatter()
        //index -> 0=Reminder Date, 1=Created Date
        if index == SortBy.reminderDate {
            dateFormatter.dateFormat = "dd-MM-yyyy"
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }
        
        if isSearch { // search is applied
            self.MyClientSearchList.sort { (obj1, obj2) -> Bool in
                if index == SortBy.reminderDate {
                    if let reminderDt1 = obj1.reminderDate, let reminderDt2 = obj2.reminderDate {
                        let date1 = dateFormatter.date(from: reminderDt1)
                        let date2 = dateFormatter.date(from: reminderDt2)
                        if let dt1 = date1, let dt2 = date2 {
                            if dt1 > dt2 {
                                return true
                            } else {
                                return false
                            }
                        }
                    }
                } else {
                    if let crtDt1 = obj1.created, let crtDt2 = obj2.created {
                        let date1 = dateFormatter.date(from: crtDt1)
                        let date2 = dateFormatter.date(from: crtDt2)
                        if let dt1 = date1, let dt2 = date2 {
                            if dt1 > dt2 {
                                return true
                            } else {
                                return false
                            }
                        }
                    }
                }
                return false
            }
        } else {
            self.MyClientList.sort { (obj1, obj2) -> Bool in
                if index == SortBy.reminderDate {
                    if let reminderDt1 = obj1.reminderDate, let reminderDt2 = obj2.reminderDate {
                        let date1 = dateFormatter.date(from: reminderDt1)
                        let date2 = dateFormatter.date(from: reminderDt2)
                        if let dt1 = date1, let dt2 = date2 {
                            if dt1 > dt2 {
                                return true
                            } else {
                                return false
                            }
                        }
                    }
                } else {
                    if let crtDt1 = obj1.created, let crtDt2 = obj2.created {
                        let date1 = dateFormatter.date(from: crtDt1)
                        let date2 = dateFormatter.date(from: crtDt2)
                        if let dt1 = date1, let dt2 = date2 {
                            if dt1 > dt2 {
                                return true
                            } else {
                                return false
                            }
                        }
                    }
                }
                return false
            }
        }
        
        self.tbl_ClientList.reloadData()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if(textField.text != "")
        {
            MyClientSearchList.removeAll()
            isSearch = true
            MyClientList.forEach { Client in
                if(Client.name!.contains(textField.text!)  || Client.mobile!.contains(textField.text!) || Client.email!.contains(textField.text!))
                {
                    self.MyClientSearchList.append(Client)
                }
            }
            print("MyClientSearchList -->\(MyClientSearchList.count)")
            self.lblNoOfClients.text = "Number of Clients: \(self.MyClientSearchList.count)"
            if(MyClientSearchList.count > 0)
            {
                
                self.lblNoData.isHidden = true
            }else{
                self.lblNoData.isHidden = false
            }
            
        }else{
            MyClientSearchList.removeAll()
            isSearch = false
            self.lblNoOfClients.text = "Number of Clients: \(self.MyClientList.count)"
        }
        self.tbl_ClientList.reloadData()
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if(textField.text == "")
        {
            isSearch = false
            MyClientSearchList.removeAll()
            
        }else{
            isSearch = true
            
        }
        return true
    }

     //#MARK:- API Calling
    
    func getClientList() {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            GetClientListManager().getClientList(param: para) { [weak self] ClientListData in
                guard ClientListData == nil else{
                    self?.MyClientList = [Clientdata]()
                    if(ClientListData!.code == 1)
                    {
                        ClientListData!.clientdata!.forEach { Client in
                            self!.MyClientList.append(Client)
                        }
                        self?.lblNoData.isHidden = true
                          print("count1---",self?.MyClientList.count)
                        let count: Int? = self?.MyClientList.count
                        self?.lblNoOfClients.text = "Number of Clients: \(count!)"
                        self?.tbl_ClientList.reloadData()
                        return
                        
                    }else{
                        self?.makeToast(strMessage: ClientListData!.message!)
                        self?.lblNoData.isHidden = false
                    }
                    print("count---",self?.MyClientList.count)
                    self?.lblNoData.isHidden = false
                    let count: Int? = self?.MyClientList.count
                    self?.lblNoOfClients.text = "Number of Clients: \(count!)"
                    self?.tbl_ClientList.reloadData()
                    return
                }
            }
        } else {
            makeToast(strMessage: NointernetConnection!)
            self.lblNoOfClients.text = "Number of Clients: \(self.MyClientList.count)"
        }
    }
    
    //#MARK:- Make Phone Call
    func makeCallTo(toNumber:String?) {
        if let phoneNumber = toNumber, let url = URL(string: "tel://\(phoneNumber)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(url);
            };
        } else {
            self.makeToast(strMessage: "Phone number is not valid");
        }
    }
    //#MARK:- Delete row data
    func deleteClient(index:Int?) {
        if let _index = index {
            
            let alertController = UIAlertController(title: "Broker Management", message: "Are you sure want to delete?", preferredStyle: .alert)
            let actionCancel = UIAlertAction(title: "NO", style: .default, handler: nil)
            let actionOK = UIAlertAction(title: "YES", style: .default) { [weak self] (action:UIAlertAction) in
                let clientData = self?.MyClientList[_index]
                var para = [String:AnyObject]()
                if let user_id = clientData?.userId, let clientID = clientData?.id {
                    para["user_id"] = user_id as AnyObject
                    para["client_id"] = clientID as AnyObject
                }
                if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
                    NetworkActivityManager.sharedInstance.busy = true
                    self?.startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
                    
                    GetClientListManager().deleteClientFromList(param: para) { [weak self] ClientListData in
                        NetworkActivityManager.sharedInstance.busy = false
                        self?.stopAnimating()
                        guard ClientListData == nil else {
                            
                            if(ClientListData?.code == 1) {
                                
                                ClientListData?.clientdata?.forEach { Client in
                                    self?.MyClientList.append(Client)
                                }
                                self?.MyClientList.remove(at: _index);
                                if let count = self?.MyClientList.count {
                                    self?.lblNoOfClients.text = "Number of Clients: \(count)"
                                }
                                self?.tbl_ClientList.reloadData()
                                return
                                
                            } else {
                                self?.makeToast(strMessage: ClientListData?.message)
                            }
                            return
                        }
                    }
                } else {
                    self?.makeToast(strMessage: NointernetConnection!)
                }
            }
            alertController.addAction(actionCancel)
            alertController.addAction(actionOK)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        }
    }
}

extension MyClientsListViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearch == false)
        {
            if(self.MyClientList.count > 0){
                return self.MyClientList.count
            }
            
        }else{
            if(self.MyClientSearchList.count > 0){
                self.lblNoData.isHidden = true
                return self.MyClientSearchList.count
            }else{
                self.lblNoData.isHidden = false
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let tblCell = cell as! ClientListTableViewCell
        var strStatus: String?
        if isSearch {
            strStatus = self.MyClientList[indexPath.row].status
        } else {
            strStatus = self.MyClientList[indexPath.row].status
        }
        
        if let sStatus = strStatus, let status = Int(sStatus) {
            switch status {
                case 0:
                    tblCell.vwBackgrd.backgroundColor = kDefaultUnreadCellBGColor;
                
                case 1:
                    tblCell.vwBackgrd.backgroundColor = UIColor.white
            default:
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientListTableViewCell") as! ClientListTableViewCell
        cell.tag = indexPath.row
        
        if isSearch == false {
            cell.setData(theModel: self.MyClientList[indexPath.row])
            cell.registerHandler(forCall: { [weak self] in
                self?.makeCallTo(toNumber: self?.MyClientList[indexPath.row].mobile)
                }, forDelete: { [weak self] in
                    self?.deleteClient(index: indexPath.row);
            })
        } else {
            cell.setData(theModel: self.MyClientSearchList[indexPath.row])
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let historyVC = self.storyboard?.instantiateViewController(withIdentifier: "ClientHistoryViewController") as! ClientHistoryViewController
        
        if(isSearch == false) {
            historyVC.ClientId = self.MyClientList[indexPath.row].id!
            historyVC.status = self.MyClientList[indexPath.row].status!
            
        } else {
            historyVC.ClientId = self.MyClientSearchList[indexPath.row].id!
            historyVC.status = self.MyClientSearchList[indexPath.row].status!
        }
        self.navigationController?.pushViewController(historyVC, animated: true)
    }
}


