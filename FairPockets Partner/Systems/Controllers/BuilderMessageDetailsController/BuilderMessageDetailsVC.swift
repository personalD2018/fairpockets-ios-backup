//
//  BuilderMessageDetailsVC.swift
//  Fairpockets Partner
//
//  Created by Pritam Dey on 26/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class BuilderMessageDetailsVC: UIViewController {

    @IBOutlet weak var lblBuilderName: UILabel!
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var textViewBuilderMessage: UITextView!
    var message=""
    var createdDate = ""
    var builderName = ""
    var orgName = ""
    var ide = ""
    var status = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "ClientHistory_key"))
        lblBuilderName.text = builderName
        lblOrgName.text = orgName
        lblCreatedDate.text = createdDate
        textViewBuilderMessage.text = message
        print("ide",ide)
        
        var param = [String:AnyObject]()
        param["user_id"] = UserId as AnyObject
        param["builder_message_row_id"] = ide as AnyObject
        if (status == "0") {
        ServerRequestHandler.sharedappController.getBuilderMessageRowRead(para: param, withHandler: {(_ status: Int, _ data: NSObject) -> Void in
            if status == 1
            {
                if let dict = (data) as? NSDictionary {
                    
                }
            }
            else
            {
                
            }
        })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
