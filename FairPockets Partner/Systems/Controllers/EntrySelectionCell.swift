//
//  EntrySelectionCell.swift
//  Fairpockets Partner
//
//  Created by Ashim Samanta on 19/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class EntrySelectionCell: UITableViewCell {

    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
