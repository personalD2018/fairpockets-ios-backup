//
//  VideoVC.swift
//  Fairpockets Partner
//
//  Created by Ashim Samanta on 15/10/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

//
//  ImagesVC.swift
//  Fairpockets Partner
//
//  Created by Pritam Dey on 26/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SkyFloatingLabelTextField
import SwiftyJSON

class VideoVC: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {
    
    @IBOutlet var txtSearch: SkyFloatingLabelTextField!
    @IBOutlet var tbl_videos: UITableView!
    @IBOutlet var lblNoData: UILabel!
    
    var videosArr:NSArray = NSArray()
    var videosArrShorted:NSMutableArray = NSMutableArray()
    var SearchvideosArr:NSMutableArray = NSMutableArray()
    var search:String=String()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar(titleText: mapping.string(forKey: "Brochure_key"))
        self.txtSearch.toolbarPlaceholder = mapping.string(forKey: "SearchInquiry_key")
        lblNoData.text = mapping.string(forKey: "NoClientshistry_key")
        self.tbl_videos.dataSource = self
        self.tbl_videos.delegate = self
        self.txtSearch.delegate = self
        tbl_videos.register(UINib(nibName: "ImagesCell", bundle: nil), forCellReuseIdentifier: "ImagesCell")
        
        tbl_videos.tableFooterView = UIView()
        tbl_videos.estimatedRowHeight = 78
        tbl_videos.rowHeight = UITableViewAutomaticDimension
        lblNoData.isHidden = true
        getVideoList()
    }
    
    
    func getVideoList() {
        
        var params = [String:AnyObject]()
        params["user_id"] = UserId as AnyObject
        if (UserType == "Sales"){
            params["user_type"] = UserType.lowercased() as AnyObject
        } else {
            params["user_type"] = UserType as AnyObject
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            self.videosArr = NSArray()
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            ServerRequestHandler.sharedappController.getDetail(url:aBasePostVideosListsURl, param: params,withHandler: {(_ status: Int, _ data: Any?) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                self.stopAnimating()
                
                if status == 1 {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self.videosArr = result
                            if self.videosArr.count > 0 {
                                self.videosArrShorted.removeAllObjects()
                                for priceDic in self.videosArr {
                                    let tempDict:NSMutableDictionary = NSMutableDictionary()
                                    let json = JSON(priceDic as Any)
                                    tempDict["builder_name"] = json["Websiteuser"]["builder_name"].stringValue
                                    tempDict["project_name"] = json["Project"]["project_name"].stringValue
                                    
                                    tempDict["created"] = json["ProjectMedia"]["created"].stringValue
                                    tempDict["project_id"] = json["ProjectMedia"]["project_id"].stringValue
                                    
                                    tempDict["youtube_link1"] = json["ProjectMedia"]["youtube_link1"].stringValue
                                    tempDict["youtube_link1_key"] = json["ProjectMedia"]["youtube_link1_key"].stringValue
                                    
                                    tempDict["youtube_link2"] = json["ProjectMedia"]["youtube_link2"].stringValue
                                    tempDict["youtube_link2_key"] = json["ProjectMedia"]["youtube_link2_key"].stringValue
                                    
                                    self.videosArrShorted.add(tempDict)
                                    print("videosArrShorted",self.videosArrShorted)
                                    self.SearchvideosArr = self.videosArrShorted.mutableCopy() as! NSMutableArray
                                }
                            }
                            
                            self.tbl_videos.reloadData()
                        }
                    }
                }
            })
        } else {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("the text is --------------0",self.txtSearch.text!)
        if string.isEmpty {
            search = String(search.characters.dropLast())
        } else {
            search=textField.text!+string
        }
        
        let predicate=NSPredicate(format: "SELF.builder_name CONTAINS[cd] %@ or SELF.project_name CONTAINS[cd] %@", search,search)
        
        print(predicate)
        let arr=(self.videosArrShorted as NSArray).filtered(using: predicate)
        
        print(arr)
        if arr.count > 0 {
            self.SearchvideosArr.removeAllObjects()
            for i in 0 ..< arr.count {
                SearchvideosArr.add(arr[i])
            }
        } else {
            if search == "" {
                SearchvideosArr =  NSMutableArray()
                SearchvideosArr = self.videosArrShorted.mutableCopy() as! NSMutableArray
            } else {
                SearchvideosArr =  NSMutableArray()
            }
        }
        tbl_videos.reloadData()
        return true
    }
}


extension VideoVC: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.SearchvideosArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImagesCell") as! ImagesCell
        if self.SearchvideosArr.count > 0 {
            if let dict:NSDictionary = self.SearchvideosArr[indexPath.row] as? NSDictionary{
                cell.coffigureCell(myDate: dict, isVideo: true)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.SearchvideosArr.count >= indexPath.row {
            tableView.deselectRow(at: indexPath, animated: true)
            if  let vDict = self.SearchvideosArr[indexPath.row] as? NSDictionary {
                let gallery = VideoGalleryVC.instantiate(fromAppStoryboard: .Main)
                gallery.videoDict = vDict
                self.navigationController?.pushViewController(gallery, animated: true)
            }
        }
    }
}


