//
//  VideoGalleryVC.swift
//  Fairpockets Partner
//
//  Created by Ashim Samanta on 16/10/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

struct Video {
    var ytLink: String?
    var ytLinkKey: String?
    var isSelected: Bool = false
}

class VideoGalleryVC: FPBaseViewController {
    
    @IBOutlet weak var tbl_videos: UITableView!
    
    @IBOutlet weak var ytView1: YTPlayerView!
    @IBOutlet weak var ytView2: YTPlayerView!
    
    @IBOutlet weak var dummyView1: UIView!
    @IBOutlet weak var dummyView2: UIView!
    
    
    var video1Selected: Bool = false;
    var video2Selected: Bool = false;
    
    var videoDict: NSDictionary?
//    var rows = [Video?]();
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "vide_gallery_title_key"))
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_share_new"), style: .plain, target: self, action: #selector(shareVideo))
        rightButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightButton
        
//        tbl_videos.tableFooterView = UIView()
//        tbl_videos.estimatedRowHeight = 300;
//        tbl_videos.rowHeight = UITableViewAutomaticDimension
//        
//        var video: Video = Video();
        
        if let videoID = videoDict?["youtube_link1_key"] as? String {
            self.ytView1.load(withVideoId: videoID);
        }
        
        if let videoID = videoDict?["youtube_link2_key"] as? String {
            self.ytView2.load(withVideoId: videoID);
        }
        
//        video.ytLink = videoDict?["youtube_link1"] as? String
//        video.ytLinkKey =
//        self.rows.append(video);
//
//        video = Video();
//        video.ytLink = videoDict?["youtube_link2"] as? String
//        video.ytLinkKey = videoDict?["youtube_link2_key"] as? String
//        self.rows.append(video);
        
        
    }
    @IBAction func selectVideo(_ sender: Any) {
        let tag = (sender as! UIControl).tag
        
        if tag == 1 {
            self.video1Selected = !self.video1Selected
            UIView.animate(withDuration: 0.30) {  [weak self] in
                if let weakSelf = self {
                    if weakSelf.video1Selected {
                        weakSelf.dummyView1.alpha = 1
                    } else {
                        weakSelf.dummyView1.alpha = 0
                    }
                }
            }
            
        } else {
            self.video2Selected = !self.video2Selected
            UIView.animate(withDuration: 0.30) { [weak self] in                
                if let weakSelf = self {
                    if weakSelf.video2Selected {
                        weakSelf.dummyView2.alpha = 1
                    } else {
                        weakSelf.dummyView2.alpha = 0
                    }
                }
            }
        }
        
    }
    
    @IBAction func playVideo(_ sender: Any) {
        let tag = (sender as! UIView).tag
        
        if tag == 101 {
            self.ytView1.playVideo()
            self.ytView2.stopVideo()
        }
        
        if tag == 102 {
            self.ytView1.stopVideo()
            self.ytView2.playVideo()
        }
    }
    
    @objc func shareVideo(sender: Any?) {
        
        var items = [String]()
        
//        self.rows.forEach { (videoObj) in
//            if let video = videoObj {
//                if video.isSelected, let link =  video.ytLink {
//                    items.append(link)
//                }
//            }
//        }
        var count = 0
        if self.video1Selected || self.video2Selected {
            count = count + 1
            if let ytLink = self.videoDict?["youtube_link1"] as? String {
                items.append(ytLink)
            }
        }
        if self.video2Selected {
            count = count + 1
            if let ytLink = self.videoDict?["youtube_link2"] as? String {
                items.append(ytLink)
            }
        }
        if count > 0 {
            self.shareItems(withItems: items)
        } else {
            self.makeToast(strMessage: "Please select atleast one video")
        }
    }
    
    deinit {
        self.ytView1 = nil
        self.ytView2 = nil
    }
}

//extension VideoGalleryVC: UITableViewDataSource,UITableViewDelegate {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.rows.count;
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "VideoCell")
//
//        if let isSel = self.rows[indexPath.row]?.isSelected {
//            if isSel {
//                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none) // (3)
//            } else {
//                tableView.deselectRow(at: indexPath, animated: false) // (4)
//            }
//        }
//        let playerView = cell?.contentView.viewWithTag(101) as? YTPlayerView
//        if let videoID = self.rows[indexPath.row]?.ytLinkKey {
//            playerView?.load(withVideoId: videoID);
//        }
//        return cell ?? UITableViewCell();
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.rows[indexPath.row]?.isSelected = true
//    }
//
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        self.rows[indexPath.row]?.isSelected = false
//    }
//}
