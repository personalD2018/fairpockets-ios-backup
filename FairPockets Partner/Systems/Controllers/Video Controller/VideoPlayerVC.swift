//
//  VideoPlayerVC.swift
//  Fairpockets Partner
//
//  Created by Ashim Samanta on 16/10/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class VideoPlayerVC: UIViewController {

    @IBOutlet weak var videoPlayer: UIWebView!
    var videoLink: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "player"))
        
        if let link = self.videoLink {
            guard
                let youtubeURL = URL(string: "https://www.youtube.com/embed/\(link)")
                else { return }
            self.videoPlayer.loadRequest(URLRequest(url: youtubeURL));
        }
        // Do any additional setup after loading the view.
    }
}
