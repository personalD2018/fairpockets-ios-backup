//
//  InventoryDetailsVC.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 15/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import NVActivityIndicatorView
import AlamofireSwiftyJSON
import SwiftyJSON

class InventoryDetailsVC: UIViewController,NVActivityIndicatorViewable {

    //MARK:- Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet var lblSearchProjects: UILabel!
    @IBOutlet var lblSelectBuilder: UILabel!
    
    @IBOutlet var lblBuilderValue: UILabel!
    @IBOutlet var builderImg: UIImageView!
    
    @IBOutlet var lblSelectProject: UILabel!
    @IBOutlet var lblProjectValue: UILabel!
    @IBOutlet var ProjectImg: UIImageView!
    
    @IBOutlet var lblSelectTower: UILabel!
    @IBOutlet var lblTowerValue: UILabel!
    @IBOutlet var TowerImg: UIImageView!
    
    @IBOutlet var lblSelectWing: UILabel!
    @IBOutlet var lblWingValue: UILabel!
    @IBOutlet var WingImg: UIImageView!
    @IBOutlet var WingView:UIView!
    
    @IBOutlet weak var lblConfiguration: UILabel!
    @IBOutlet weak var lblConfValue: UILabel!
    @IBOutlet weak var ConfImg: UIImageView!
    @IBOutlet weak var confView: xUIView!
    
    @IBOutlet weak var lblSelectStatus: UILabel!
    @IBOutlet weak var statusView: xUIView!
    @IBOutlet weak var statusImg: UIImageView!
    @IBOutlet weak var lblStatusValue: UILabel!
    
    @IBOutlet var btnSubmit: UIButton!
    
    @IBOutlet var lblBuilderTopConstraint: NSLayoutConstraint!
    @IBOutlet var lblBuilderHeightConstraint: NSLayoutConstraint!
    @IBOutlet var BuilderDropdownHeightConstraint: NSLayoutConstraint!
    @IBOutlet var builderDownImg: UIImageView!
    
    @IBOutlet weak var lblWingtopCnstrnt: NSLayoutConstraint!
    @IBOutlet weak var lblWingHeightCnstrnt: NSLayoutConstraint!
    @IBOutlet weak var drpdwnTopCnstrnt: NSLayoutConstraint!
    @IBOutlet weak var drodwnHeightCnstrnt: NSLayoutConstraint!
    
    var BulderDD = DropDown()
    var BuilderList:NSArray = NSArray()
    var theBuilderList:[String] = [String]()
    var BuilderIsOpen = false
    var builderSelectedId = -1
    
    var ProjectDD = DropDown()
    var ProjectList:NSArray = NSArray()
    var theProjectList:[String] = [String]()
    var ProjectSelectedId = -1
    
    var TowerDD = DropDown()
    var TowerList:NSArray = NSArray()
    var theTowerList:[String] = [String]()
    var TowerSelectedId = -1
    
    var WingDD = DropDown()
    var WingList:NSArray = NSArray()
    var theWingList:[String] = [String]()
    var WingSelectedId = -1
    
    var dropDownConfiguration = DropDown()
    var configurationList: NSArray?
    var confNameList:[String] = [String]()
    var confSelectedId = -1
    
    var dropDownStatus = DropDown()
    var statusList:NSArray?
    var statusNameList:[String] = [String]()
    var statusSelectedId = -1
    
    
    let selectPlaceHolder = mapping.string(forKey: "Select_key")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButtonAndMenuButton(titleText: "Builder-Broker Management")
        setPlaceHolder()
        if(UserType.caseInsensitiveCompare("sales") == ComparisonResult.orderedSame){
            isSales = true
            lblBuilderTopConstraint.constant = 0
            lblBuilderHeightConstraint.constant = 0
            BuilderDropdownHeightConstraint.constant = 0
            builderDownImg.isHidden = true
            getProjectList()
        } else {
            isSales = false
            lblBuilderTopConstraint.constant = 30
            lblBuilderHeightConstraint.constant = 18
            BuilderDropdownHeightConstraint.constant = 40
            builderDownImg.isHidden = false
            getBuilderList()
        }
        self.hideWingDropDownField()
        print("End")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configDropdown(dropdown: BulderDD, sender: lblBuilderValue)
        configDropdown(dropdown: ProjectDD, sender: lblProjectValue)
        configDropdown(dropdown: TowerDD, sender: lblTowerValue)
        configDropdown(dropdown: WingDD, sender: lblWingValue)
        configDropdown(dropdown: dropDownConfiguration, sender: lblConfValue)
        configDropdown(dropdown: dropDownStatus, sender: lblStatusValue)
    }
    
    func setPlaceHolder() {
        
        lblSearchProjects.text = mapping.string(forKey: "SearchProjects_key")
        lblSelectBuilder.text = mapping.string(forKey: "SelectBuilder_key")
        
        lblBuilderValue.text = mapping.string(forKey: "Select_key")
        
        lblSelectProject.text = mapping.string(forKey: "SelectProject_key")
        lblProjectValue.text = mapping.string(forKey: "Select_key")
        
        lblSelectTower.text = mapping.string(forKey: "SelectTower_key")
        lblTowerValue.text = mapping.string(forKey: "Select_key")
        
        lblSelectWing.text = mapping.string(forKey: "SelectWing_key")
        lblWingValue.text = mapping.string(forKey: "Select_key")
        
        lblConfiguration.text = mapping.string(forKey: "SelectConf_key")
        lblConfValue.text = ""//mapping.string(forKey: "Select_key")
        
        lblSelectStatus.text = mapping.string(forKey: "SelectStatus_key")
        lblStatusValue.text = ""//mapping.string(forKey: "Select_key")
        
        btnSubmit.setTitle(mapping.string(forKey: "Submit_key"), for: .normal)
    }
    
    func resetProjectDropDownField() {
        self.ProjectList = NSArray()
        self.theProjectList.removeAll()
        self.ProjectSelectedId = -1
    }
    
    func resetTowerDropDownField() {
        self.TowerList = NSArray()
        self.theTowerList.removeAll()
        self.TowerSelectedId = -1
    }
    
    func resetWingDropDownField() {
        self.WingList = NSArray()
        self.theWingList.removeAll()
        self.WingSelectedId = -1
    }
    
    func resetConfigurationDropDownField() {
        self.configurationList = NSArray()
        self.confNameList.removeAll()
        self.confSelectedId = -1
    }
    
    func resetStatusDropDownField() {
        self.statusList = NSArray()
        self.statusNameList.removeAll()
        self.statusSelectedId = -1
    }
    
    func hideWingDropDownField() {
        self.lblSelectWing.isHidden = true
        self.lblWingValue.isHidden = true
        self.WingImg.isHidden = true
        self.WingView.isHidden = true
        self.contentView.layoutIfNeeded()
        self.lblWingtopCnstrnt.constant = 0
        self.lblWingHeightCnstrnt.constant = 0
        self.drpdwnTopCnstrnt.constant = 0
        self.drodwnHeightCnstrnt.constant = 0
        UIView.animate(withDuration: 0.30) {
            self.contentView.layoutIfNeeded()
        }
    }
    
    func showWingDropDownField() {
        self.lblSelectWing.isHidden = false
        self.lblWingValue.isHidden = false
        self.WingImg.isHidden = false
        self.WingView.isHidden = false
        self.contentView.layoutIfNeeded()
        self.lblWingtopCnstrnt.constant = 30
        self.lblWingHeightCnstrnt.constant = 18
        self.drpdwnTopCnstrnt.constant = 8
        self.drodwnHeightCnstrnt.constant = 40
        UIView.animate(withDuration: 0.30) {
            self.contentView.layoutIfNeeded()
        }
    }
    
    func showDropDown() {
        BulderDD.dataSource = theBuilderList
        BulderDD.show()
        BulderDD.selectionAction = {[weak self] (index,item) in
            self?.lblBuilderValue.text! = item
            self?.builderSelectedId = index
            self?.ProjectSelectedId = -1
            self?.TowerSelectedId = -1
            self?.WingSelectedId = -1
            self?.confSelectedId = -1
            self?.statusSelectedId = -1
            self?.confSelectedId = -1
            self?.statusSelectedId = -1
            self?.getProjectList()
        }
    }
    
    func showProjectDropDown() {
        ProjectDD.dataSource = theProjectList
        ProjectDD.show()
        ProjectDD.selectionAction = {[weak self] (index,item) in
            self?.lblProjectValue.text! = item
            self?.TowerSelectedId = -1
            self?.WingSelectedId = -1
            self?.confSelectedId = -1
            self?.statusSelectedId = -1
            self?.ProjectSelectedId = index
            
            self?.getTowerList()
        }
    }
    
    func showTowerDropDown(){
        TowerDD.dataSource = theTowerList
        TowerDD.show()
        TowerDD.selectionAction = {[weak self] (index,item) in
            self?.lblTowerValue.text! = item
            self?.WingSelectedId = -1
            self?.confSelectedId = -1
            self?.statusSelectedId = -1
            self?.TowerSelectedId = index
            self?.getWingList()
        }
    }
    
    func showWingDropDown(){
        WingDD.dataSource = theWingList
        WingDD.show()
        WingDD.selectionAction = {[weak self] (index,item) in
            self?.lblWingValue.text = item
            self?.WingSelectedId = index
            self?.confSelectedId = -1
            self?.statusSelectedId = -1
            self?.getConfigurationList()
        }
    }
    
    func showConfigurationDropDown(){
        dropDownConfiguration.dataSource = confNameList
        dropDownConfiguration.show()
        dropDownConfiguration.selectionAction = {[weak self] (index,item) in
            self?.lblConfValue.text = item
            self?.confSelectedId = index
            self?.statusSelectedId = -1
            self?.getStatusList()
        }
    }
    
    func showStatusDropDown(){
        dropDownStatus.dataSource = statusNameList
        dropDownStatus.show()
        dropDownStatus.selectionAction = {[weak self] (index,item) in
            self?.lblStatusValue.text = item
            self?.statusSelectedId = index
        }
    }
    
    func getBuilderList() {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType as AnyObject
       
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            // self.ProjectList.removeAll()
            self.ProjectList = NSArray()
            self.theProjectList.removeAll()
            self.ProjectSelectedId = Int()
            self.lblBuilderValue.text = mapping.string(forKey: "Select_key")
            self.lblProjectValue.text = ""
            self.lblTowerValue.text = ""
            self.lblWingValue.text = ""
            lblConfValue.text = ""
            lblStatusValue.text = ""
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvBuilderList(param: para,withHandler: {[weak self] (_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self?.BuilderList = result
                            print("projectlist",self?.BuilderList)
                            for i in 0..<result.count
                            {
                                if let dict = result.object(at: i) as? NSDictionary{
                                    if let projName = dict.value(forKey: "builder_name") as? String
                                    {
                                        self?.theBuilderList.append(projName)
                                    }
                                }
                            }
                            print("The Builder list",self?.theBuilderList)
                            
                        }
                    }
                }
                else
                {
                    
                }
            })
            
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func getDetailsList() {
      var para = [String:AnyObject]()
        
        if(isSales) {
            para["builder_id"] = Builder_id as AnyObject
        } else {
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary{
              para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
        }
        
        if let dict = ProjectList.object(at: ProjectSelectedId) as? NSDictionary{
            para["project_id"] = dict.value(forKey: "project_id") as AnyObject
        }
        
        if let dict = TowerList.object(at: TowerSelectedId) as? NSDictionary {
            para["inventory_id"] = dict.value(forKey: "inventory_id") as AnyObject
        }
        
        if let confList = self.configurationList, let confDict = confList.object(at: self.confSelectedId) as? NSDictionary {
            para["inv_area"] = confDict.value(forKey: "flat_area") as AnyObject
        }
        
        if let sList = self.statusList, let confDict = sList.object(at: self.statusSelectedId) as? NSDictionary {
            para["inv_status"] = confDict.value(forKey: "flat_status") as AnyObject
        }
        
        para["user_id"] = UserId as AnyObject
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvDetails(param: para,withHandler: { [weak self] (_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1  {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            let next = self?.storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
                            next.arrDetails = result
                            next.params = para
                            if let shouldEdit = dict.value(forKey: "user_inv_permission_stat") as? NSNumber, shouldEdit == 1 {
                                next.shouldEdit = true;
                            }
                            
                            if let sList = self?.statusList, let confDict = sList.object(at: (self?.statusSelectedId)!) as? NSDictionary {
                               next.invStatus = confDict.value(forKey: "flat_status")
                            }
                            self?.navigationController?.pushViewController(next, animated: true)
                        }
                    }
                }
            })
        } else {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func getWingList() {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType.lowercased() as AnyObject
        print(TowerList)
        
        if let dict = TowerList.object(at: TowerSelectedId) as? NSDictionary {
            para["inventory_id"] = dict.value(forKey: "inventory_id") as AnyObject
            para["tower_name"] = dict.value(forKey: "tower_name") as AnyObject
        }
        
        if(isSales) {
            para["builder_id"] = Builder_id as AnyObject
        } else {
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary {
                para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            self.resetWingDropDownField()
            self.resetConfigurationDropDownField()
            self.resetStatusDropDownField()
            
            self.lblWingValue.text = mapping.string(forKey: "Select_key")
            lblConfValue.text = ""
            lblStatusValue.text = ""
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvWingList(param: para,withHandler: {[weak self](_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1 {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self?.WingList = result
                            for i in 0..<result.count
                            {
                                if let dict = result.object(at: i) as? NSDictionary{
                                    if let wingName = dict.value(forKey: "wing") as? String
                                    {
                                        self?.theWingList.append(wingName)
                                    }
                                }
                            }
                            DispatchQueue.main.async {
                                if (self?.theWingList == [""]){
                                    self?.hideWingDropDownField()
                                } else {
                                    self?.showWingDropDownField()
                                }
                            }
                        }
                    }
                } else {
                    self?.getConfigurationList()
                }
            })
        }  else {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func getTowerList() {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType.lowercased() as AnyObject
         if let dict = ProjectList.object(at: ProjectSelectedId) as? NSDictionary{
            para["project_id"] = dict.value(forKey: "project_id") as AnyObject
        }
        if(isSales) {
            para["builder_id"] = Builder_id as AnyObject
        } else {
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary{
                para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            self.resetTowerDropDownField()
            self.resetWingDropDownField()
            self.resetConfigurationDropDownField()
            self.resetStatusDropDownField()
            
            self.lblTowerValue.text = mapping.string(forKey: "Select_key")
            self.lblWingValue.text = ""
            lblConfValue.text = ""
            lblStatusValue.text = ""
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvTowerList(param: para,withHandler: { [weak self] (_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1 {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                           self?.TowerList = result
                            for i in 0..<result.count
                            {
                                if let dict = result.object(at: i) as? NSDictionary{
                                    if let towerName = dict.value(forKey: "tower_name") as? String
                                    {
                                        self?.theTowerList.append(towerName)
                                    }
                                }
                            }
                        }
                    }
                }
            })
        } else {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func getProjectList() {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        
        if(isSales) {
            para["user_type"] = UserType.lowercased() as AnyObject
            para["builder_id"] = "0" as AnyObject
        }else{
            para["user_type"] = UserType as AnyObject
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary{
                para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            self.resetProjectDropDownField()
            self.resetTowerDropDownField()
            self.resetWingDropDownField()
            self.resetConfigurationDropDownField()
            self.resetStatusDropDownField()
            
            self.lblProjectValue.text = mapping.string(forKey: "Select_key")
            self.lblTowerValue.text = ""
            self.lblWingValue.text = ""
            self.lblConfValue.text = ""
            lblStatusValue.text = ""
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvProjectList(para:para, withHandler: { [weak self] (_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1 {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                             self?.ProjectList = result
                            for i in 0..<result.count
                            {
                                if let dict = result.object(at: i) as? NSDictionary{
                                    if let projName = dict.value(forKey: "project_name") as? String
                                    {
                                        self?.theProjectList.append(projName)
                                    }
                                }
                            }
                        }
                    }
                }
            })
        } else {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func getConfigurationList() {
       
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType.lowercased() as AnyObject
        if let dict = ProjectList.object(at: ProjectSelectedId) as? NSDictionary{
            para["project_id"] = dict.value(forKey: "project_id") as AnyObject
        }
        
        if let dict = TowerList.object(at: TowerSelectedId) as? NSDictionary {
            para["inventory_id"] = dict.value(forKey: "inventory_id") as AnyObject
        }
        
        if WingList.count > 0 {
            para["status"] = "wing" as AnyObject
        } else {
            para["status"] = "tower" as AnyObject
        }
        
        if(isSales) {
            para["builder_id"] = Builder_id as AnyObject
        } else {
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary {
                para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            self.resetConfigurationDropDownField()
            self.resetStatusDropDownField()
            self.lblConfValue.text = mapping.string(forKey: "Select_key")
            lblStatusValue.text = ""
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getDetail(url:aBasePostInvConfigurationListURl, param: para,withHandler: {[weak self](_ status: Int, _ data: Any?) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1 {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self?.configurationList = result
                            for i in 0..<result.count {
                                if let dict = result.object(at: i) as? NSDictionary {
                                    if let confName = dict.value(forKey: "flat_area") as? String {
                                        self?.confNameList.append(confName + "sq ft")
                                    }
                                }
                            }
                        }
                    }
                }
            })
        } else {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func getStatusList() {
        
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType.lowercased() as AnyObject
        if let dict = ProjectList.object(at: ProjectSelectedId) as? NSDictionary{
            para["project_id"] = dict.value(forKey: "project_id") as AnyObject
        }
        
        if let dict = TowerList.object(at: TowerSelectedId) as? NSDictionary {
            para["inventory_id"] = dict.value(forKey: "inventory_id") as AnyObject
        }
        
        if WingList.count > 0 {
            para["status"] = "wing" as AnyObject
        } else {
            para["status"] = "tower" as AnyObject
        }
        
        if let confList = self.configurationList, let confDict = confList.object(at: self.confSelectedId) as? NSDictionary {
            para["inv_area"] = confDict.value(forKey: "flat_area") as AnyObject
        }
        
        if(isSales) {
            para["builder_id"] = Builder_id as AnyObject
        } else {
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary {
                para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            self.resetStatusDropDownField()
            self.lblStatusValue.text = mapping.string(forKey: "Select_key")
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getDetail(url:aBasePostInvStatusListURl, param: para,withHandler: {[weak self](_ status: Int, _ data: Any?) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1 {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self?.statusList = result
                            for i in 0..<result.count {
                                if let dict = result.object(at: i) as? NSDictionary {
                                    if let status = dict.value(forKey: "flat_status") as? NSNumber {
                                        if status == 8 {
                                            self?.statusNameList.append(invTotal);
                                        }
                                    }
                                    
                                    if let status = dict.value(forKey: "flat_status") as? String {
                                        
                                        switch status {
                                            case "1" :
                                                self?.statusNameList.append(invAvailable)
                                            
                                            case "2" :
                                                self?.statusNameList.append(invNotAvailable)
                                            
                                            case "3" :
                                                self?.statusNameList.append(invOnHold)
                                            
                                            case "4" :
                                                self?.statusNameList.append(invInternalHold)
                                            
                                            case "8" :
                                                self?.statusNameList.append(invTotal)
                                            
                                            default: break
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
        }  else {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    //#MARK:- Config DropDown
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
    
    //#MARK:- Button Action
    
    @IBAction func btnSelectBuilderListAction(_ sender: UIButton) {
     
        if(theBuilderList.count > 0)
        {
            showDropDown()
        }
        
    }
    @IBAction func ProjectListAction(_ sender: Any) {
        if theProjectList.count > 0 {
            showProjectDropDown()
        }
    }
 
    @IBAction func btnSelectTowerAction(_ sender: UIButton) {
        if theTowerList.count > 0 {
           showTowerDropDown()
        }
    }
    @IBAction func btnSelectWingAction(_ sender: UIButton) {
        if theWingList.count > 0 {
             showWingDropDown()
        }
    }
    
    @IBAction func btnConfigurationAction(_ sender: Any) {
        if self.confNameList.count > 0 {
            showConfigurationDropDown()
        }
    }
    
    @IBAction func btnStatusAction(_ sender: Any) {
        if statusNameList.count > 0 {
            showStatusDropDown()
        }
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if(!isSales) {
            guard self.builderSelectedId != -1 && self.lblBuilderValue.text! != selectPlaceHolder else {
                self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectBuilder_key"))
                return
            }
        }
        guard self.ProjectSelectedId != -1 && self.lblProjectValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectProject_key"))
            return
        }
        guard self.TowerSelectedId != -1 && self.lblTowerValue.text != "" && self.lblTowerValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectTower_key"))
            return
        }
       if (self.WingView.isHidden==false){
        guard  self.WingSelectedId != -1 && self.lblWingValue.text != "" && self.lblWingValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectWing_key"))
            return
        }
        guard self.confSelectedId != -1 && self.lblConfValue.text != "" && self.lblConfValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectConf_key"))
            return
        }
    
        guard self.statusSelectedId != -1 && self.lblStatusValue.text != "" && self.lblStatusValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectStatus_key"))
            return
        }
    }
        self.getDetailsList()
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
