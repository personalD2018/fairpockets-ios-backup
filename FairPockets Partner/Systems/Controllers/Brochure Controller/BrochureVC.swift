//
//  BrochureVC.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 22/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//
import UIKit
import Alamofire
import NVActivityIndicatorView
import SkyFloatingLabelTextField
import SwiftyJSON

class BrochureVC: FPBaseViewController, NVActivityIndicatorViewable, UITextFieldDelegate  {
    @IBOutlet var txtSearch: SkyFloatingLabelTextField!
    @IBOutlet var tbl_Brochure: UITableView!
    @IBOutlet var lblNoData: UILabel!
    var Brochure:NSArray = NSArray()
    var SearchBrochureArr:NSMutableArray = NSMutableArray()
    var BrochureShortedarr:NSMutableArray = NSMutableArray()
    var search:String=String()
    var isSearch = false
    var countVal:Int! = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar(titleText: mapping.string(forKey: "Brochure_key"))
        self.txtSearch.toolbarPlaceholder = mapping.string(forKey: "SearchInquiry_key")
        lblNoData.text = mapping.string(forKey: "NoClientshistry_key")
        self.tbl_Brochure.dataSource = self
        self.tbl_Brochure.delegate = self
        self.txtSearch.delegate = self
        tbl_Brochure.register(UINib(nibName: "PriceListCell", bundle: nil), forCellReuseIdentifier: "PriceListTableViewCell")
        
        tbl_Brochure.tableFooterView = UIView()
        tbl_Brochure.estimatedRowHeight = 78
        tbl_Brochure.rowHeight = UITableViewAutomaticDimension
        lblNoData.isHidden = true
//        txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.getAllBrochureList()
        
        var param = [String:AnyObject]()
        param["user_id"] = UserId as AnyObject
        param["status"] = "brochure_list" as AnyObject
        param["read"] = countVal as AnyObject
        param["count"] = countVal as AnyObject
        
        ServerRequestHandler.sharedappController.getMessageNotificationRead(para: param, withHandler: {(_ status: Int, _ data: NSObject) -> Void in
            if status == 1
            {
                if let dict = (data) as? NSDictionary {
                    
                }
            }
            else
            {
                
            }
        })
    }

    func getAllBrochureList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        if (UserType == "Sales"){
            para["user_type"] = UserType.lowercased() as AnyObject
            
        }else{
            para["user_type"] = UserType as AnyObject
        }
       

        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            // self.ProjectList.removeAll()
            self.Brochure = NSArray()
            
            
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            ServerRequestHandler.sharedappController.getBrochureList(para: para,withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                self.stopAnimating()
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self.Brochure = result
                            if self.Brochure.count>0{
                                self.BrochureShortedarr.removeAllObjects()
                                for priceDic in self.Brochure {
                                    let tempDict:NSMutableDictionary = NSMutableDictionary()
                                    let json = JSON(priceDic as Any)
                                    tempDict["builder_name"] = json["Websiteuser"]["builder_name"].stringValue
                                    tempDict["project_name"] = json["Project"]["project_name"].stringValue
                                    
                                    
                                    tempDict["UpdatedDate"] = json["ProjectOfficeUse"]["UpdatedDate"].stringValue
                                    tempDict["brochure_list"] = json["ProjectOfficeUse"]["brochure_list"].stringValue
                                    self.BrochureShortedarr.add(tempDict)
                                    self.SearchBrochureArr = self.BrochureShortedarr.mutableCopy() as! NSMutableArray
                                }
                            }
                            self.tbl_Brochure.reloadData()
                        }
                    }
                }
                
            })
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        print("the text is --------------0",self.txtSearch.text!)
        if string.isEmpty
        {
            search = String(search.characters.dropLast())
        }
        else
        {
            search=textField.text!+string
        }
        let predicate=NSPredicate(format: "SELF.builder_name CONTAINS[cd] %@ or SELF.project_name CONTAINS[cd] %@", search,search)
        print(predicate)
        let arr=(self.BrochureShortedarr as NSArray).filtered(using: predicate)
        
        print(arr)
        if arr.count > 0
        {
            self.SearchBrochureArr.removeAllObjects()
            for i in 0 ..< arr.count
            {
                SearchBrochureArr.add(arr[i])
            }
        }
        else
        {   if search==""
        { SearchBrochureArr =  NSMutableArray()
            
            SearchBrochureArr = self.BrochureShortedarr.mutableCopy() as! NSMutableArray
        }
        else{
            SearchBrochureArr =  NSMutableArray()
            }
        }
        tbl_Brochure.reloadData()
        return true
    }

    

}


extension BrochureVC: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.SearchBrochureArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PriceListTableViewCell") as! PriceListTableViewCell
        if self.SearchBrochureArr.count>0{
            if let dict:NSDictionary = self.SearchBrochureArr[indexPath.row] as! NSDictionary{
                cell.coffigureCell(myDate: dict)
                cell.btnOpenPdf.tag = indexPath.row
                cell.btnSharePdf.tag = indexPath.row
                cell.btnOpenPdf.addTarget(self, action: #selector(btnOpenPDFAction(sender:)), for: .touchUpInside)
                cell.btnSharePdf.addTarget(self, action: #selector(btnSharePdf(sender:)), for: .touchUpInside)
            }
        }
        return cell
    }
    
    
    
    @objc func btnSharePdf(sender: UIButton){
        let index = sender.tag
        if self.SearchBrochureArr.count>0{
            if  let postDetails:NSDictionary = self.SearchBrochureArr[index] as? NSDictionary {
                let json = JSON(postDetails as Any)
                let pdf_name = json["brochure_list"].stringValue
                let urlString = "https://www.fairpockets.com/upload/project_office_brochure/" + pdf_name
                self.startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
                FPFileManager().downloadFile(fromURL: urlString) { [weak self] (data, fileUrl) in
                    DispatchQueue.main.async {
                        self?.stopAnimating()
                        self?.sharePDF(fileUrl: fileUrl)
                    }
                }
            }
        }
    }
    
    //
    @objc func btnOpenPDFAction(sender: UIButton){
        let index = sender.tag
        if self.Brochure.count>0{
            if  let postDetails:NSDictionary = self.Brochure[index] as? NSDictionary {
                let json = JSON(postDetails as Any)
//                if let dict = json["ProjectOfficeUse"] as? NSDictionary{
//
//                }
              //  let dict = json["ProjectOfficeUse"]
                let dict = postDetails.value(forKey: "ProjectOfficeUse") as? NSDictionary
                let pdf_name:String! = dict?.value(forKey: "brochure_list") as? String
                if (pdf_name == ""){
                  makeToast(strMessage: "No PDF Found To View")
                } else {
                    let pdf_url = "https://www.fairpockets.com/upload/project_office_brochure/" + pdf_name
                    let pdfView = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewController") as! PDFViewController
                    pdfView.urls = pdf_url
                    self.navigationController?.pushViewController(pdfView, animated: true)
                }
            }
            
        }
    }
    
}

