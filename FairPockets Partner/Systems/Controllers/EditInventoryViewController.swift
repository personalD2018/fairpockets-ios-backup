//
//  EditInventoryViewController.swift
//  Fairpockets Partner
//
//  Created by Ashim Samanta on 19/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class EnvOption {
    var isSelected:Bool
    var text: String
    
    init() {
        isSelected = false
        text = "";
    }
}

class EditInventoryViewController: UIViewController, NVActivityIndicatorViewable {
    var dict:NSDictionary?
    var params = [String:AnyObject]()
    var floor_id:String = ""
    var options = [EnvOption]();
    typealias _handler = (Bool) -> Void
    var compHandler:_handler?
    @IBOutlet weak var tblVW: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var opt = EnvOption();
        opt.isSelected = true;
        opt.text = "Available"
        options.append(opt);
        
        opt = EnvOption();
        opt.isSelected = false;
        opt.text = "On-Hold"
        options.append(opt);
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    @IBAction func saveChoice(_ sender: Any) {
        let option = self.options[1];
        if !option.isSelected {
            if let compHand = self.compHandler {
                compHand(true);
                return;
            }
        }
        var param = [String:AnyObject]()
        param["user_id"] = self.params["user_id"]
        param["inventory_id"] = self.params["inventory_id"]
        param["floor_no"] = self.floor_id as AnyObject
        param["flat_no"] = self.dict!["flat_no"] as AnyObject
        param["flat_status"] = "3" as AnyObject
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            self.startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            NetworkActivityManager.sharedInstance.busy = true
            
            ServerRequestHandler.sharedappController.getInvStatus(param: param ,withHandler: { [weak self] (_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                if status == 1  {
                    print(data);
                    self?.dismiss(animated: true, completion: nil);
                    if let compHand = self?.compHandler {
                        compHand(true);
                    }
                    self?.makeToast(strMessage: "Success");
                } else {
                    self?.makeToast(strMessage: "Failed");
                }
            })
        } else {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }
    
    func setComplitionHandler(handler:@escaping _handler) {
        self.compHandler = handler;
    }
}

extension EditInventoryViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35.0;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionCell") as! EntrySelectionCell
        cell.tag = indexPath.row
        
        let option = self.options[indexPath.row];
        
        if (option.isSelected) {
            cell.imgVw.image = UIImage(named: "radio_checked");
        } else {
            cell.imgVw.image = UIImage(named: "radio_normal");
        }
        cell.label.text = option.text;
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        switch indexPath.row {
        case 0:
            self.options[0].isSelected = true;
            self.options[1].isSelected = false;
            
        case 1:
            self.options[0].isSelected = false;
            self.options[1].isSelected = true;
        default:
            break
        }
        let indx1 = IndexPath(row: 0, section: 0)
        let indx2 = IndexPath(row: 1, section: 0)
        
        tableView.reloadRows(at: [indx1, indx2], with: .automatic);
    }
    
}
