//
//  ViewExtention.swift
//  freightsoftware
//
//  Created by DK on 21/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation

extension UIView {
    func viewNextPresentingViewController() -> UIViewController? {
        var theController = self.next
        
        while theController != nil && !(theController?.isKind(of: UIViewController.classForCoder()))! {
            theController = theController?.next
        }
        
        return theController as? UIViewController
    }
}
