//
//  ButtonDesinable.swift
//  UseOnlyXib
//
//  Created by Bhavesh on 12/06/17.
//  Copyright © 2017 Dignizant. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable open class ButtonDesinable : UIButton {
	
    @IBInspectable open var cornerRadius: CGFloat = 0.0 {
	      didSet {
		     layer.cornerRadius = cornerRadius
		     layer.masksToBounds = cornerRadius > 0
		}
    }

	@IBInspectable open var bordercolor : UIColor = .red{
		didSet{
			layer.borderColor = bordercolor.cgColor
		}
	}
	
	@IBInspectable open var borderWidth : CGFloat = 0.0{
		didSet{
			layer.borderWidth = borderWidth
		}
	}

}

@IBDesignable open class labelDesinable : UILabel {
	
	@IBInspectable open var cornerRadius: CGFloat = 0.0 {
		didSet {
			layer.cornerRadius = cornerRadius
			layer.masksToBounds = cornerRadius > 0
		}
	}
	
	@IBInspectable open var bordercolor : UIColor = .red{
		didSet{
			layer.borderColor = bordercolor.cgColor
		}
	}
	
	@IBInspectable open var borderWidth : CGFloat = 0.0{
		didSet{
			layer.borderWidth = borderWidth
		}
	}
	
	
}

@IBDesignable open class TextFieldDesinable : UITextField{
	
	@IBInspectable var borderColor: UIColor = UIColor.clear {
		didSet {
			layer.borderColor = borderColor.cgColor
		}
	}
	
	@IBInspectable var borderWidth: CGFloat = 0 {
		didSet {
			layer.borderWidth = borderWidth
		}
	}
	
	@IBInspectable var cornerRadius: CGFloat = 0 {
		didSet {
			layer.cornerRadius = cornerRadius
		}
	}
	
	@IBInspectable var placeHolderColor : UIColor = UIColor.lightGray{
		didSet {
			setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
		}
	}

	@IBInspectable var paddingLeft: CGFloat = 0
	@IBInspectable var paddingRight: CGFloat = 0
	
	override open func textRect(forBounds bounds: CGRect) -> CGRect {
		return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y,
		              width: bounds.size.width - paddingLeft - paddingRight, height: bounds.size.height);
	}
	
	override open func editingRect(forBounds bounds: CGRect) -> CGRect {
		return textRect(forBounds: bounds)
	}
	
	@IBInspectable var bottomLineWidth : CGFloat = 1 {
		didSet{
			let border: CALayer = CALayer()
			border.borderColor = UIColor.darkGray.cgColor
			self.frame = CGRect(x: 0, y: self.frame.size.height - bottomLineWidth, width: self.frame.size.width, height: self.frame.size.height)
			border.borderWidth = borderWidth
			self.layer.addSublayer(border)
			self.layer.masksToBounds = true
		}
	}
	
	@IBInspectable var bottomLineColor : UIColor = UIColor.clear{
		didSet {
			let border: CALayer = CALayer()
			border.borderColor = bottomLineColor.cgColor
		}
	}
	
}
@IBDesignable open class TextViewDesinable : UITextView{
	
	private let lblplaceholdercolor:UIColor = UIColor.red
	private var lblplaceholder:UILabel!
	
	@IBInspectable var placeholder:String = ""{
		didSet {
			self.text = placeholder
		}
	}
	
	@IBInspectable var placeHolderColor : UIColor = UIColor.lightGray{
		didSet {
			self.textColor = placeHolderColor
		}
	}
	@IBInspectable var BorderColor:UIColor = UIColor.white{
		didSet{
			layer.borderColor = BorderColor.cgColor
		}
	}
	@IBInspectable var BorderWidth:CGFloat = 0.0{
		didSet{
			layer.borderWidth = BorderWidth
		}
	}
	@IBInspectable var CornerRadius:CGFloat = 0.0{
		didSet{
			layer.cornerRadius = CornerRadius
		
		}
	}
	
}
@IBDesignable open class ViewDesinable : UIView{
	@IBInspectable var BorderColor:UIColor = UIColor.white{
		didSet{
			layer.borderColor = BorderColor.cgColor
		}
	}
	@IBInspectable var BorderWidth:CGFloat = 0.0{
		didSet{
			layer.borderWidth = BorderWidth
		}
	}
	@IBInspectable var CornerRadius:CGFloat = 0.0{
		didSet{
			layer.cornerRadius = CornerRadius
			
		}
	}
	@IBInspectable var shadow: Bool {
		get {
			return layer.shadowOpacity > 0.0
		}
		set {
			if newValue == true {
				self.addShadow()
			}
		}
	}
	func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
	               shadowOffset: CGSize = CGSize(width: 1.0, height: 1.0),
	               shadowOpacity: Float = 0.2,
	               shadowRadius: CGFloat = 3.0) {
		layer.shadowColor = shadowColor
		layer.shadowOffset = shadowOffset
		layer.shadowOpacity = shadowOpacity
		layer.shadowRadius = shadowRadius
	}
}
