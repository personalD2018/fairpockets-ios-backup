import SystemConfiguration
import Foundation
import UIKit

public class Reachability {
    class func isConnectedToNetwork()->Bool{
        
        let networkReachability = ReachabilityObjc.reachabilityForInternetConnection()
        let networkStatus = networkReachability?.currentReachabilityStatus()
        if networkStatus == NotReachable {
            //ToastMessage.showMessageWithTitle("Internet Failure", Message: "Please check your internet connectivity its failed.", BackgroundColor: ToastMessage_failed, withDuration: 4.0)
            print("Internet Failure --> Please check your internet connectivity its failed.")
            let Alert = localizedStringForKey(key: "app_name")
            let NetworkNotFound = localizedStringForKey(key: "internet_connection_error")
            let Ok = localizedStringForKey(key: "ok")
            let alert = UIAlertView(title: Alert, message: NetworkNotFound, delegate: nil, cancelButtonTitle: Ok)
            alert.show()
            return false;
        }
        return true;
    }
}
