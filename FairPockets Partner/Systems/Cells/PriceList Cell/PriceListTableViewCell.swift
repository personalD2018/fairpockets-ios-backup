//
//  PriceListTableViewCell.swift
//  Fairpockets Partner
//
//  Created by Pritam Dey on 24/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON

class PriceListTableViewCell: UITableViewCell {
    @IBOutlet var lblBuilderName: UILabel!
    @IBOutlet var lblUpdatedDate: UILabel!
    @IBOutlet var lblProjectName: UILabel!
    @IBOutlet var btnOpenPdf:UIButton!
    @IBOutlet var btnSharePdf:UIButton!
    
    var url:String=""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func coffigureCell(myDate:NSDictionary)
    {
       let json = JSON(myDate)
        lblBuilderName.text = json["builder_name"].stringValue
        lblProjectName.text = json["project_name"].stringValue
        lblUpdatedDate.text = json["UpdatedDate"].stringValue
       
    }
    
   
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
