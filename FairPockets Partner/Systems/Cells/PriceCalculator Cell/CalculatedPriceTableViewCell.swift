//
//  CalculatedPriceTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 07/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class CalculatedPriceTableViewCell: UITableViewCell {
   
    @IBOutlet var lblYourFinalCalculatedPrice: UILabel!
    @IBOutlet var lblCalculatedPrice: UILabel!
    @IBOutlet var lblBaseCharges: labelDesinable!
    @IBOutlet var lblBaseChargesValue: labelDesinable!
    @IBOutlet var lblGSTTotal: labelDesinable!
    @IBOutlet var lblGSTTotalValue: labelDesinable!
    @IBOutlet var lblAdditioanlCharge: labelDesinable!
    @IBOutlet var lblAdditionalChrgeValue: labelDesinable!
    
    @IBOutlet var lblStmpRegistration: labelDesinable!
    
    @IBOutlet var lblStampRegistrationValue: labelDesinable!
    @IBOutlet var lblFinalCalculatedPrice: labelDesinable!
    @IBOutlet var lblFinalCalculatedPriceValue: labelDesinable!
    @IBOutlet var btnSave: xUIButton!
    @IBOutlet weak var btnShareOutlet: UIButton!
    @IBOutlet weak var btnViewOutlet: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        /*lblYourFinalCalculatedPrice.text = "  \(mapping.string(forKey:"YourFinalCalculatedPrice_key"))"
         lblBaseCharges.text = "  \(mapping.string(forKey:"BaseCharges_key"))"
        lblGSTTotal.text = mapping.string(forKey:"GSTTotal_key")
          lblAdditioanlCharge.text = "  \(mapping.string(forKey:"AdditionalCharges_key"))"
        //let stampRegistration = "\(mapping.string(forKey:"Stamp_key")!) "+"+"+" \(mapping.string(forKey:"Stamp_key")!)"
       // lblStmpRegistration.text = stampRegistration
        lblFinalCalculatedPrice.text = "  \(mapping.string(forKey:"FinalCalculatedPrice_key"))"
        btnSave.setTitle(mapping.string(forKey:"Save_key"), for: .normal)*/
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnSave(_ sender: Any) {
        if  let PriceCAlculatorVC =  self.viewNextPresentingViewController() as? PriceCAlculatorViewController
        {
            PriceCAlculatorVC.saveBtnAction()
        }
    }
    
}
