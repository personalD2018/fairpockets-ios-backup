//
//  OptionalChargesTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 05/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class OptionalChargesTableViewCell: UITableViewCell {

    @IBOutlet var lblOptionalCharges: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblOptionalCharges.text = mapping.string(forKey: "OptionalCharges_key")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
