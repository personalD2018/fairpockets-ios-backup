//
//  OtherCalculationTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 05/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class OtherCalculationTableViewCell: UITableViewCell {

    @IBOutlet var CalculateTable: UITableView!
    
    @IBOutlet var OtherTableHeights: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
