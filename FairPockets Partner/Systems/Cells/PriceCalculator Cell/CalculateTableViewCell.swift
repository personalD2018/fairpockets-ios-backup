//
//  CalculateTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 07/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class CalculateTableViewCell: UITableViewCell {
    
    @IBOutlet var TitleView: xUIView!
    @IBOutlet var OtherCalculatedTitle: UILabel!
    
    @IBOutlet var ValueView: xUIView!
    @IBOutlet var OtherCalculatedValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
