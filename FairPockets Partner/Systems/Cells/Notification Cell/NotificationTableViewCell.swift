//
//  NotificationTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 04/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet var lblNotificationTitle: UILabel!
    @IBOutlet var lblNotificationDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(theModel:Notificationdata)
    {
        lblNotificationTitle.text! = theModel.reminderText!
        lblNotificationDate.text! = theModel.reminderDate!
    }
    
}
