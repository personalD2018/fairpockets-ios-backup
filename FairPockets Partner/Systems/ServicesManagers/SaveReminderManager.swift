//
//  SaveReminderManager.swift
//  Builder-Broker Management
//
//  Created by DK on 09/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import ObjectMapper


typealias GetSaveReminderResponse = (_ search: SaveReminderDataClass?) -> ()

/**
 *  This struct helps in Product relates CRUD operations. Mainly interacts with Network Resources.
 */
struct  GetSaveReminderManager {
    
    /**
     Lists products from Server DB.
     
     - parameter completion: returns `ProductResponse` which is having products' array if success otherwise `nil`
     */
    
    func getSaveReminder(param: [String : AnyObject],completion: @escaping GetSaveReminderResponse) {
        // let URLstr = "\(baseUserUrl)"+"driverLogin"
        //let URLString = URLstr.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        //print(URLstr)
        print(aBasePostSaveReminderURl)
        print(param)
        
        
        //Alamofire.request(URLstr, parameters: nil, encoding: URLEncoding.default)
        
        Alamofire.request(aBasePostSaveReminderURl, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: authHeader())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/html"])
            .responseSwiftyJSON { response in
                // debugPrint(response.request!.URL?.absoluteString.stringByRemovingPercentEncoding)
                print(response)
                
                guard
                    let responseJSON = response.result.value,
                    let result = responseJSON.rawString()
                    else {
                        print(response.result.error as Any)
                        completion(nil)
                        return
                }
                
                print(result)
                /* if let data = try? JSONSerialization.data(withJSONObject: result, options: .prettyPrinted),
                 let str = String(data: data, encoding: .utf8) {
                 print("str-->\(str)")
                 }*/
                
                guard result=="null" else{
                    guard
                        let resultData = Mapper<SaveReminderDataClass>().map(JSONString: result)
                        //let resultData = Mapper<GetAlbumsPhotosClass>().mapArray(JSONString: result)
                        else{
                            print(result as Any)
                            completion(nil)
                            return
                    }
                    
                    //  Maps JSON Array of products into Product Object's Array
                    //print(resultData)
                    completion(resultData)
                    return
                }
                
                //popup message no more data found
        }
        
    }
    func CalculatorDataSubmit(json:String,completion: @escaping GetSaveReminderResponse)
    {
        let URLstr = "\(aBasePostSaveReminderURl)"
        print(URLstr)
        let url = URL(string: URLstr)!
        let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        Alamofire.request(request)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/html"])
            .responseSwiftyJSON {
                (response) in
                
                print(response)
                guard
                    let responseJSON = response.result.value,
                    let result = responseJSON.rawString()
                    
                    else {
                        print(response.result.error as Any)
                        completion(nil)
                        return
                }
                print(result)
                
                guard result=="null" else{
                    guard
                        let resultData = Mapper<SaveReminderDataClass>().map(JSONString: result)
                        //let resultData = Mapper<GetProductDisplayClass>().mapArray(JSONString: result)
                        else{
                            print(result as Any)
                            completion(nil)
                            return
                    }
                    
                    //  Maps JSON Array of products into Product Object's Array
                    //print(resultData)
                    completion(resultData)
                    return
                }
        }
        
        
    }
    
    private func authHeader() -> [String : String] {
        return [
            // "X-Authorization": "\(kBaseApiKey)",
            //"Content-Type": "application/x-www-form-urlencoded",
            "Content-Type": "application/x-www-form-urlencoded",
            //"Accept-Language":"\(language)"
        ]
    }
    
}

