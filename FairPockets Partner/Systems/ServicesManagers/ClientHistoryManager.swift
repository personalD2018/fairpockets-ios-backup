//
//  ClientHistoryManager.swift
//  Builder-Broker Management
//
//  Created by DK on 04/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import ObjectMapper


typealias GetClientHistoryResponse = (_ search: MyClientHistoryClass?) -> ()

/**
 *  This struct helps in Product relates CRUD operations. Mainly interacts with Network Resources.
 */
struct  GetClientHistoryManager {
    
    /**
     Lists products from Server DB.
     
     - parameter completion: returns `ProductResponse` which is having products' array if success otherwise `nil`
     */
    
    func getClientHistory(param: [String : AnyObject],completion: @escaping GetClientHistoryResponse) {
        // let URLstr = "\(baseUserUrl)"+"driverLogin"
        //let URLString = URLstr.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        //print(URLstr)
        print(aBasePostClientHistoryURl)
        print(param)
        
        
        //Alamofire.request(URLstr, parameters: nil, encoding: URLEncoding.default)
        
        Alamofire.request(aBasePostClientHistoryURl, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: authHeader())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/html"])
            .responseSwiftyJSON { response in
                // debugPrint(response.request!.URL?.absoluteString.stringByRemovingPercentEncoding)
                print(response)
                
                guard
                    let responseJSON = response.result.value,
                    let result = responseJSON.rawString()
                    
                    else {
                        print(response.result.error as Any)
                        completion(nil)
                        return
                }
                
                //print(result)
                
                guard result=="null" else{
                    guard
                        let resultData = Mapper<MyClientHistoryClass>().map(JSONString: result)
                        else{
                            print(result as Any)
                            completion(nil)
                            return
                    }
                    
                    //  Maps JSON Array of products into Product Object's Array
                    //print(resultData)
                    completion(resultData)
                    return
                }
                
                //popup message no more data found
        }
        
    }
    
    private func authHeader() -> [String : String] {
        return [
            // "X-Authorization": "\(kBaseApiKey)",
            //"Content-Type": "application/x-www-form-urlencoded",
            "Content-Type": "application/x-www-form-urlencoded",
            //"Accept-Language":"\(language)"
        ]
    }
    
}


