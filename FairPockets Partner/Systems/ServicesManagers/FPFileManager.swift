//
//  FPFileManager.swift
//  Fairpockets Partner
//
//  Created by Ashim Samanta on 21/10/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation

struct FPFileManager {
    
    private let fileManager = FileManager.default
    
    func writeFileToDisk(name: String?, fileData: NSData?) -> String? {
        if let paths = getDocumentDirectoryPath(), let fData = fileData, let fName = name {
            let filePath = (paths as NSString).appendingPathComponent(fName)
            
            if fData.write(toFile: filePath, atomically: true) {
                return filePath
            }
        }
        return nil
    }
    
    func readFileFromDisk(name: String?) -> String? {
        if let paths = getDocumentDirectoryPath(), let fName = name {
            let filePath = (paths as NSString).appendingPathComponent(fName)
            if fileManager.fileExists(atPath: filePath) {
                return filePath;
            }
        }
        return nil
    }
    
    func isFileExists(name:String?) -> Bool {
        if let paths = getDocumentDirectoryPath(), let fName = name {
            let filePath = (paths as NSString).appendingPathComponent(fName)
            return fileManager.fileExists(atPath: filePath)
        }
        return false
    }
    
    private func getDocumentDirectoryPath() -> String? {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectoryPath = (paths[0] as NSString).appendingPathComponent("FairpocketsPartner")
        if createDirectory(path: documentsDirectoryPath) {
            return documentsDirectoryPath
        } else {
            return nil
        }
    }
    
    private func createDirectory(path: String) -> Bool {
        if !fileManager.fileExists(atPath: path) {
            do {
                try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                return true;
            } catch {
                print("Unable to write the file.")
                return false
            }
        } else {
            print("Already dictionary created.")
            return true
        }
    }
    
    private func deleteDirectory(path: String)->Bool {
        if fileManager.fileExists(atPath: path) {
            do {
                try fileManager.removeItem(atPath: path)
                return true
            } catch {
                print("Unable to delete the directory.")
                return false
            }
        } else {
            print("No Directory found")
            return false
        }
    }
    
    func downloadFile(fromURL urlString: String, complitionBlock: @escaping ((NSData?, String?) -> Void)) {
        
        let dispatchQueue = DispatchQueue(label: "FairpocketsDownloadingDispatchQueue")
        dispatchQueue.async {
            var fileData: NSData?
            var fileUrl: String?
        
            if let url = URL(string: urlString) {
                let fileName = url.lastPathComponent
                
                var fileNamager = FPFileManager()
                if !fileNamager.isFileExists(name: fileName) {
                    fileData = NSData(contentsOf: url)
                    fileUrl = fileNamager.writeFileToDisk(name: fileName, fileData: fileData)
                } else {
                    fileUrl = fileNamager.readFileFromDisk(name: fileName)
                }
            }
            complitionBlock(fileData, fileUrl)
        }
    }
}
