//
//  ViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 01/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class ViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable {
    //#MARK:- Outlets
    
    @IBOutlet var lblTitleBuilder: UILabel!
    @IBOutlet var lblMobile: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var txtMobileNumber: UITextField!
    @IBOutlet var btnSebdOtp: xUIButton!
    @IBOutlet var btnTermNCondition:UIButton!
    @IBOutlet var btnPrivacyPoloicy:UIButton!
    @IBOutlet var btnAgree:UIButton!
    //#MARK:- Declaration
    
    
    

    //#MARK:- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
         self.navigationController?.isNavigationBarHidden = true
        [txtMobileNumber].forEach { (text) in
            text!.delegate = self
        }
         setUpText()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnAgree.setImage(UIImage(named: "ic_check_box_white.png"), for: .normal)
        UserDefaults.standard.set("1", forKey: "token")
        UserDefaults.standard.synchronize()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   //#MARK:- Member Function
    
   func setUpText()
   {
        lblTitleBuilder.text = mapping.string(forKey: "Builder-Broker_Management_key")
        lblMobile.text = mapping.string(forKey: "MOBILE_Capital_key")
        lblEmail.text = mapping.string(forKey: "EMAIL_key")
    
        txtMobileNumber.placeholder = mapping.string(forKey: "MobileNumber_key")
        btnSebdOtp.setTitle(mapping.string(forKey: "Send_otp_key"), for: .normal)
    
        let attribTermsAndCondition : [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.font : UIFont.italicSystemFont(ofSize: 18.0),
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
        let attributeString = NSMutableAttributedString(string: "Terms & Conditions",
                                                        attributes: attribTermsAndCondition)
        btnTermNCondition.setAttributedTitle(attributeString, for: .normal)
    
   }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobileNumber {
            let str = (NSString(string: textField.text!)).replacingCharacters(in: range, with: string)
            if str.count <= 10 {
                return true
            }
            textField.text = str.substring(to: str.index(str.startIndex, offsetBy: 10))
            return false
        }
        return true
    }
    
    //#MARK:- Button Action
    
    @IBAction func btnMobileAction(_ sender: UIButton) {
        
    }
    @IBAction func btnAgree(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if(sender.isSelected == true)
        {
            btnAgree.setImage(UIImage(named: "ic_check_box_outline_blank_white.png"), for: .normal)
            UserDefaults.standard.set("0", forKey: "token")
            UserDefaults.standard.synchronize()
        }else{
            btnAgree.setImage(UIImage(named: "ic_check_box_white.png"), for: .normal)
            UserDefaults.standard.set("1", forKey: "token")
            UserDefaults.standard.synchronize()
        }
    }
    @IBAction func TermsNconditions(_ sender: UIButton) {
        let pdfView = self.storyboard!.instantiateViewController(withIdentifier: "TermAndCondition") as! TermAndCondition
        pdfView.keys = "TC"
        self.navigationController?.pushViewController(pdfView, animated: true)
    }
    @IBAction func PrivacyPolicy(_ sender: UIButton) {
        let pdfView = self.storyboard!.instantiateViewController(withIdentifier: "TermAndCondition") as! TermAndCondition
        pdfView.keys = "PP"
        self.navigationController?.pushViewController(pdfView, animated: true)
    }
    @IBAction func btnEmailAction(_ sender: UIButton) {
        let LogInVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(LogInVC, animated: false)
    }
    
    @IBAction func btnSendAction(_ sender: xUIButton) {
        guard txtMobileNumber.text != "" else {
            self.makeToast(strMessage: mapping.string(forKey: "Enter_phone_number_key"))
            return
        }
         let val = UserDefaults.standard.value(forKey: "token") as? String
        if (val == "1"){
        SendMobile()
        }else{
          self.makeToast(strMessage: mapping.string(forKey: "TermsAndConditions"))
            return
        }
        
    }
    //#MARK:- API Calling
    func SendMobile()
    {
        var para = [String:AnyObject]()
        para["mobile"] = txtMobileNumber.text! as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            
            GetLoginManager().getLogin(param: para) { [weak self] LoginData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard LoginData == nil else{
                        if(LoginData!.code == 1)
                        {
                            let VerifyOTPVerifyOTPVC = self!.storyboard!.instantiateViewController(withIdentifier: "VerifyOTPViewController") as! VerifyOTPViewController
                            VerifyOTPVerifyOTPVC.MobileNumber = self!.txtMobileNumber.text!
                            VerifyOTPVerifyOTPVC.otp = LoginData!.otp!
                            self?.navigationController?.pushViewController(VerifyOTPVerifyOTPVC, animated: true)
                        }else{
                            self!.makeToast(strMessage: LoginData!.message!)
                        }
                    
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
        
    }
}

