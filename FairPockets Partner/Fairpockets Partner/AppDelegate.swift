//
//  AppDelegate.swift
//  Builder-Broker Management
//
//  Created by DK on 01/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import Alamofire
import SplunkMint

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    var window: UIWindow?
    var NotificationList = [Notificationdata]()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Mint.sharedInstance().disableNetworkMonitoring()
        
        Mint.sharedInstance().applicationEnvironment = SPLAppEnvUserAcceptanceTesting;
        Mint.sharedInstance().initAndStartSession(withAPIKey: "9684c7ca");
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        ScrenHeight = UIScreen.main.bounds.height
        ScrenWidth = UIScreen.main.bounds.width
        //TODO: - Language Change
        setLangauge(language: English)
        //UINavigationBar.appearance().isTranslucent = false
        //UINavigationBar.appearance().barTintColor = kDefaultPrimaryColor
         checkUserIsLoginOrNot()
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func checkUserIsLoginOrNot()
    {
        print(userdefualts.object(forKey: "LoginData"))
        if let LoginData = userdefualts.object(forKey: "LoginData") as? [String:AnyObject] {
            print(LoginData)
            
            UserType =  LoginData["UserType"] as! String
            UserId = LoginData["UserId"] as! String
            Builder_id = LoginData["Builder_id"] as! String
            UserIsLogin = true
            getNotificationList()
            
        }else{
            UserIsLogin = false
        }
        if UserIsLogin == true
        {
            let HomeVC = MainStoryBoard.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
            let navigationController = NavigationController(rootViewController: HomeVC)
            
            let mainViewController = MainViewController()
            mainViewController.rootViewController = navigationController
            mainViewController.setup(type: UInt(1))
            
            navigationController.isNavigationBarHidden = true
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler( [.alert,.sound,.badge])
        
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        completionHandler()
        
        let dict = response.notification.request.content.userInfo
        print("Dict :\(dict)")
        
    }
    
    func getNotificationList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetNotificationListManager().getNotificationList(param: para) { [weak self] NotificationData in
                guard NotificationData == nil else{
                    if(NotificationData!.code == 1)
                    {
                        NotificationData!.notificationdata!.forEach { notification in
                            self!.NotificationList.append(notification)
                        }
                        self!.NotificationList.forEach { notification in
                            if (notification.reminderDate! != "")
                            {
                                
                                let ReminderInDate = self!.stringTodate(Formatter: "dd-MM-yyyy", strDate: notification.reminderDate!)
                                
                                print("ReminderDate : \(ReminderInDate)")
                                
                                let calendar = Calendar.current
                                
                                let year = calendar.component(.year, from: ReminderInDate ?? Date())
                                let month = calendar.component(.month, from: ReminderInDate ?? Date())
                                let day = calendar.component(.day, from: ReminderInDate ?? Date())
                                
                                self!.locallyNotification(year: year, month: month, day: day)
                                
                            }
                        }
                        
                        return
                        
                    }else{
                       // self!.makeToast(strMessage: NotificationData!.message!)
                    }
                    return
                }
            }
        }
        else
        {
            //makeToast(strMessage: NointernetConnection!)
        }
    }
    func locallyNotification(year:Int , month: Int , day: Int)
    {
        
        var datepicker = DateComponents()
        datepicker.year = year
        datepicker.month = month
        datepicker.day = day
        datepicker.hour = 09
        datepicker.minute = 00
        datepicker.second = 00
        
        print("datepicker :\(datepicker)")
        let trigger = UNCalendarNotificationTrigger(dateMatching: datepicker, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "Builder-broker Management"
        content.body = "Just a reminder to open your price calculation!"
        content.categoryIdentifier = "test"
        content.sound = UNNotificationSound.default()
        
        let request = UNNotificationRequest(identifier: "test", content: content, trigger: trigger)
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().add(request){(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
        
    }
    func stringTodate(Formatter:String,strDate:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        // dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }

}

