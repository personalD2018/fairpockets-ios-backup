//
//  FPBaseViewController.swift
//  Fairpockets Partner
//
//  Created by Ashim Samanta on 01/11/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class FPBaseViewController: UIViewController {
    
    var documentController: UIDocumentInteractionController?
    var activityController: UIActivityViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func sharePDF(fileUrl: String?) {
        if let fileURL = fileUrl {
            let url_ = URL(fileURLWithPath: fileURL)
            self.documentController = UIDocumentInteractionController(url: url_.absoluteURL)
            self.documentController?.uti = "com.adobe.pdf"
            let fileName = url_.lastPathComponent
            self.documentController?.name = fileName            
            self.documentController?.presentOptionsMenu(from: self.view.frame, in: self.view, animated: true)
        }
    }
    
    func shareItems(withItems items: [Any]) {
        self.activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
//        _ = [
//            UIActivityType.airDrop,
//            UIActivityType.print,
//            UIActivityType.assignToContact,
//            UIActivityType.saveToCameraRoll,
//            UIActivityType.addToReadingList,
//            UIActivityType.postToFlickr,
//            UIActivityType.postToVimeo,
//            UIActivityType.postToFacebook,
//            UIActivityType.postToTwitter,
//            UIActivityType.postToWeibo,
//            UIActivityType.message,
//            UIActivityType.mail,
//            UIActivityType.copyToPasteboard,
//            UIActivityType.assignToContact,
//            UIActivityType.saveToCameraRoll,
//            UIActivityType.addToReadingList,
//            UIActivityType.postToTencentWeibo
//        ]
        if let controller = self.activityController {
            self.present(controller, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
