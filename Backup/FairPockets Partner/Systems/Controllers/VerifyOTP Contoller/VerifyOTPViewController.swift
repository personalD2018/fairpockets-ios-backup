//
//  VerifyOTPViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 03/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class VerifyOTPViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable {
    //#MARK:- Outlets
    
    @IBOutlet var lblVerifyMobile: UILabel!
    @IBOutlet var OtpVerifyMsg: UILabel!
    @IBOutlet var txtOTP: UITextField!
    @IBOutlet var lblDidnotReceive: UILabel!
    @IBOutlet var btnResend: UIButton!
    @IBOutlet var btnChange: UIButton!
    
    //#MARK:- Declaration
    var MobileNumber = ""
    var otp = -1
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        [txtOTP].forEach { (text) in
            text!.delegate = self
        }
        setPlaceHolder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //#MARK:- Member Function
    
    func setPlaceHolder()
    {
        lblVerifyMobile.text! = mapping.string(forKey: "VERIFYMOBILENUMBER_key")
        let msg1 = mapping.string(forKey: "OTPSent_key")!
        let msg2 = mapping.string(forKey: "ifWeFail_key")!
        OtpVerifyMsg.text! = "\(String(describing: msg1))"+"\n\(String(describing: msg2))"
        txtOTP.placeholder = mapping.string(forKey: "otp_key")
        lblDidnotReceive.text = mapping.string(forKey: "DidnotSend_key")
        btnResend.setTitle(mapping.string(forKey: "Resend_key"), for: .normal)
        btnChange.setTitle(mapping.string(forKey: "Change_key"), for: .normal)
    }

    @IBAction func btnResndAction(_ sender: UIButton) {
        guard MobileNumber != "" else {
            //self.makeToast(strMessage: mapping.string(forKey: "Enter_phone_number_key"))
            self.navigationController?.popViewController(animated: true)
            return
        }
        ReSendOTP()
    }
    @objc func checkOTP()
    {
        guard MobileNumber != "" else {
            //self.makeToast(strMessage: mapping.string(forKey: "Enter_phone_number_key"))
            self.navigationController?.popViewController(animated: true)
            return
        }
        guard txtOTP.text != "" else {
            self.makeToast(strMessage: mapping.string(forKey: "Enter_otp_key"))
            return
        }
        print(Int(txtOTP.text!)!,otp)
        guard Int(txtOTP.text!)! == otp else {
            self.makeToast(strMessage: mapping.string(forKey: "MismatchOTP_key"))
            return
        }
        verifyMobile()
    }
    
    @IBAction func btnChangeAction(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtOTP {
            let str = (NSString(string: textField.text!)).replacingCharacters(in: range, with: string)
            
            if str.count < 4 {
                return true
            }
            textField.text = str.substring(to: str.index(str.startIndex, offsetBy: 4))
            if str.count == 4 {
                txtOTP.resignFirstResponder()
                checkOTP()
            }
            return false
        }
        return true
    }
    //#MARK:- API Calling
    func ReSendOTP()
    {
        var para = [String:AnyObject]()
        para["mobile"] = MobileNumber as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetLoginManager().getLogin(param: para) { [weak self] LoginData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard LoginData == nil else{
                    if(LoginData!.code == 1)
                    {
                       
                        self!.txtOTP.text = "\(LoginData!.otp!)"
                        self!.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self!.checkOTP), userInfo: nil, repeats: false)
                        
                    }else{
                        self!.makeToast(strMessage: LoginData!.message!)
                    }
                    
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
        
    }
    
    func verifyMobile()
    {
        var para = [String:AnyObject]()
        para["mobile"] = MobileNumber as AnyObject
        para["otp"] = txtOTP.text! as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetVerifyOtpManager().getVerifyOtp(param: para) { [weak self] VerifyOtpData in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard VerifyOtpData == nil else{
                    if(VerifyOtpData!.code == 1)
                    {
                        self!.timer.invalidate()
                        let data = VerifyOtpData!.logindata!
                        var LoginResponse = [String:AnyObject]()
                        LoginResponse["UserId"] = data.userId! as AnyObject
                        LoginResponse["UserType"] = data.userType! as AnyObject
                        LoginResponse["Builder_id"] = data.builder_id! as AnyObject
                        
                        userdefualts.removeObject(forKey: "LoginData")
                        userdefualts.set(LoginResponse, forKey: "LoginData")
                        userdefualts.synchronize()
                        UserType = data.userType!
                        UserId = data.userId!
                        Builder_id = data.builder_id!
                        UserIsLogin = true
                        
                        let HomeVC = self?.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        let navigationController = NavigationController(rootViewController: HomeVC)
                        
                        let mainViewController = MainViewController()
                        mainViewController.rootViewController = navigationController
                        mainViewController.setup(type: UInt(1))
                        
                        navigationController.isNavigationBarHidden = true
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = mainViewController
                    }else{
                        self!.makeToast(strMessage: VerifyOtpData!.message!)
                    }
                    
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
        
    }
}

