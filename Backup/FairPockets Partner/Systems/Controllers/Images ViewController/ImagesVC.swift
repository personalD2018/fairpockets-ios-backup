//
//  ImagesVC.swift
//  Fairpockets Partner
//
//  Created by Pritam Dey on 26/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SkyFloatingLabelTextField
import SwiftyJSON

class ImagesVC: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {
    
    @IBOutlet var txtSearch: SkyFloatingLabelTextField!
    @IBOutlet var tbl_images: UITableView!
    @IBOutlet var lblNoData: UILabel!
    var imagesArr:NSArray = NSArray()
    var imagesArrShorted:NSMutableArray = NSMutableArray()
    var SearchImagesArr:NSMutableArray = NSMutableArray()
    var search:String=String()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar(titleText: mapping.string(forKey: "Brochure_key"))
        self.txtSearch.toolbarPlaceholder = mapping.string(forKey: "SearchInquiry_key")
        lblNoData.text = mapping.string(forKey: "NoClientshistry_key")
        self.tbl_images.dataSource = self
        self.tbl_images.delegate = self
        self.txtSearch.delegate = self
        tbl_images.register(UINib(nibName: "ImagesCell", bundle: nil), forCellReuseIdentifier: "ImagesCell")
        
        tbl_images.tableFooterView = UIView()
        tbl_images.estimatedRowHeight = 78
        tbl_images.rowHeight = UITableViewAutomaticDimension
        lblNoData.isHidden = true
         getAllImagesList()
    }

    
    func getAllImagesList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        if (UserType == "Sales"){
            para["user_type"] = UserType.lowercased() as AnyObject
            
        }else{
            para["user_type"] = UserType as AnyObject
        }

        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            // self.ProjectList.removeAll()
            self.imagesArr = NSArray()
            
            
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            ServerRequestHandler.sharedappController.getimagesList(para: para,withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                self.stopAnimating()
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self.imagesArr = result
                            if self.imagesArr.count>0{
                                self.imagesArrShorted.removeAllObjects()
                                for priceDic in self.imagesArr {
                                    let tempDict:NSMutableDictionary = NSMutableDictionary()
                                    let json = JSON(priceDic as Any)
                                    tempDict["builder_name"] = json["Websiteuser"]["builder_name"].stringValue
                                    tempDict["project_name"] = json["Project"]["project_name"].stringValue
                                    
                                    
                                    tempDict["UpdatedDate"] = json["ProjectImage"]["UpdatedDate"].stringValue
                                    tempDict["image_name"] = json["ProjectImage"]["image_name"].stringValue
                                     tempDict["project_id"] = json["ProjectImage"]["project_id"].stringValue
                                    self.imagesArrShorted.add(tempDict)
                                    print("PriceListShorted",self.imagesArrShorted)
                                    self.SearchImagesArr = self.imagesArrShorted.mutableCopy() as! NSMutableArray
                                }
                            }
                            
                            
                            self.tbl_images.reloadData()
                        }
                    }
                }
                
            })
            
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        print("the text is --------------0",self.txtSearch.text!)
        if string.isEmpty
        {
            search = String(search.characters.dropLast())
        }
        else
        {
            search=textField.text!+string
        }
        let predicate=NSPredicate(format: "SELF.builder_name CONTAINS[cd] %@ or SELF.project_name CONTAINS[cd] %@", search,search)
        print(predicate)
        let arr=(self.imagesArrShorted as NSArray).filtered(using: predicate)
        
        print(arr)
        if arr.count > 0
        {
            self.SearchImagesArr.removeAllObjects()
            for i in 0 ..< arr.count
            {
                SearchImagesArr.add(arr[i])
            }
        }
        else
        {   if search==""
        { SearchImagesArr =  NSMutableArray()
            
            SearchImagesArr = self.imagesArrShorted.mutableCopy() as! NSMutableArray
        }
        else{
            SearchImagesArr =  NSMutableArray()
            }
        }
        tbl_images.reloadData()
        return true
    }


}


extension ImagesVC: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.SearchImagesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImagesCell") as! ImagesCell
        if self.SearchImagesArr.count>0{
            if let dict:NSDictionary = self.SearchImagesArr[indexPath.row] as! NSDictionary{
               cell.coffigureCell(myDate: dict)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.SearchImagesArr.count>=indexPath.row{
            if  let messageDic = self.SearchImagesArr[indexPath.row] as? NSDictionary{
                let json = JSON(messageDic)
                let gallery = self.storyboard?.instantiateViewController(withIdentifier: "ImageGalleryVC") as! ImageGalleryVC
                gallery.projectId = json["project_id"].stringValue
                self.navigationController?.pushViewController(gallery, animated: true)
            }
        }
    }

}


