//
//  InventoryDetailsVC.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 15/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import NVActivityIndicatorView
import AlamofireSwiftyJSON
import SwiftyJSON
class InventoryDetailsVC: UIViewController,NVActivityIndicatorViewable {

    
    //MARK:- Outlets
    
    @IBOutlet var lblSearchProjects: UILabel!
    @IBOutlet var lblSelectBuilder: UILabel!
    
    @IBOutlet var lblBuilderValue: UILabel!
    @IBOutlet var builderImg: UIImageView!
    
    @IBOutlet var lblSelectProject: UILabel!
    @IBOutlet var lblProjectValue: UILabel!
    @IBOutlet var ProjectImg: UIImageView!
    
    @IBOutlet var lblSelectTower: UILabel!
    @IBOutlet var lblTowerValue: UILabel!
    @IBOutlet var TowerImg: UIImageView!
    
    @IBOutlet var lblSelectWing: UILabel!
    @IBOutlet var lblWingValue: UILabel!
    @IBOutlet var WingImg: UIImageView!
    @IBOutlet var WingView:UIView!
    
    @IBOutlet var btnSubmit: UIButton!
    
    @IBOutlet var lblBuilderTopConstraint: NSLayoutConstraint!
    @IBOutlet var lblBuilderHeightConstraint: NSLayoutConstraint!
    @IBOutlet var BuilderDropdownHeightConstraint: NSLayoutConstraint!
    @IBOutlet var builderDownImg: UIImageView!
    
    var BulderDD = DropDown()
    var BuilderList:NSArray = NSArray()
    var theBuilderList:[String] = [String]()
    var BuilderIsOpen = false
    var builderSelectedId = -1
    var ProjectDD = DropDown()
    var ProjectList:NSArray = NSArray()
    var theProjectList:[String] = [String]()
    var ProjectSelectedId = -1
    var TowerDD = DropDown()
    var TowerList:NSArray = NSArray()
    var theTowerList:[String] = [String]()
    var TowerSelectedId = -1
    var WingDD = DropDown()
    var WingList:NSArray = NSArray()
    var theWingList:[String] = [String]()
    var WingSelectedId = -1
    let selectPlaceHolder = mapping.string(forKey: "Select_key")
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
     
        setupNavigationbarwithBackButtonAndMenuButton(titleText: "Builder-Broker Management")
       // setupNavigationBar(titleText: mapping.string(forKey: "Builder-Broker_Management_key"))
        setPlaceHolder()
        if(UserType.caseInsensitiveCompare("sales") == ComparisonResult.orderedSame){
            isSales = true
            lblBuilderTopConstraint.constant = 0
            lblBuilderHeightConstraint.constant = 0
            BuilderDropdownHeightConstraint.constant = 0
            builderDownImg.isHidden = true
            getProjectList()
        }else{
            isSales = false
            lblBuilderTopConstraint.constant = 30
            lblBuilderHeightConstraint.constant = 18
            BuilderDropdownHeightConstraint.constant = 40
            builderDownImg.isHidden = false
            getBuilderList()
        }
        print("End")
    }
    
    override func viewDidLayoutSubviews() {
        configDropdown(dropdown: BulderDD, sender: lblBuilderValue)
        configDropdown(dropdown: ProjectDD, sender: lblProjectValue)
        configDropdown(dropdown: TowerDD, sender: lblTowerValue)
        configDropdown(dropdown: WingDD, sender: lblWingValue)
        
    }
    
    func setPlaceHolder()
    {
        lblSearchProjects.text = mapping.string(forKey: "SearchProjects_key")
        lblSelectBuilder.text = mapping.string(forKey: "SelectBuilder_key")
        lblBuilderValue.text = mapping.string(forKey: "Select_key")
        lblSelectProject.text = mapping.string(forKey: "SelectProject_key")
        lblProjectValue.text = mapping.string(forKey: "Select_key")
        lblSelectTower.text = mapping.string(forKey: "SelectTower_key")
        lblTowerValue.text = mapping.string(forKey: "Select_key")
        lblSelectWing.text = mapping.string(forKey: "SelectWing_key")
        lblWingValue.text = mapping.string(forKey: "Select_key")
        btnSubmit.setTitle(mapping.string(forKey: "Submit_key"), for: .normal)
    }
    func showDropDown() {
        BulderDD.dataSource = theBuilderList
        BulderDD.show()
        BulderDD.selectionAction = { (index,item) in
            self.lblBuilderValue.text! = item
            self.builderSelectedId = -1
            self.builderSelectedId = index
            self.ProjectSelectedId = -1
            self.TowerSelectedId = -1
            self.WingSelectedId = -1
            self.getProjectList()
        }
    }
    func showProjectDropDown() {
        ProjectDD.dataSource = theProjectList
        ProjectDD.show()
        ProjectDD.selectionAction = { (index,item) in
            self.lblProjectValue.text! = item
            self.ProjectSelectedId = -1
            self.TowerSelectedId = -1
            self.WingSelectedId = -1
            self.ProjectSelectedId = index
            self.getTowerList()
        }
    }
    func showTowerDropDown(){
        TowerDD.dataSource = theTowerList
        TowerDD.show()
        TowerDD.selectionAction = { (index,item) in
            self.lblTowerValue.text! = item
            self.TowerSelectedId = -1
            self.WingSelectedId = -1
            self.TowerSelectedId = index
            self.getWingList()
        }
    }
    func showWingDropDown(){
        WingDD.dataSource = theWingList
        WingDD.show()
        WingDD.selectionAction = { (index,item) in
            self.lblWingValue.text! = item
            self.WingSelectedId = -1
            self.WingSelectedId = index
            
        }
    }
    
    func getBuilderList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType as AnyObject
       
        
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            // self.ProjectList.removeAll()
            self.ProjectList = NSArray()
            self.theProjectList.removeAll()
            self.ProjectSelectedId = Int()
            self.lblBuilderValue.text = mapping.string(forKey: "Select_key")
            self.lblProjectValue.text = ""
            self.lblTowerValue.text = ""
            self.lblWingValue.text = ""
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvBuilderList(param: para,withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self.BuilderList = result
                            print("projectlist",self.BuilderList)
                            for i in 0..<result.count
                            {
                                if let dict = result.object(at: i) as? NSDictionary{
                                    if let projName = dict.value(forKey: "builder_name") as? String
                                    {
                                        self.theBuilderList.append(projName)
                                    }
                                }
                            }
                            print("The Builder list",self.theBuilderList)
                            
                        }
                    }
                }
                else
                {
                    
                }
            })
            
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func getDetailsList()
    {
      var para = [String:AnyObject]()
        if let dict = TowerList.object(at: TowerSelectedId) as? NSDictionary{
            para["inventory_id"] = dict.value(forKey: "inventory_id") as AnyObject
        }
        if(isSales)
        {
            para["builder_id"] = Builder_id as AnyObject
        }else{
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary{
              para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
          
        }
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            // self.ProjectList.removeAll()
            self.WingList = NSArray()
            self.theWingList.removeAll()
            self.WingSelectedId = Int()
            //  lblProjectValue.text = mapping.string(forKey: "Select_key")
            // self.lblTowerValue.text = ""
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvDetails(param: para,withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            let next = self.storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
                            next.arrDetails = result
                            self.navigationController?.pushViewController(next, animated: true)
                         //   self.WingList = result
//                            for i in 0..<result.count
//                            {
//                                if let dict = result.object(at: i) as? NSDictionary{
//                                    if let wingName = dict.value(forKey: "wing") as? String
//                                    {
//                                        self.theWingList.append(wingName)
//                                    }
//                                }
//                            }
                         //   print("The Details list",self.theWingList)
                            
                            
                        }
                    }
                }
                else
                {
                    
                }
            })
            
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }

        
    }
    
    func getWingList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType.lowercased() as AnyObject
        print(TowerList)
        if let dict = TowerList.object(at: TowerSelectedId) as? NSDictionary{
            para["inventory_id"] = dict.value(forKey: "inventory_id") as AnyObject
            para["tower_name"] = dict.value(forKey: "tower_name") as AnyObject
        }
        // para["project_id"] = self.ProjectList[ProjectSelectedId].projectId! as AnyObject
        if(isSales)
        {
            para["builder_id"] = Builder_id as AnyObject
        }else{
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary{
                para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            // self.ProjectList.removeAll()
            self.WingList = NSArray()
            self.theWingList.removeAll()
            self.WingSelectedId = Int()
          //  lblProjectValue.text = mapping.string(forKey: "Select_key")
            self.lblWingValue.text = mapping.string(forKey: "Select_key")
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvWingList(param: para,withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self.WingList = result
                            for i in 0..<result.count
                            {
                                if let dict = result.object(at: i) as? NSDictionary{
                                    if let wingName = dict.value(forKey: "wing") as? String
                                    {
                                        self.theWingList.append(wingName)
                                    }
                                }
                            }
                            print("The wing list",self.theWingList)
                            if (self.theWingList == [""]){
                               self.lblSelectWing.isHidden = true
                                self.lblWingValue.isHidden = true
                                self.WingImg.isHidden = true
                                self.WingView.isHidden = true
                                }
                            else{
                                self.lblSelectWing.isHidden = false
                                self.lblWingValue.isHidden = false
                                self.WingImg.isHidden = false
                                  self.WingView.isHidden = false
                            }
                            
                        }
                    }
                }
                else
                {
                    
                }
            })
            
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    func getTowerList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType.lowercased() as AnyObject
         if let dict = ProjectList.object(at: ProjectSelectedId) as? NSDictionary{
         para["project_id"] = dict.value(forKey: "project_id") as AnyObject
        }
       // para["project_id"] = self.ProjectList[ProjectSelectedId].projectId! as AnyObject
        if(isSales)
        {
            para["builder_id"] = Builder_id as AnyObject
        }else{
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary{
                para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
        }
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.TowerList = NSArray()
            self.theTowerList.removeAll()
            self.TowerSelectedId = Int()
          
            self.lblTowerValue.text = mapping.string(forKey: "Select_key")
            self.lblWingValue.text = ""
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvTowerList(param: para,withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                           self.TowerList = result
                            for i in 0..<result.count
                            {
                                if let dict = result.object(at: i) as? NSDictionary{
                                    if let towerName = dict.value(forKey: "tower_name") as? String
                                    {
                                        self.theTowerList.append(towerName)
                                    }
                                }
                            }
                          //  print("The project list",self.theProjectList)
                  
                            
                        }
                    }
                }
                else
                {
                    
                }
            })
       
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }
    func getProjectList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        
        if(isSales)
        {
            para["user_type"] = UserType.lowercased() as AnyObject
            para["builder_id"] = "0" as AnyObject
        }else{
            para["user_type"] = UserType as AnyObject
            if let dict = BuilderList.object(at: builderSelectedId) as? NSDictionary{
                para["builder_id"] = dict.value(forKey: "builder_id") as AnyObject
            }
        }
        
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
           // self.ProjectList.removeAll()
            self.ProjectList = NSArray()
            self.theProjectList.removeAll()
            self.ProjectSelectedId = Int()
            self.lblProjectValue.text = mapping.string(forKey: "Select_key")
            self.lblTowerValue.text = ""
            self.lblWingValue.text = ""
            
            NetworkActivityManager.sharedInstance.busy = true
            ServerRequestHandler.sharedappController.getInvProjectList(para:para, withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                             self.ProjectList = result
                            print("projectlist",self.ProjectList)
                            for i in 0..<result.count
                            {
                                if let dict = result.object(at: i) as? NSDictionary{
                                    if let projName = dict.value(forKey: "project_name") as? String
                                    {
                                        self.theProjectList.append(projName)
                                    }
                                }
                            }
                            print("The project list",self.theProjectList)

                        }
                    }
                }
                else
                {

                }
            })

        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }
    
    //#MARK:- Config DropDown
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        print("dropdown.width \(dropdown.width)")
    }
    
    //#MARK:- Button Action
    
    @IBAction func btnSelectBuilderListAction(_ sender: UIButton) {
     
        if(theBuilderList.count > 0)
        {
            showDropDown()
        }
        
    }
    @IBAction func ProjectListAction(_ sender: Any) {
                if(theProjectList.count > 0)
                {
                    showProjectDropDown()
                }
    }
 
    @IBAction func btnSelectTowerAction(_ sender: UIButton) {
        if(theTowerList.count > 0)
        {
           showTowerDropDown()
        }
    }
    @IBAction func btnSelectWingAction(_ sender: UIButton) {
        if(theWingList.count > 0)
        {
             showWingDropDown()
        }
    }
    @IBAction func submitAction(_ sender: Any) {
        if(!isSales)
        {
            guard self.builderSelectedId != -1 && self.lblBuilderValue.text! != selectPlaceHolder else {
                self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectBuilder_key"))
                return
            }
        }
        guard self.ProjectSelectedId != -1 && self.lblProjectValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectProject_key"))
            return
        }
        guard self.TowerSelectedId != -1 && self.lblTowerValue.text != "" && self.lblTowerValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectTower_key"))
            return
        }
       if (self.WingView.isHidden==false){
        guard  self.WingSelectedId != -1 && self.lblWingValue.text != "" && self.lblWingValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectWing_key"))
            return
        }
    }
        self.getDetailsList()
    }
  //  @IBAction func btnSubmitAction(_ sender: Any) {
     
//        if(!isSales)
//        {
//            guard self.builderSelectedId != -1 && self.lblBuilderValue.text! != selectPlaceHolder else {
//                self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectBuilder_key"))
//                return
//            }
//        }
//        guard self.ProjectSelectedId != -1 && self.lblProjectValue.text! != selectPlaceHolder else {
//            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectProject_key"))
//            return
//        }
//        guard self.UnitSelectedId != -1 && self.lblUnitValue.text != "" && self.lblUnitValue.text! != selectPlaceHolder else {
//            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectUnit_key"))
//            return
//        }
//        jsonObj = JSON()
//        FinaljsonObj = JSON()
//        let PriceCalaculatorVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceCAlculatorViewController") as! PriceCAlculatorViewController
//        if(isSales)
//        {
//            PriceCalaculatorVC.BuilderId = ""
//            PriceCalaculatorVC.BuilderName = ""
//        }else{
//            PriceCalaculatorVC.BuilderId = BuilderList[builderSelectedId].builderId!
//            PriceCalaculatorVC.BuilderName = BuilderList[builderSelectedId].builderName!
//        }
//        PriceCalaculatorVC.ProjectId = ProjectList[ProjectSelectedId].projectId!
//        PriceCalaculatorVC.ProjectName = ProjectList[ProjectSelectedId].projectName!
//        PriceCalaculatorVC.UnitName = self.UnitList[UnitSelectedId]
//        self.navigationController?.pushViewController(PriceCalaculatorVC, animated: true)
//
        
        
   // }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
