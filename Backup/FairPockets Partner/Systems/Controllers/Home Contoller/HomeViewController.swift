//
//  HomeViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 02/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import DropDown
import AlamofireSwiftyJSON
import SwiftyJSON

class HomeViewController: UIViewController,NVActivityIndicatorViewable  {

    //MARK:- Outlets
    
    @IBOutlet var lblSearchProjects: UILabel!
    @IBOutlet var lblSelectBuilder: UILabel!
   
    @IBOutlet var lblBuilderValue: UILabel!
     @IBOutlet var builderImg: UIImageView!
    
    @IBOutlet var lblSelectProject: UILabel!
    @IBOutlet var lblProjectValue: UILabel!
    @IBOutlet var ProjectImg: UIImageView!
    
    @IBOutlet var lblSelectUnit: UILabel!
    @IBOutlet var lblUnitValue: UILabel!
    @IBOutlet var UnitImg: UIImageView!
    
    @IBOutlet var btnSubmit: UIButton!
    
    @IBOutlet var lblBuilderTopConstraint: NSLayoutConstraint!
    @IBOutlet var lblBuilderHeightConstraint: NSLayoutConstraint!    
    @IBOutlet var BuilderDropdownHeightConstraint: NSLayoutConstraint!
    @IBOutlet var builderDownImg: UIImageView!
    
    //MARK:- Declaretion
    var BulderDD = DropDown()
    var BuilderList = [Bulderdata]()
    var theBuilderList:[String] = [String]()
    var BuilderIsOpen = false
    var builderSelectedId = -1
    var ProjectDD = DropDown()
    var ProjectList = [Projectdata]()
    var theProjectList:[String] = [String]()
    var ProjectSelectedId = -1
    var UnitDD = DropDown()
    var UnitList:[String] = [String]()
    var theUnitList:[String] = [String]()
    var UnitSelectedId = -1
    let selectPlaceHolder = mapping.string(forKey: "Select_key")
    
    //MARK:- Declaretion
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false        
       // setupNavigationBar(titleText: mapping.string(forKey: "Builder-Broker_Management_key"))
        setupNavigationbarwithBackButtonAndMenuButton(titleText: "Builder-Broker Management")
      
        setPlaceHolder()
        if(UserType.caseInsensitiveCompare("sales") == ComparisonResult.orderedSame){
            isSales = true
            lblBuilderTopConstraint.constant = 0
            lblBuilderHeightConstraint.constant = 0
            BuilderDropdownHeightConstraint.constant = 0
            builderDownImg.isHidden = true
            getProjectList()
        }else{
            isSales = false
            lblBuilderTopConstraint.constant = 30
            lblBuilderHeightConstraint.constant = 18
            BuilderDropdownHeightConstraint.constant = 40
            builderDownImg.isHidden = false
            getBuilderList()
        }
        

   }
    
    override func viewDidAppear(_ animated: Bool) {
       
    }
    
    override func viewDidLayoutSubviews() {
        configDropdown(dropdown: BulderDD, sender: lblBuilderValue)
        configDropdown(dropdown: ProjectDD, sender: lblProjectValue)
        configDropdown(dropdown: UnitDD, sender: lblUnitValue)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Member Function
    
    func setPlaceHolder()
    {
        lblSearchProjects.text = mapping.string(forKey: "SearchProjects_key")
        lblSelectBuilder.text = mapping.string(forKey: "SelectBuilder_key")
        lblBuilderValue.text = mapping.string(forKey: "Select_key")
        lblSelectProject.text = mapping.string(forKey: "SelectProject_key")
        lblProjectValue.text = mapping.string(forKey: "Select_key")
        lblSelectUnit.text = mapping.string(forKey: "SelectUnit_key")
        lblUnitValue.text = mapping.string(forKey: "Select_key")
        btnSubmit.setTitle(mapping.string(forKey: "Submit_key"), for: .normal)
    }
    
    //#MARK:-  DropDown show/hide Builder method
    
    func showDropDown() {
        BulderDD.dataSource = theBuilderList
        BulderDD.show()
        BulderDD.selectionAction = { (index,item) in
            self.lblBuilderValue.text! = item
            self.builderSelectedId = index
            self.ProjectSelectedId = -1
            self.UnitSelectedId = -1
            self.getProjectList()
        }
    }
    func hideDropDown() {
        BulderDD.hide()
    }
    func showProjectDropDown() {
        ProjectDD.dataSource = theProjectList
        ProjectDD.show()
        ProjectDD.selectionAction = { (index,item) in
            self.lblProjectValue.text! = item
            self.ProjectSelectedId = -1
            self.UnitSelectedId = -1
            self.ProjectSelectedId = index
            self.getUnitList()
        }
    }
    func hideProjectDropDown() {
        ProjectDD.hide()
    }
    
    func showUnitDropDown() {
        UnitDD.dataSource = theUnitList
        UnitDD.show()
        UnitDD.selectionAction = { (index,item) in
            self.lblUnitValue.text! = item
            self.UnitSelectedId = index
        }
    }
    func hideUnitDropDown() {
        UnitDD.hide()
    }
    
    //#MARK:- Config DropDown
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        print("dropdown.width \(dropdown.width)")
    }
     //#MARK:- Button Action
    
    @IBAction func btnSelectBuilderAction(_ sender: UIButton) {
        /*UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
            self.builderImg.transform = CGAffineTransform.init(translationX: <#T##CGFloat#>, y: <#T##CGFloat#>)
            rotated(by:self.builderImg.transform, CGFloat(M_PI_2))
        }) { (finished) -> Void in
            self.btnSelectBuilderAction(sender)
        }*/
       
        /*
        if(BuilderIsOpen == false)
        {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
                self.builderImg.transform = self.builderImg.transform.rotated(by: CGFloat(M_PI))
            })
            //self.builderImg.rotate360Degrees()
            if(theBuilderList.count > 0)
            {
                showDropDown()
            }
        }else{
            self.builderImg.layer.removeAllAnimations()
            /*UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
                self.builderImg.transform = self.builderImg.transform.rotated(by: CGFloat(M_PI))
            })
             hideDropDown()*/
        }*/
        if(theBuilderList.count > 0)
        {
            showDropDown()
        }
        
    }
    @IBAction func btnSelectProjectAction(_ sender: UIButton) {
        if(theProjectList.count > 0)
        {
            showProjectDropDown()
        }
    }
    @IBAction func btnSelectUnitAction(_ sender: UIButton) {
        if(theUnitList.count > 0)
        {
            showUnitDropDown()
        }
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        /*
        
        Intent intent = new Intent(getActivity(), CalculatorActivity.class);
        if (isBuilder) {
            intent.putExtra(Constants.EXTRA_BUILDER_NAME, buildersResponseList.get(spinnerBuilder.getSelectedIndex() - 1).getBuilder_name());
        }
        
        Integer pid = projectsResponseList.get(spinnerProjects.getSelectedIndex() - 1).getProjec_id();
        Log.e("project_id", pid + "");
        intent.putExtra(Constants.EXTRA_PROJECT_ID, projectsResponseList.get(spinnerProjects.getSelectedIndex() - 1).getProjec_id());
        
        intent.putExtra(Constants.EXTRA_PROJECT_NAME, projectsResponseList.get(spinnerProjects.getSelectedIndex() - 1).getProject_name());
        intent.putExtra(Constants.EXTRA_UNIT, unitsResponseList.get(spinnerUnits.getSelectedIndex()));
        startActivity(intent);
    }
});*/
        if(!isSales)
        {
            guard self.builderSelectedId != -1 && self.lblBuilderValue.text! != selectPlaceHolder else {
                self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectBuilder_key"))
                return
            }
        }        
        guard self.ProjectSelectedId != -1 && self.lblProjectValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectProject_key"))
            return
        }
        guard self.UnitSelectedId != -1 && self.lblUnitValue.text != "" && self.lblUnitValue.text! != selectPlaceHolder else {
            self.makeToast(strMessage: mapping.string(forKey: "PleaseSelectUnit_key"))
            return
        }
        jsonObj = JSON()
        FinaljsonObj = JSON()
        let PriceCalaculatorVC = self.storyboard?.instantiateViewController(withIdentifier: "PriceCAlculatorViewController") as! PriceCAlculatorViewController
        if(isSales)
        {
            PriceCalaculatorVC.BuilderId = ""
            PriceCalaculatorVC.BuilderName = ""
        }else{
            PriceCalaculatorVC.BuilderId = BuilderList[builderSelectedId].builderId!
            PriceCalaculatorVC.BuilderName = BuilderList[builderSelectedId].builderName!
        }
        PriceCalaculatorVC.ProjectId = ProjectList[ProjectSelectedId].projectId!
        PriceCalaculatorVC.ProjectName = ProjectList[ProjectSelectedId].projectName!
        PriceCalaculatorVC.UnitName = self.UnitList[UnitSelectedId]
        self.navigationController?.pushViewController(PriceCalaculatorVC, animated: true)
        
        

    }
    
    //#MARK:- API Calling
    func getBuilderList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.BuilderList.removeAll()
            self.theBuilderList.removeAll()
            self.builderSelectedId = Int()
            lblBuilderValue.text = mapping.string(forKey: "Select_key")
            self.lblProjectValue.text = ""
            self.lblUnitValue.text = ""
            
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetBuilderListManager().getBuilderList(param: para) { [weak self] BuilderList in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard BuilderList == nil else{
                    if(BuilderList!.code == 1)
                    {
                        self!.BuilderList = BuilderList!.bulderdata!
                        BuilderList!.bulderdata!.forEach { bulder in
                            self!.theBuilderList.append(bulder.builderName!)
                        }
                    }else
                    {
                        self!.makeToast(strMessage: BuilderList!.message!)
                    }
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
        
    }
    func getProjectList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType as AnyObject
        if(isSales)
        {
            para["builder_id"] = "0" as AnyObject
        }else{
            para["builder_id"] = self.BuilderList[builderSelectedId].builderId! as AnyObject
        }
        
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.ProjectList.removeAll()
            self.theProjectList.removeAll()
            self.ProjectSelectedId = Int()
            lblProjectValue.text = mapping.string(forKey: "Select_key")
            self.lblUnitValue.text = ""
            
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetProjectListManager().getProjectList(param: para) { [weak self] ProjectList in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard ProjectList == nil else{
                    if(ProjectList!.code == 1)
                    {
                        self!.ProjectList = ProjectList!.projectdata!
                        ProjectList!.projectdata!.forEach { bulder in
                            self!.theProjectList.append(bulder.projectName!)
                        }
                        
                    }else{
                        self!.makeToast(strMessage: ProjectList!.message!)
                    }
                    
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }
    
    func getUnitList()
    {
        print(self.ProjectList.count)
        print(ProjectSelectedId)
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["user_type"] = UserType as AnyObject
        para["project_id"] = self.ProjectList[ProjectSelectedId].projectId! as AnyObject
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.theUnitList.removeAll()
            lblUnitValue.text = mapping.string(forKey: "Select_key")
            
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetUnitListManager().getUnitList(param: para) { [weak self] UnitList in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard UnitList == nil else{
                    if(UnitList!.code == "1")
                    {
                        self!.UnitList = UnitList!.unitdata!
                        UnitList!.unitdata!.forEach { unit in
                            let units = unit.components(separatedBy: "/")
                            self!.theUnitList.append(units[0])
                        }
                        print(self!.theUnitList)
                        //self!.theUnitList = UnitList!.unitdata!
                        
                        
                    }else{
                        self!.makeToast(strMessage: UnitList!.message!)
                    }
                    
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }

    
}
extension UIImageView {
    func rotate360Degrees(duration: CFTimeInterval = 2) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 1)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=1.0
        self.layer.add(rotateAnimation, forKey: nil)
    }
}
