//
//  HomeScreenVC.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 14/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import SDWebImage


class HomeScreenVC: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var btnPriceCalCulator: UIButton!
    @IBOutlet weak var btnInventoryDetails: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnInventoryDetails.isHidden = true
        self.btnPriceCalCulator.isHidden = true
        

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.navigationController?.isNavigationBarHidden = true
        self.getUserAccess()
    }
    
    func getUserAccess()  {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            ServerRequestHandler.sharedappController.getUserAccessAfterEmailLogin(para: para,withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                self.stopAnimating()
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        print("access are --->",dict)
                        let data = JSON(dict)
                        let priceButton = data["data"]["UserCalStatus"].stringValue
                        let InvButton = data["data"]["UserInvStatus"].stringValue
                        self.btnInventoryDetails.isHidden = InvButton == "true" ? false:true
                        self.btnPriceCalCulator.isHidden = priceButton == "true" ? false:true
                    }
                }
                
            })
            
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }

    @IBAction func inventoryDetails(_ sender: Any)
    {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "InventoryDetailsVC") as! InventoryDetailsVC
        ServerRequestHandler.sharedappController.defaultHomeLoader = 200
        self.navigationController?.pushViewController(home, animated: true)
    }
    @IBAction func priceCalc(_ sender: Any)
    {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
         ServerRequestHandler.sharedappController.defaultHomeLoader = 100
        self.navigationController?.pushViewController(home, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
