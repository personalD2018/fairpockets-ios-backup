//
//  ClientHistoryViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 04/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SkyFloatingLabelTextField


class ClientHistoryViewController: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {

    //#MARK:- Outlets
    
    @IBOutlet var txtSearch: SkyFloatingLabelTextField!
    @IBOutlet var tbl_ClientList: UITableView!
    @IBOutlet var lblNoData: UILabel!
    //#MARK:- Declaration
    var MyClientList = [ClientHistorydata]()
    var ClientId = String()
    var isSearch = false
    var MyClientSearchList = [ClientHistorydata]()
    var status:String! = String()
    var ide:String! = String()
    
    //#MARK:- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "ClientHistory_key"))
        self.txtSearch.toolbarPlaceholder = mapping.string(forKey: "SearchInquiry_key")
        lblNoData.text = mapping.string(forKey: "NoClientshistry_key")
        self.tbl_ClientList.dataSource = self
        self.tbl_ClientList.delegate = self
        self.txtSearch.delegate = self
        tbl_ClientList.register(UINib(nibName: "ClientHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientHistoryTableViewCell")
        tbl_ClientList.tableFooterView = UIView()
        tbl_ClientList.estimatedRowHeight = 78
        tbl_ClientList.rowHeight = UITableViewAutomaticDimension
        lblNoData.isHidden = true
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        getClientHistoryList()
        
        var param = [String:AnyObject]()
        param["client_notification_row_id"] = ClientId as AnyObject
        if (status == "0") {
            ServerRequestHandler.sharedappController.getBuilderMyClientRowRead(para: param, withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        
                    }
                }
                else
                {
                    
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //#MARK:- Member Function
    func OpenPdfView(index:Int)
    {
        if(self.MyClientList[index].pdfLink==""){
            self.makeToast(strMessage: "No PDF Found To View")
        }else{
        let pdfView = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewController") as! PDFViewController
        pdfView.urls = self.MyClientList[index].pdfLink!
        self.navigationController?.pushViewController(pdfView, animated: true)
        }
    }
    func shareSocialMedia(index:Int)
    {
        if let urlString = self.MyClientList[index].pdfLink {
            print(urlString)
            let vc = UIActivityViewController(activityItems: [urlString], applicationActivities: nil)
            _ = [
                UIActivityType.airDrop,
                UIActivityType.print,
                UIActivityType.assignToContact,
                UIActivityType.saveToCameraRoll,
                UIActivityType.addToReadingList,
                UIActivityType.postToFlickr,
                UIActivityType.postToVimeo,
                UIActivityType.postToFacebook,
                UIActivityType.postToTwitter,
                UIActivityType.postToWeibo,
                UIActivityType.message,
                UIActivityType.mail,
                UIActivityType.copyToPasteboard,
                UIActivityType.assignToContact,
                UIActivityType.saveToCameraRoll,
                UIActivityType.addToReadingList,
                UIActivityType.postToTencentWeibo
            ]
            self.present(vc, animated: true)
        }
    }
    //#MARK:- UITextFeild Delegate Function
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if(textField.text != "")
        {
            MyClientSearchList.removeAll()
            isSearch = true
            MyClientList.forEach { Client in
                if(Client.remarks!.contains(textField.text!)  || Client.reminderDate!.contains(textField.text!) || Client.reminderText!.contains(textField.text!))
                {
                    self.MyClientSearchList.append(Client)
                }
            }
            print("MyClientSearchList -->\(MyClientSearchList.count)")
            if(MyClientSearchList.count > 0)
            {
                
                self.lblNoData.isHidden = true
            }else{
                self.lblNoData.isHidden = false
            }
            
        }else{
            MyClientSearchList.removeAll()
            isSearch = false
        }
        self.tbl_ClientList.reloadData()
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if(textField.text == "")
        {
            isSearch = false
            MyClientSearchList.removeAll()
            
        }else{
            isSearch = true
            
        }
        return true
    }

    //#MARK:- API Calling
    
    func getClientHistoryList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["client_id"] = ClientId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            GetClientHistoryManager().getClientHistory(param: para) { [weak self] ClientListData in
                guard ClientListData == nil else{
                    if(ClientListData?.code == 1)
                    {
                        ClientListData?.clientHistorydata!.forEach { Client in
                            self?.MyClientList.append(Client)
                        }
                        self?.lblNoData.isHidden = true
                        self?.tbl_ClientList.reloadData()
                        return
                        
                    }else{
                        self?.makeToast(strMessage: ClientListData?.message)
                        self?.lblNoData.isHidden = false
                    }
                    self?.lblNoData.isHidden = false
                    self?.tbl_ClientList.reloadData()
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection)
        }
    }
    
    func deleteRemarks(index:Int?) {
        var clientData: ClientHistorydata?
        if let _index = index {
            
            let alertController = UIAlertController(title: "Broker Management", message: "Are you sure want to delete?", preferredStyle: .alert)
            let actionCancel = UIAlertAction(title: "NO", style: .default, handler: nil)
            let actionOK = UIAlertAction(title: "YES", style: .default) { [weak self] (action:UIAlertAction) in
                if (self?.isSearch)! == true {
                    clientData = self?.MyClientSearchList[_index]
                } else {
                    clientData = self?.MyClientList[_index];
                }
                
                var params = [String:AnyObject]()
                params["user_id"] = clientData?.userId as AnyObject
                params["client_id"] = clientData?.clientId as AnyObject
                params["client_history_id"] = clientData?.id as AnyObject
                
                if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
                    NetworkActivityManager.sharedInstance.busy = true
                    self?.startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
                    ServerRequestHandler.sharedappController.getDetail(url: aBasePostClientHistoryRemarksDeleteURl, param: params) { [weak self] (status: Int, response: Any?) in
                        self?.stopAnimating()
                        NetworkActivityManager.sharedInstance.busy = false
                        if let responseDict = response as? NSDictionary {
                            let _message = responseDict.value(forKey: "message")
                            if status == 1 {
                                if self?.isSearch == true {
                                    self?.MyClientSearchList.remove(at: _index);
                                    
                                } else {
                                    self?.MyClientList.remove(at: _index);
                                }
                                self?.tbl_ClientList.reloadData();
                                return
                            } else {
                                self?.makeToast(strMessage: _message as? String);
                            }
                        }
                    }
                } else {
                    self?.makeToast(strMessage: NointernetConnection!)
                }
            }
            alertController.addAction(actionCancel)
            alertController.addAction(actionOK)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension ClientHistoryViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearch == false) {
            if(self.MyClientList.count > 0) {
                self.lblNoData.isHidden = true
            }else{
                self.lblNoData.isHidden = false
            }
            return self.MyClientList.count
        } else {
            if(self.MyClientSearchList.count > 0) {
                self.lblNoData.isHidden = true
            }else{
                self.lblNoData.isHidden = false
            }
            return self.MyClientSearchList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientHistoryTableViewCell") as! ClientHistoryTableViewCell
        
        cell.tag = indexPath.row
        
        if(isSearch == false) {
            cell.setData(theModel: self.MyClientList[indexPath.row])
        } else {
            cell.setData(theModel: self.MyClientSearchList[indexPath.row])
        }
        
        cell.registerHandler { [weak self] in
            self?.deleteRemarks(index: indexPath.row)
        }
        
        return cell
    }
}



