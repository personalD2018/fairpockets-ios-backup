//
//  SaveDetailsPopupVc.swift
//  Builder-Broker Management
//
//  Created by YASH on 08/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import UserNotifications
import Alamofire
import NVActivityIndicatorView
import AlamofireSwiftyJSON
import SwiftyJSON

var FinaljsonObj = JSON()
var savedPdf = ""

protocol CommonPickerDelegate
{
    func setValueOnReadValue(value: Bool, PdfLink: String)
}

class SaveDetailsPopupVc: UIViewController,NVActivityIndicatorViewable  {

    //MARK: - Outlet
    
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtMobile: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtRemark: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtNextReminderDate: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtComment: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnSave: xUIButton!
    
    
    //MARK: - Variable
    
     var pickerDelegate : CommonPickerDelegate?
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        [txtEmail,txtName,txtMobile,txtRemark,txtNextReminderDate,txtComment].forEach { (text) in
            text!.delegate = self
        }
        
        // Do any additional setup after loading the view.
    }
    
    
}

extension SaveDetailsPopupVc
{
    
    //MARK:- Alert Controller
    
    func showAlertController(strMessage:String,strTitle:String)
    {
        let alertController = UIAlertController(title: strTitle, message: strMessage, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        
        alertController.addAction(action1)
      
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - IBAction Tapped
    
    @IBAction func btnSaveActionTapped(_ sender: UIButton)
    {
        if (txtName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: mapping.string(forKey: "Enter_name_key"))
          //  txtName.errorMessage = mapping.string(forKey: "Enter_name_key")
            showAlertController(strMessage: mapping.string(forKey: "Enter_name_key"), strTitle:  mapping.string(forKey: "Builder-Broker_Management_key"))
            return
        }
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
          //  txtName.errorMessage = ""
            makeToast(strMessage: mapping.string(forKey: "Enter_email_address_key"))
         //   txtEmail.errorMessage = mapping.string(forKey: "Enter_email_address_key")
            showAlertController(strMessage: mapping.string(forKey: "Enter_email_address_key"), strTitle:  mapping.string(forKey: "Builder-Broker_Management_key"))
            return
        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: mapping.string(forKey: "Please_enter_valid_email_address_key"))
          //  txtEmail.errorMessage = mapping.string(forKey: "Please_enter_valid_email_address_key")
            showAlertController(strMessage: mapping.string(forKey: "Please_enter_valid_email_address_key"), strTitle:  mapping.string(forKey: "Builder-Broker_Management_key"))
            return
        }
        else if (txtMobile.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: mapping.string(forKey: "Enter_mobile_number_key"))
         //   txtMobile.errorMessage = mapping.string(forKey: "Enter_mobile_number_key")
            showAlertController(strMessage: mapping.string(forKey: "Enter_mobile_number_key"), strTitle:  mapping.string(forKey: "Builder-Broker_Management_key"))
            return
        }
        else if (txtMobile.text?.count)! < 10
        {
            makeToast(strMessage: mapping.string(forKey: "Enter_valid_mobile_number_key"))
         //   txtMobile.errorMessage = mapping.string(forKey: "Enter_valid_mobile_number_key")
            showAlertController(strMessage: mapping.string(forKey: "Enter_valid_mobile_number_key"), strTitle:  mapping.string(forKey: "Builder-Broker_Management_key"))
        }
        else if (txtRemark.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: mapping.string(forKey: "Enter_remark_key"))
          //  txtRemark.errorMessage = mapping.string(forKey: "Enter_remark_key")
            showAlertController(strMessage: mapping.string(forKey: "Enter_remark_key"), strTitle:  mapping.string(forKey: "Builder-Broker_Management_key"))
            return
        }
        else if (txtNextReminderDate.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: mapping.string(forKey: "Enter_next_remider_date_key"))
         //   txtNextReminderDate.errorMessage = mapping.string(forKey: "Enter_next_remider_date_key")
            showAlertController(strMessage: mapping.string(forKey: "Enter_next_remider_date_key"), strTitle:  mapping.string(forKey: "Builder-Broker_Management_key"))
            return
        }
        else if (txtComment.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: mapping.string(forKey: "Enter_comment_key"))
        //    txtComment.errorMessage = mapping.string(forKey: "Enter_comment_key")
            showAlertController(strMessage: mapping.string(forKey: "Enter_comment_key"), strTitle:  mapping.string(forKey: "Builder-Broker_Management_key"))
            return
        }
        else
        {
            savePDF()
        }
       
    }
    
    @IBAction func btnCloseActionTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - API Method

    func savePDF()
    {
       // var para = [String:AnyObject]()
        //para["user_id"] = UserId as AnyObject
        let strDate = DateToString(Formatter: "dd-MM-yyyy", date: Date())
        var dictUserDetail = JSON()
        dictUserDetail["email"].stringValue = self.txtEmail.text ?? ""
        dictUserDetail["mobile"].stringValue = self.txtMobile.text ?? ""
        dictUserDetail["name"].stringValue = self.txtName.text ?? ""
        dictUserDetail["remarks"].stringValue = self.txtRemark.text ?? ""
        dictUserDetail["reminderDate"].stringValue = strDate
        dictUserDetail["reminderText"].stringValue = self.txtComment.text ?? ""
        FinaljsonObj["clientDetails"] = JSON(dictUserDetail)
        print("FinaljsonObj \(FinaljsonObj)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetSaveReminderManager().CalculatorDataSubmit(json: FinaljsonObj.rawString()!) { [weak self] SaveReminder in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard SaveReminder == nil else{
                    if(SaveReminder!.code == 1)
                    {
                        let calendar = Calendar.current
                        
                        let year = calendar.component(.year, from: Date())
                        let month = calendar.component(.month, from: Date())
                        let day = calendar.component(.day, from: Date())
                        
                        appDelegate?.locallyNotification(year: year, month: month, day: day)
                        
                        savedPdf = SaveReminder!.pdfLink!
                        self?.makeToast(strMessage: SaveReminder!.message ?? "Successfull submitted")
                        self!.dismiss(animated: true, completion: nil)
                        self?.pickerDelegate?.setValueOnReadValue(value: true, PdfLink: savedPdf)
                        
                    }else{
                        self!.makeToast(strMessage: SaveReminder!.message!)
                    }
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    
        /*if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type:
                NVActivityIndicatorType(rawValue:LoaderType))
            GetSaveReminderManager().getSaveReminder(param: para) { [weak self] SaveReminder in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard SaveReminder == nil else{
                    if(SaveReminder!.code == 1)
                    {
                        savedPdf = SaveReminder!.pdfLink!
                        self!.dismiss(animated: true, completion: nil)
                        
                    }else{
                        self!.makeToast(strMessage: SaveReminder!.message!)
                    }
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }*/
        
        
    }
}


extension SaveDetailsPopupVc: UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtNextReminderDate
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonDateTimeVC") as! CommonDateTimeVC
            obj.pickerDelegate = self
            obj.controlType = 1
            obj.isSetMaximumDate = false
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
            return false
            
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        if textField == txtMobile
        {
            let maxLength = 10
            let currentString: NSString = txtMobile.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        return true
    }
    
}


extension SaveDetailsPopupVc : DateTimePickerDelegate
{
    func setDateandTime(dateValue: Date,type : Int) {
        
        if type == 1
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            let fromDate = DateToString(Formatter: "dd-MM-yyyy", date: dateValue)
            
            // txtBirthdate.text = dateFormatter.string(from: dateValue)
            txtNextReminderDate.text = fromDate
            txtComment.becomeFirstResponder()
        }
    }
}


