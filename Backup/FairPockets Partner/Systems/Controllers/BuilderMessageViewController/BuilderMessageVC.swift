//
//  BuilderMessageVC.swift
//  Fairpockets Partner
//
//  Created by Pritam Dey on 26/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SkyFloatingLabelTextField
import SwiftyJSON

class BuilderMessageVC: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {
    @IBOutlet var txtSearch: SkyFloatingLabelTextField!
    @IBOutlet var tbl_BuilderMessage: UITableView!
    @IBOutlet var lblNoData: UILabel!
    var messageArr:NSArray = NSArray()
    var messageArrShorted:NSMutableArray = NSMutableArray()
    var SearchmessageArr:NSMutableArray = NSMutableArray()
    var search:String=String()
     var searchMessageArr:NSMutableArray = NSMutableArray()
    var countVal:Int! = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

       
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar(titleText: mapping.string(forKey: "Brochure_key"))
        self.txtSearch.toolbarPlaceholder = mapping.string(forKey: "SearchInquiry_key")
        lblNoData.text = mapping.string(forKey: "NoClientshistry_key")
        self.tbl_BuilderMessage.dataSource = self
        self.tbl_BuilderMessage.delegate = self
        self.txtSearch.delegate = self
        tbl_BuilderMessage.register(UINib(nibName: "BuilderMessageCell", bundle: nil), forCellReuseIdentifier: "BuilderMessageCell")
        
        tbl_BuilderMessage.tableFooterView = UIView()
        tbl_BuilderMessage.estimatedRowHeight = 78
        tbl_BuilderMessage.rowHeight = UITableViewAutomaticDimension
        lblNoData.isHidden = true
        
        
        var param = [String:AnyObject]()
        param["user_id"] = UserId as AnyObject
        param["status"] = "builder_message" as AnyObject
        param["read"] =  "1" as AnyObject
        param["count"] = countVal as AnyObject

        ServerRequestHandler.sharedappController.getMessageNotificationRead(para: param, withHandler: {(_ status: Int, _ data: NSObject) -> Void in
            if status == 1
            {
                if let dict = (data) as? NSDictionary {

                }
            }
            else
            {

            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getBuilderMessageList()
    }

    
    func getBuilderMessageList()
    {
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
       // para["user_type"] = UserType.lowercased() as AnyObject
        if (UserType == "Sales"){
            para["user_type"] = UserType.lowercased() as AnyObject
            
        }else{
            para["user_type"] = UserType as AnyObject
        }
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            // self.ProjectList.removeAll()
            self.messageArr = NSArray()
            
            
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            ServerRequestHandler.sharedappController.getBuilderMessageList(para: para,withHandler: {(_ status: Int, _ data: NSObject) -> Void in
                NetworkActivityManager.sharedInstance.busy = false
                self.stopAnimating()
                if status == 1
                {
                    if let dict = (data) as? NSDictionary {
                        if let result = dict.value(forKey: "data") as? NSArray {
                            print("result",result)
                            self.messageArr = result
                            if self.messageArr.count>0{
                                self.messageArrShorted.removeAllObjects()
                                for priceDic in self.messageArr {
                                    let tempDict:NSMutableDictionary = NSMutableDictionary()
                                    let json = JSON(priceDic as Any)
                                    tempDict["builder_name"] = json["Websiteuser"]["builder_name"].stringValue
                                    tempDict["project_name"] = json["Project"]["project_name"].stringValue
                                    
                                    
                                    tempDict["created"] = json["MessageBoardAppusersAlls"]["created"].stringValue
                                    tempDict["message"] = json["MessageBoardAppusersAlls"]["message"].stringValue
                                    tempDict["status"] = json["Appusermessages"]["status"].stringValue
                                    tempDict["id"] = json["MessageBoardAppusersAlls"]["id"].stringValue
                                    self.messageArrShorted.add(tempDict)
                                    print("PriceListShorted",self.messageArrShorted)
                                    self.searchMessageArr = self.messageArrShorted.mutableCopy() as! NSMutableArray
                                }
                            }
                            
                            self.tbl_BuilderMessage.reloadData()
                        }
                    }
                }
                
            })
            
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        print("the text is --------------0",self.txtSearch.text!)
        if string.isEmpty
        {
            search = String(search.characters.dropLast())
        }
        else
        {
            search=textField.text!+string
        }
        let predicate=NSPredicate(format: "SELF.builder_name CONTAINS[cd] %@ or SELF.project_name CONTAINS[cd] %@", search,search)
        print(predicate)
        let arr=(self.messageArrShorted as NSArray).filtered(using: predicate)
        
        print(arr)
        if arr.count > 0
        {
            self.searchMessageArr.removeAllObjects()
            for i in 0 ..< arr.count
            {
                searchMessageArr.add(arr[i])
            }
        }
        else
        {   if search==""
        { searchMessageArr =  NSMutableArray()
            
            searchMessageArr = self.messageArrShorted.mutableCopy() as! NSMutableArray
        }
        else{
            searchMessageArr =  NSMutableArray()
            }
        }
        tbl_BuilderMessage.reloadData()
        return true
    }

    
   
    
    
}

extension BuilderMessageVC: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchMessageArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuilderMessageCell") as! BuilderMessageCell
        cell.tag = indexPath.row
        if self.searchMessageArr.count>0{
            if let dict:NSDictionary = self.searchMessageArr[indexPath.row] as! NSDictionary{
                if let status = dict.value(forKey: "status") as? String{
                    if (status == "1"){
                       cell.vwBackgrd.backgroundColor=UIColor.white
                        cell.lblDate.textColor = UIColor.black
                        cell.lblBuilderName.textColor = UIColor.black
                        cell.lblProjectName.textColor = UIColor.black
                        cell.lblBuilderMessage.textColor = UIColor.black
                        cell.lblReadMore.textColor = UIColor.black
                    }
                    if (status == "0"){
                         cell.vwBackgrd.backgroundColor = UIColor(red: 41/255, green: 51/255, blue: 61/255, alpha: 1.0)
                         cell.lblDate.textColor = UIColor.white
                         cell.lblBuilderName.textColor = UIColor.white
                         cell.lblProjectName.textColor = UIColor.white
                         cell.lblBuilderMessage.textColor = UIColor.white
                         cell.lblReadMore.textColor = UIColor.white
                    }
                }
                cell.coffigureBuilderMessageCell(myDate: dict)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.messageArr.count>=indexPath.row{
           if  let messageDic = self.searchMessageArr[indexPath.row] as? NSDictionary{
            let json = JSON(messageDic)
            let details = self.storyboard?.instantiateViewController(withIdentifier: "BuilderMessageDetailsVC") as! BuilderMessageDetailsVC
                details.builderName = json["builder_name"].stringValue
                details.orgName = json["project_name"].stringValue
                details.message = json["message"].stringValue
                details.createdDate = json["created"].stringValue
                details.ide = json["id"].stringValue
                details.status = json["status"].stringValue
            
             self.navigationController?.pushViewController(details, animated: true)
            }
        }
    }
    
}

