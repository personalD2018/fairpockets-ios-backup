//
//  VideoGalleryVC.swift
//  Fairpockets Partner
//
//  Created by Ashim Samanta on 16/10/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

struct Video {
    var ytLink: String?
    var ytLinkKey: String?
}

class VideoGalleryVC: UIViewController {
    
    @IBOutlet weak var tbl_videos: UITableView!
    var videoDict: NSDictionary?
    var rows = [Video?]();
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "vide_gallery_title_key"))
        
        tbl_videos.tableFooterView = UIView()
        tbl_videos.estimatedRowHeight = 300;
        tbl_videos.rowHeight = UITableViewAutomaticDimension
        
        var video: Video = Video();
        video.ytLink = videoDict?["youtube_link1"] as? String
        video.ytLinkKey = videoDict?["youtube_link1_key"] as? String
        self.rows.append(video);
        
        video = Video();
        video.ytLink = videoDict?["youtube_link2"] as? String
        video.ytLinkKey = videoDict?["youtube_link2_key"] as? String
        self.rows.append(video);
    }
}

extension VideoGalleryVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rows.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "VideoCell")
        let imgView = cell?.contentView.viewWithTag(101) as? UIImageView
        imgView?.image = UIImage(named:"video_cell_img")
        return cell ?? UITableViewCell();
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let playerVC = VideoPlayerVC.instantiate(fromAppStoryboard: .Main)
        playerVC.videoLink = self.rows[indexPath.row]?.ytLinkKey
        self.navigationController?.pushViewController(playerVC, animated: true)
    }
    
}
