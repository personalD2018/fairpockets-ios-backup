//
//  PriceCAlculatorViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 05/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import NVActivityIndicatorView
import DropDown

var jsonObj = JSON()

class PriceCAlculatorViewController: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {
    
    //#MARK:- Outlets
    
    @IBOutlet var tblPriceCalculator: UITableView!
    
    //#MARK:- Declaration

    var BuilderId = ""
    var BuilderName = ""
    var ProjectId = ""
    var ProjectName = ""
    var UnitName = ""
    var floorChoice = [FloorChoice]()
    var discount = [Discount]()
    var optionalCharges = [OptionalCharges]()
    var propertyDetail = [PropertyDetail]()
    var mandatoryCharges = [MandatoryCharges]()
    var isshowDiscount = false
    var isshowInputCredit = false
    var gstInputCredit = ""
    var final_calculated_price = Double()
    var PriceCalculatorResponse = [[String:AnyObject]]()
    var chartData = [ChartData]()
    var TempChartData = [ChartData]()
    var isDiscountShow = false
    var NumberOfExtraTableCell = Int()
    var OptionalchargeCurrentIndex = Int()
    var CalculationCurrentIndex = Int()
    var isRegistrationSelection = true
    var selectedFloor = String()
    var selectFloor = -1
    var FloorDD = DropDown()
    var theFloorList:[String] = [String]()
    var OneTimeDDConfigure = Bool()
    var discountCurrentValue = String()
    var discountCurrentUnit = String()
    var discountMeximumValue = Int()
    var isFormSubmit = Bool()
    var strPdfLink = String()
    var baseRate = String()
    var baseCharges = Int()
    var propertyId = String()
    var textfieldIndexPath : IndexPath?
    //#MARK:- View LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "PriceCalculator_key"))
        self.tblPriceCalculator.dataSource = self
        self.tblPriceCalculator.delegate = self
        tblPriceCalculator.register(UINib(nibName: "PriceHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "PriceHeaderTableViewCell")
        tblPriceCalculator.register(UINib(nibName: "MendatoryChargesTableViewCell", bundle: nil), forCellReuseIdentifier: "MendatoryChargesTableViewCell")
        tblPriceCalculator.register(UINib(nibName: "MandatoryValueTableViewCell", bundle: nil), forCellReuseIdentifier: "MandatoryValueTableViewCell")
         tblPriceCalculator.register(UINib(nibName: "OptionalChargesTableViewCell", bundle: nil), forCellReuseIdentifier: "OptionalChargesTableViewCell")
        tblPriceCalculator.register(UINib(nibName: "CalculationTableViewCell", bundle: nil), forCellReuseIdentifier: "CalculationTableViewCell")
         tblPriceCalculator.register(UINib(nibName: "OtherCalculationTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherCalculationTableViewCell")
        tblPriceCalculator.register(UINib(nibName: "CalculateTableViewCell", bundle: nil), forCellReuseIdentifier: "CalculateTableViewCell")
        tblPriceCalculator.register(UINib(nibName: "FloorChoiceTableViewCell", bundle: nil), forCellReuseIdentifier: "FloorChoiceTableViewCell")
         tblPriceCalculator.register(UINib(nibName: "CalculatedPriceTableViewCell", bundle: nil), forCellReuseIdentifier: "CalculatedPriceTableViewCell")
        tblPriceCalculator.register(UINib(nibName: "DiscountTableViewCell", bundle: nil), forCellReuseIdentifier: "DiscountTableViewCell")
        
        
        tblPriceCalculator.tableFooterView = UIView(frame: CGRect.zero)
        tblPriceCalculator.estimatedRowHeight = 500
        tblPriceCalculator.rowHeight = UITableViewAutomaticDimension
        let myString = self.UnitName
        print(myString)
        var unitArray = myString.components(separatedBy: "/")
        print(unitArray)
        propertyId = unitArray[1].trimmingCharacters(in: .whitespaces)
        print(propertyId)
        if(!isSales)
        {
            if(BuilderId != "" && ProjectId != "" && UnitName != "")
            {
                getPriceCaclulator()
            }
            else{
                makeToast(strMessage: somethingWentWrongTryAgain!)
                self.navigationController?.popViewController(animated: true)
            }
        }else{
            if(ProjectId != "" && UnitName != "")
            {
                getPriceCaclulator()
            }
            else{
                makeToast(strMessage: somethingWentWrongTryAgain!)
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        savedPdf = ""
    }
    
    //#MARK:- Member Function
    func converValue(val:Double) -> String {
        let currency = val
        //var shortenedAmount: Float = actualAmount
        var shortenedAmount: Float = Float(currency)
        var suffix = ""
        if currency >= 10000000.0 {
            suffix = "Cr"
            shortenedAmount /= 10000000.0
        } else if currency >= 1000000.0 {
            suffix = "Lakh"
            shortenedAmount /= 100000.0
            //} else if currency >= 100000.0 {
            //    suffix = "Lakh"
            //    shortenedAmount /= 100000.0
        } else if currency >= 1000.0 {
            //suffix = "K"
            //shortenedAmount /= 1000.0
        }
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        let numberAsString = numberFormatter.string(from: shortenedAmount as NSNumber)
        let requiredString = "\(numberAsString ?? "") \(suffix)"
        
        print("Required String :\(requiredString)")
        return requiredString

    }
    func getAmount(bal:Int) -> String
    {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = "INR"
        let BaseChargesAmount: String? = formatter.string(for: bal)
        let BaseCharges = BaseChargesAmount!.components(separatedBy: ".")
        return BaseCharges[0]
    }
    func HideShowDiscount()
    {
        if(isDiscountShow == true)
        {
            isDiscountShow = false
            
        }else{
            isDiscountShow = true
        }
        tblPriceCalculator.reloadData()
    
    }
    
    func ShowDropDown()
    {
        print("theFloorList-->\(theFloorList.count)")
        if(theFloorList.count > 0)
        {
            showDropDown()
        }
        
    }
    
    func AddRemoveOptioanlCharges(index:Int)
    {
        var Calculatorindex = Int()
        var OptionalCharges = Int()
        var isAdded = false
        var selectedTitle = ""
        var selectedChargeValue = ""
        for (ind,data) in PriceCalculatorResponse.enumerated()
        {
            let title = data["Title"] as? String
            if(title == "Calculation")
            {
                Calculatorindex = ind
                CalculationCurrentIndex = ind
            }
            if(title == "Optional Charges")
            {
                print("OptionalchargeCurrentIndex \(OptionalchargeCurrentIndex)")
                OptionalCharges = ind
                OptionalchargeCurrentIndex = ind
            }
        }
        let Optional = NSMutableDictionary(dictionary: (PriceCalculatorResponse[OptionalCharges]))
        print(Optional)
        var array = Optional["Data"] as! NSArray
        print(array)
        let OptionalMutableArray = NSMutableArray(array: array)
        let Optionachange = OptionalMutableArray[index] as! NSDictionary
        let OptionalMutableDic = NSMutableDictionary(dictionary: Optionachange)
        let isSelect = OptionalMutableDic["isSelected"] as? Bool
        if(isSelect == true)
        {
            OptionalMutableDic["isSelected"] =  false
            isAdded = true
            selectedTitle = OptionalMutableDic["charge_title"] as! String
            selectedChargeValue = OptionalMutableDic["charge_value"] as! String
            
        }else{
            OptionalMutableDic["isSelected"] =  true
            isAdded = false
            selectedTitle = OptionalMutableDic["charge_title"] as! String
            selectedChargeValue = OptionalMutableDic["charge_value"] as! String
        }
        let post_Optionachange = OptionalMutableDic as! [String:AnyObject]
        OptionalMutableArray.replaceObject(at: index, with: post_Optionachange)
        let post_array = OptionalMutableArray as NSArray
        array = post_array
        Optional.setValue(array, forKey: "Data")
        print(Optional)
        PriceCalculatorResponse.remove(at: OptionalCharges)
        PriceCalculatorResponse.insert((Optional as! [String : AnyObject]), at: OptionalCharges)
        
        
        let Calculator = NSMutableDictionary(dictionary: (PriceCalculatorResponse[Calculatorindex]))
        print(Calculator)
        var Calculatorarray = Calculator["Data"] as! NSArray
        let CalculatorMutableArray = NSMutableArray(array: Calculatorarray)
         let CalculatorTempMutableArray = NSMutableArray()
        print(CalculatorMutableArray)
        if(isAdded == true)
        {
            for (Index,Calculat) in CalculatorMutableArray.enumerated()
            {
                let dic = Calculat as! NSDictionary
                print(dic)
                print("Index-->\(Index)")
                if let title = dic["charge_title"]
                {
                    if(selectedTitle == (dic["charge_title"] as! String) && selectedChargeValue  == (dic["charge_value"] as! String))
                    {
                        CalculatorMutableArray.remove(Calculat)
                        let Calculator_array = CalculatorMutableArray as NSArray
                        print(Calculator_array)
                        Calculatorarray = Calculator_array
                        Calculator.setValue(Calculatorarray, forKey: "Data")
                        print(Calculator)
                        NumberOfExtraTableCell -= 1
                        PriceCalculatorResponse.remove(at: Calculatorindex)
                        PriceCalculatorResponse.insert((Calculator as! [String : AnyObject]), at: Calculatorindex)
                    }
                }
            }
            
            
        }else{
            
            var Optionaldic = [String:AnyObject]()
            Optionaldic["charge_title"] = OptionalMutableDic["charge_title"] as AnyObject
            Optionaldic["charge_value"] = OptionalMutableDic["charge_value"] as AnyObject
            Optionaldic["unit_type"] = OptionalMutableDic["unit_type"] as AnyObject
            CalculatorTempMutableArray.add(Optionaldic)
            CalculatorTempMutableArray.addObjects(from: Calculatorarray as! [Any])
            
            let Calculator_array = CalculatorTempMutableArray as NSArray
            print(Calculator_array)
            /*if(CalculatorMutableArray.count == 2)
            {
                Calculator_array = CalculatorMutableArray.reversed() as NSArray
                print("Calculator_array before-->\(Calculator_array)")
                CalculatorMutableArray.add(Optionaldic)
                Calculator_array = CalculatorMutableArray.reversed() as NSArray
                print("Calculator_array after-->\(Calculator_array)")
            }
            else if(CalculatorMutableArray.count > 2)
            {
                print("Calculator_array before-->\(Calculator_array)")
                CalculatorMutableArray.add(Optionaldic)
                Calculator_array = CalculatorMutableArray.reversed() as NSArray
                print("Calculator_array after-->\(Calculator_array)")
            }*/
           
            Calculatorarray = Calculator_array
            Calculator.setValue(Calculatorarray, forKey: "Data")
            print(Calculator)
            NumberOfExtraTableCell += 1
            PriceCalculatorResponse.remove(at: Calculatorindex)
            PriceCalculatorResponse.insert((Calculator as! [String : AnyObject]), at: Calculatorindex)
        }
        
        print(PriceCalculatorResponse)
        print(NumberOfExtraTableCell)
        GetCalculatData()
        //self.tblPriceCalculator.reloadData()
    }
    
    
    //#MARK:- Button Action
    
    func saveBtnAction()
    {
        var myJson = jsonObj
        var jsonPropertyDetails = myJson["property_detail"]
        if(isRegistrationSelection)
        {
            jsonPropertyDetails["registration_charges"].stringValue = propertyDetail[0].registrationCharges!
            
        }else{
            jsonPropertyDetails["registration_charges"].stringValue = "0 %"
        }
        jsonPropertyDetails["properties_id"].stringValue = propertyId
        jsonPropertyDetails["floor_no"].stringValue = selectedFloor
        jsonPropertyDetails["project_id"].stringValue = self.ProjectId
        jsonPropertyDetails["property_area"].stringValue = propertyDetail[0].propertyArea!
        jsonPropertyDetails["base_rate"].stringValue = self.baseRate
        jsonPropertyDetails["stamp_duty"].stringValue = propertyDetail[0].stampDuty!
        jsonPropertyDetails["gst_on_base"].stringValue = propertyDetail[0].gstOnBase!
        jsonPropertyDetails["gst_on_others"].stringValue = propertyDetail[0].gstOnOthers!
        jsonPropertyDetails["property_possession_charge_total"].stringValue = propertyDetail[0].propertyPossessionChargeTotal ?? "0"
        myJson["property_detail"] = jsonPropertyDetails
        myJson["final_calculated_price"].stringValue = "\(final_calculated_price)"
        myJson["properties_id"].stringValue = propertyId
        var jsonOptionalcharges = myJson["optional_charges"].arrayValue
        if jsonOptionalcharges.count == 0
        {
             myJson["optional_charges"] = []
        }
        else
        {
            let OptionalCharge = PriceCalculatorResponse[OptionalchargeCurrentIndex]
            let OptionalData = OptionalCharge["Data"] as! NSArray
            for index in stride(from: OptionalData.count-1, to: -1, by: -1) {
                let dic = OptionalData[index] as! NSDictionary
                let isSelect = dic["isSelected"] as? Bool
                print(index)
                if(isSelect == false)
                {
                    jsonOptionalcharges.remove(at: index)
                }
            }
            myJson["optional_charges"].arrayObject = jsonOptionalcharges
        }
       
        myJson["showInputCredit"].stringValue = "\(self.isshowInputCredit)"
        myJson["gstInputCredit"].stringValue = self.gstInputCredit
        myJson["floor_choice"].dictionaryObject?.removeAll()
        var jsonDiscount = myJson["discount"]
        if(isDiscountShow == true)
        {
            myJson["showDiscount"].stringValue = "true"            
            jsonDiscount["disc_amount"].stringValue = discountCurrentValue
            myJson["discount"] = jsonDiscount
            print(myJson)
        }else{
            myJson["showDiscount"].stringValue = "false"
            jsonDiscount.dictionaryObject?.removeAll()
        }
        myJson["discount"] = jsonDiscount
        print(myJson)
        
        FinaljsonObj = myJson
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SaveDetailsPopupVc") as! SaveDetailsPopupVc
        obj.pickerDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
    }
    
    @objc func btnViewAction()
    {
        if isFormSubmit == false
        {
             makeToast(strMessage: mapping.string(forKey: "Save_calculator_data_to_generate_to_pdf_key")!)
            return
        }
        let pdfView = self.storyboard!.instantiateViewController(withIdentifier: "PDFViewController") as! PDFViewController
        pdfView.urls = strPdfLink
        self.navigationController?.pushViewController(pdfView, animated: true)
    }
    
    @objc func btnShareAction()
    {
        if isFormSubmit == false
        {
            makeToast(strMessage: mapping.string(forKey: "Save_calculator_data_to_generate_to_pdf_key")!)
            return
        }
        shareSocialMedia()
    }
    
    func shareSocialMedia()
    {
        let urlString = strPdfLink
        print(urlString)
        let vc = UIActivityViewController(activityItems: [urlString], applicationActivities: nil)
        let excludeActivities = [
            UIActivityType.airDrop,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToFacebook,
            UIActivityType.postToTwitter,
            UIActivityType.postToWeibo,
            UIActivityType.message,
            UIActivityType.mail,
            UIActivityType.copyToPasteboard,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToTencentWeibo
        ]
    //    vc.excludedActivityTypes = excludeActivities;
        self.present(vc, animated: true)
    }
    //#MARK:-  DropDown show/hide method
    
    func showDropDown() {
        FloorDD.dataSource = theFloorList
        FloorDD.show()
        FloorDD.selectionAction = { (index,item) in
            self.selectFloor = index
            self.selectedFloor = item
            self.GetFloorSelectionData()
            
        }
    }
    func hideDropDown() {
        FloorDD.hide()
    }
    //#MARK:- Config DropDown
    func configDropdown(dropdown: DropDown, sender: UIView) {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        //dropdown.width = sender.frame.width
        dropdown.width = 80
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        //strOffset = "0"
        // getCategoryList()
        print("Abhay")
        print("textfieldIndexPath - ",textfieldIndexPath)
        if let number = Int(textField.text!)
        {
            if(number <= discountMeximumValue)
            {
                discountCurrentValue = textField.text!
               // GetDiscountData()
                
            }else{
                discountCurrentValue = ""
               // self.tblPriceCalculator.reloadData()
                
            }
            //let sectionIndex:Int = self.textfieldIndexPath!.section
            //tblPriceCalculator.reloadSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .automatic)
            //let Calculator = NSMutableDictionary(dictionary: (PriceCalculatorResponse[CalculationCurrentIndex]))
            //print(Calculator)
            //let Calculatorarray = Calculator["Data"] as! NSArray
            let indexPath = IndexPath(item: self.textfieldIndexPath!.row, section: self.textfieldIndexPath!.section)
            self.tblPriceCalculator.reloadRows(at: [indexPath], with: .none)
            let cell = tblPriceCalculator.cellForRow(at: textfieldIndexPath!) as?FloorChoiceTableViewCell
            cell?.txtDiscount.becomeFirstResponder()
        }
        
    }
    @objc func doneButtonClicked(_ sender: Any) {
        if(discountCurrentValue != "")
        {
            if(Int(discountCurrentValue)! <= discountMeximumValue)
            {
                GetDiscountData()
            }
        }
        else{
            self.tblPriceCalculator.reloadData()
        }
        
    }
    
    
    //#MARK:- API Calling
    func getPriceCaclulator()
    {
        let unit = UnitName.components(separatedBy: "-")
        print(unit)
        let unit1 = unit[1].components(separatedBy: " ")
        print(unit1)
        let sqrft = unit1[1].trimmingCharacters(in: .whitespaces)
        print(sqrft)
        var para = [String:AnyObject]()
        para["user_id"] = UserId as AnyObject
        para["project_id"] = ProjectId as AnyObject
        para["property_area"] = sqrft as AnyObject
        para["properties_id"] = propertyId as AnyObject
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetPriceCalculatorManager().getPriceCalculator(priojectId: ProjectId, param: para) { [weak self] PriceCalculator in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard PriceCalculator == nil else{
                    if(PriceCalculator!.code == 1)
                    {
                        self!.floorChoice = [PriceCalculator!.floorChoice!]
                        self!.discount = [PriceCalculator!.discount!]
                        //self!.optionalCharges = PriceCalculator!.optionalCharges!
                        self!.propertyDetail = [PriceCalculator!.propertyDetail!]
                         self!.baseRate = self!.propertyDetail[0].baseRate ?? "0"
                       // self!.mandatoryCharges = PriceCalculator!.mandatoryCharges!
                        
                        
                        if let chartdata = PriceCalculator!.chartData
                        {
                            self!.chartData = [chartdata]
                            self!.TempChartData = [chartdata]
                        }
                        self!.baseCharges = self!.chartData[0].baseCharges!
                        print(self!.TempChartData[0].stampPlusRegistration ?? 0)
                        self!.isshowDiscount = Bool(PriceCalculator!.showDiscount!)!
                        self!.isshowInputCredit = Bool(PriceCalculator!.showInputCredit!)!
                        if(self!.isshowInputCredit == true)
                        {
                            self!.gstInputCredit = PriceCalculator!.gstInputCredit!
                        }
                        
                        self!.final_calculated_price = PriceCalculator!.finalCalculatedPrice!
                        self!.PriceCalculatorResponse = [[String:AnyObject]]()
                        var dic = [String:AnyObject]()
                        dic["Title"] = "Header" as AnyObject
                        self!.PriceCalculatorResponse.append(dic)
                        
                        if PriceCalculator!.mandatoryCharges != nil
                        {
                            var dic = [String:AnyObject]()
                            dic["Title"] = "Madatory Charges" as AnyObject
                            var Array = [[String:AnyObject]]()
                            var Mandatorydic = [String:AnyObject]()
                            PriceCalculator!.mandatoryCharges!.forEach { mandatoryCharges in
                                Mandatorydic["charge_title"] = mandatoryCharges.chargeTitle as AnyObject
                                Mandatorydic["charge_value"] = mandatoryCharges.chargeValue as AnyObject
                                Mandatorydic["unit_type"] = mandatoryCharges.unitType as AnyObject
                                Mandatorydic["isSelected"] = true as AnyObject
                                Array.append(Mandatorydic)
                                Mandatorydic = [String:AnyObject]()
                            }
                            dic["Data"] = Array as AnyObject
                            self!.PriceCalculatorResponse.append(dic)
                        }
                        
                        if PriceCalculator!.optionalCharges != nil
                        {
                            var dic = [String:AnyObject]()
                            dic["Title"] = "Optional Charges" as AnyObject
                            var Array = [[String:AnyObject]]()
                            var Optionaldic = [String:AnyObject]()
                            PriceCalculator!.optionalCharges!.forEach { optionalCharges in
                                Optionaldic["charge_title"] = optionalCharges.chargeTitle as AnyObject
                                Optionaldic["charge_value"] = optionalCharges.chargeValue as AnyObject
                                Optionaldic["unit_type"] = optionalCharges.unitType as AnyObject
                                Optionaldic["isSelected"] = false as AnyObject
                                Array.append(Optionaldic)
                                Optionaldic = [String:AnyObject]()
                            }
                            dic["Data"] = Array as AnyObject
                            self!.PriceCalculatorResponse.append(dic)
                            for (ind,data) in self!.PriceCalculatorResponse.enumerated()
                            {
                                let title = data["Title"] as? String
                                if(title == "Calculation")
                                {
                                    self!.CalculationCurrentIndex = ind
                                }
                                if(title == "Optional Charges")
                                {
                                    self!.OptionalchargeCurrentIndex = ind
                                }
                            }
                        }
                        var dic3 = [String:AnyObject]()
                        dic3["Title"] = "Calculation" as AnyObject
                        var Array = [[String:AnyObject]]()
                        var Floordic = [String:AnyObject]()
                        if let startloop = PriceCalculator!.floorChoice!.startloop, let endLoop = PriceCalculator!.floorChoice!.endloop {
                            Floordic["floor_start"] = startloop as AnyObject
                            self?.selectedFloor = startloop
                            Floordic["floor_end"] = endLoop as AnyObject
                            
                            let strtFlr = Int(startloop)!
                            let endFlr = Int(endLoop)!
                            for i in strtFlr...endFlr
                            {
                                self?.theFloorList.append("\(i)")
                            }
                        }
                        Array.append(Floordic)
                        
                        
                       // print("theFloorList-->\(self!.theFloorList)")
                        if PriceCalculator!.discount!.discAmount != nil
                        {
                            var Discountrdic = [String:AnyObject]()
                            Discountrdic["disc_amount"] = PriceCalculator!.discount!.discAmount as AnyObject
                            Discountrdic["disc_unit"] = PriceCalculator!.discount!.discUnit! as AnyObject
                            Array.append(Discountrdic)
                            self!.discountCurrentValue = PriceCalculator!.discount!.discAmount as! String
                            self!.discountCurrentUnit = PriceCalculator!.discount!.discUnit as! String
                            self!.discountMeximumValue = Int(PriceCalculator!.discount!.discAmount as! String)!
                            //print(self!.discountCurrentValue,self!.discountMeximumValue)
                            
                        }else{
                            var Discountrdic = [String:AnyObject]()
                            Discountrdic["disc_amount"] = "" as AnyObject
                            Discountrdic["disc_unit"] = "" as AnyObject
                            
                            Array.append(Discountrdic)
                        }
                        dic3["Data"] = Array as AnyObject
                        self!.PriceCalculatorResponse.append(dic3)
                        var dic1 = [String:AnyObject]()
                        dic1["Title"] = "Footer" as AnyObject
                        self!.PriceCalculatorResponse.append(dic1)
                        print("PriceCalculatorResponse-->\(self!.PriceCalculatorResponse)")
                        self!.tblPriceCalculator.reloadData()
                       
                    }else{
                        self!.makeToast(strMessage: PriceCalculator!.message!)
                    }
                    
                    return
                }
                
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }

    func AddRemoveRegistrationCharges()
    {
        var myJson = jsonObj
        var jsonChartData = myJson["chart_data"]
        jsonChartData["additional_charges"].stringValue = "\(self.TempChartData[0].additionalCharges ?? 0)"
        jsonChartData["base_charges"].stringValue = "\(self.TempChartData[0].baseCharges ?? 0)"
        jsonChartData["gst_total"].stringValue = "\(self.TempChartData[0].gstTotal ?? 0)"
        //print(self.TempChartData[0].stampPlusRegistration ?? 0)
        if(Int(self.TempChartData[0].stampPlusRegistration ?? 0) > 0)
        {
            jsonChartData["stamp_plus_registration"].stringValue = "\(self.TempChartData[0].stampPlusRegistration!)"
        }else{
            jsonChartData["stamp_plus_registration"].stringValue = "0 Rs"
        }
        myJson["chart_data"] = jsonChartData
        
        var jsonPropertyDetails = myJson["property_detail"]
        if(isRegistrationSelection)
        {
            isRegistrationSelection = false
            jsonPropertyDetails["registration_charges"].stringValue = "0 %"
            
        }else{
            isRegistrationSelection = true
            jsonPropertyDetails["registration_charges"].stringValue = propertyDetail[0].registrationCharges!
        }
        jsonPropertyDetails["properties_id"].stringValue = propertyId
        jsonPropertyDetails["floor_no"].stringValue = selectedFloor
        jsonPropertyDetails["project_id"].stringValue = self.ProjectId
        jsonPropertyDetails["property_area"].stringValue = propertyDetail[0].propertyArea ?? "0"
        jsonPropertyDetails["base_rate"].stringValue = self.baseRate
        jsonPropertyDetails["stamp_duty"].stringValue = propertyDetail[0].stampDuty ?? "0"
        jsonPropertyDetails["gst_on_base"].stringValue = propertyDetail[0].gstOnBase ?? "0"
        jsonPropertyDetails["gst_on_others"].stringValue = propertyDetail[0].gstOnOthers ?? "0"
        jsonPropertyDetails["property_possession_charge_total"].stringValue = propertyDetail[0].propertyPossessionChargeTotal ?? "0"
        myJson["property_detail"] = jsonPropertyDetails
        myJson["final_calculated_price"].stringValue = "\(final_calculated_price)"
        myJson["properties_id"].stringValue = propertyId
        var jsonOptionalcharges = myJson["optional_charges"].arrayValue
        if jsonOptionalcharges.count == 0
        {
             myJson["optional_charges"] = []
        }
        else
        {
            let OptionalCharge = PriceCalculatorResponse[OptionalchargeCurrentIndex]
           // print("OptionalCharge \(OptionalCharge)")
            let OptionalData = OptionalCharge["Data"] as! NSArray
            for index in stride(from: OptionalData.count-1, to: -1, by: -1) {
                let dic = OptionalData[index] as! NSDictionary
                let isSelect = dic["isSelected"] as? Bool
                //print("index \(isSelect)")
                if(isSelect == false)
                {
                    jsonOptionalcharges.remove(at: index)
                }else{
                    jsonOptionalcharges[index]["serialNo"].stringValue = "\(100+index)"
                    jsonOptionalcharges[index]["isChecked"].stringValue = "true"
                }
            }
            myJson["optional_charges"].arrayObject = jsonOptionalcharges
        }
       
      //  myJson["optional_charges"].arrayObject = jsonOptionalcharges
        myJson["showInputCredit"].stringValue = "\(self.isshowInputCredit)"
        myJson["gstInputCredit"].stringValue = self.gstInputCredit
        myJson["floor_choice"].dictionaryObject?.removeAll()
        var jsonDiscount = myJson["discount"]
        if(isDiscountShow == true)
        {
            myJson["showDiscount"].stringValue = "true"
            
            jsonDiscount["disc_amount"].stringValue = discountCurrentValue
            myJson["discount"] = jsonDiscount
            print(myJson)
        }else{
            myJson["showDiscount"].stringValue = "false"
            jsonDiscount.dictionaryObject?.removeAll()
        }
        myJson["discount"] = jsonDiscount
        print(myJson)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetRegistrationChargesManager().getRegistrationCharges(json: myJson.rawString()!) { [weak self] RegistrationCharges in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard RegistrationCharges == nil else{
                    if(RegistrationCharges!.code == 1)
                    {
                        self!.final_calculated_price = Double(RegistrationCharges!.finalCalculatedPrice!)
                        if let chartdata = RegistrationCharges!.chartData
                        {
                            self!.TempChartData = [chartdata]
                        }
                        /*self!.propertyDetail[0].propertyPossessionChargeTotal = RegistrationCharges!.propertyDetail!.propertyPossessionChargeTotal ?? "0"
                        self!.propertyDetail[0].propertyLawnValueTotal = RegistrationCharges!.propertyDetail!.propertyLawnValueTotal!
                        self!.propertyDetail[0].propertyTerraceValueTotal = RegistrationCharges!.propertyDetail!.propertyTerraceValueTotal!
                        
                        self!.propertyDetail[0].baseRate = RegistrationCharges!.propertyDetail!.baseRate ?? "0"
                        self!.propertyDetail[0].propertyArea = RegistrationCharges!.propertyDetail!.propertyArea ?? "0"*/
                        if let baseRate = RegistrationCharges!.baseRate
                        {
                            self!.baseRate = baseRate
                        }
                        self!.baseRate = RegistrationCharges!.baseRate ?? "0"
                        self!.tblPriceCalculator.reloadData()
                        return
                        
                    }else{
                        self!.makeToast(strMessage: RegistrationCharges!.message!)
                    }
                    self!.tblPriceCalculator.reloadData()
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }
    func GetFloorSelectionData()
    {
        var myJson = jsonObj
       // print(myJson)
        var jsonPropertyDetails = myJson["property_detail"]
        if(isRegistrationSelection)
        {
            jsonPropertyDetails["registration_charges"].stringValue = propertyDetail[0].registrationCharges!
            
        }else{
            jsonPropertyDetails["registration_charges"].stringValue = "0 %"
        }
        jsonPropertyDetails["properties_id"].stringValue = propertyId
        jsonPropertyDetails["floor_no"].stringValue = selectedFloor
        jsonPropertyDetails["project_id"].stringValue = self.ProjectId
        jsonPropertyDetails["property_area"].stringValue = propertyDetail[0].propertyArea!
        jsonPropertyDetails["base_rate"].stringValue = self.baseRate
        jsonPropertyDetails["stamp_duty"].stringValue = propertyDetail[0].stampDuty!
        jsonPropertyDetails["gst_on_base"].stringValue = propertyDetail[0].gstOnBase!
        jsonPropertyDetails["gst_on_others"].stringValue = propertyDetail[0].gstOnOthers!
        jsonPropertyDetails["property_possession_charge_total"].stringValue = propertyDetail[0].propertyPossessionChargeTotal ?? "0"
        myJson["property_detail"] = jsonPropertyDetails
        myJson["final_calculated_price"].stringValue = "\(final_calculated_price)"
        myJson["properties_id"].stringValue = propertyId
        var jsonOptionalcharges = myJson["optional_charges"].arrayValue
       // print(jsonOptionalcharges)
        if jsonOptionalcharges.count == 0
        {
            myJson["optional_charges"] = []
        }
        else
        {
            let OptionalCharge = PriceCalculatorResponse[OptionalchargeCurrentIndex]
            let OptionalData = OptionalCharge["Data"] as! NSArray
            for index in stride(from: OptionalData.count-1, to: -1, by: -1) {
                let dic = OptionalData[index] as! NSDictionary
                let isSelect = dic["isSelected"] as! Bool
              //  print(index)
                if(isSelect == false)
                {
                    jsonOptionalcharges.remove(at: index)
                }else{
                    jsonOptionalcharges[index]["serialNo"].stringValue = "\(100+index)"
                    jsonOptionalcharges[index]["isChecked"].stringValue = "true"
                }
            }
            myJson["optional_charges"].arrayObject = jsonOptionalcharges
        }
        myJson["showInputCredit"].stringValue = "\(self.isshowInputCredit)"
        myJson["gstInputCredit"].stringValue = self.gstInputCredit
        myJson["floor_choice"].dictionaryObject?.removeAll()
        myJson["chart_data"].dictionaryObject?.removeAll()
        var jsonDiscount = myJson["discount"]
        if(isDiscountShow == true)
        {
            myJson["showDiscount"].stringValue = "true"
            
            jsonDiscount["disc_amount"].stringValue = discountCurrentValue
            myJson["discount"] = jsonDiscount
            print(myJson)
        }else{
            myJson["showDiscount"].stringValue = "false"
            jsonDiscount.dictionaryObject?.removeAll()
        }
        myJson["discount"] = jsonDiscount
        print(myJson)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetRegistrationChargesManager().getFloorWishData(json: myJson.rawString()!) { [weak self] RegistrationCharges in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard RegistrationCharges == nil else{
                    if(RegistrationCharges!.code == 1)
                    {
                        self!.final_calculated_price = Double(RegistrationCharges!.finalCalculatedPrice!)
                        if let chartdata = RegistrationCharges!.chartData
                        {
                            self!.TempChartData = [chartdata]
                        }
                        self!.propertyDetail[0].propertyPossessionChargeTotal = RegistrationCharges!.propertyDetail!.propertyPossessionChargeTotal ?? "0"                        
                        self!.propertyDetail[0].propertyLawnValueTotal = RegistrationCharges!.propertyDetail!.propertyLawnValueTotal!
                        self!.propertyDetail[0].propertyTerraceValueTotal = RegistrationCharges!.propertyDetail!.propertyTerraceValueTotal!
                        
                        self!.propertyDetail[0].baseRate = RegistrationCharges!.propertyDetail!.baseRate ?? "0"
                        self!.propertyDetail[0].propertyArea = RegistrationCharges!.propertyDetail!.propertyArea ?? "0"
                        if let baseRate = RegistrationCharges!.baseRate
                        {
                            self!.baseRate = baseRate
                        }
                        self!.baseRate = RegistrationCharges!.baseRate ?? "0"
                        print(self!.baseRate)
                        self!.tblPriceCalculator.reloadData()
                        return
                        
                    }else{
                        self!.makeToast(strMessage: RegistrationCharges!.message!)
                    }
                    self!.tblPriceCalculator.reloadData()
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }
    func GetDiscountData()
    {
        var myJson = jsonObj
        //print("myJson \(myJson)")
        var jsonChartData = myJson["chart_data"]
       // print("jsonChartData \(jsonChartData)")
       // print("TempChartData \(TempChartData[0])")
       // print("TempChartData \(self.TempChartData[0].additionalCharges ?? 0)")
        jsonChartData["additional_charges"].stringValue = "\(self.TempChartData[0].additionalCharges ?? 0)"
        jsonChartData["base_charges"].stringValue = "\(self.TempChartData[0].baseCharges ?? 0)"
        jsonChartData["gst_total"].stringValue = "\(self.TempChartData[0].gstTotal ?? 0)"
        var stampPlus = propertyDetail[0].registrationCharges ?? "0"
        stampPlus = stampPlus.replacingOccurrences(of: "%", with: "")
        print(stampPlus)
       //  stampPlus = stampPlus.replacingOccurrences(of: " ", with: "")
        jsonChartData["stamp_plus_registration"].stringValue = "\(stampPlus)"
       /* if(self.TempChartData[0].stampPlusRegistration != nil)
        {
            let stamp = self.TempChartData[0].stampPlusRegistration!
            print(stamp)
            if(Int(self.TempChartData[0].stampPlusRegistration!) > 0)
            {
                jsonChartData["stamp_plus_registration"].stringValue = "\(self.TempChartData[0].stampPlusRegistration!)"
            }else{
                jsonChartData["stamp_plus_registration"].stringValue = "0 Rs"
            }
        }else{
            jsonChartData["stamp_plus_registration"].stringValue = "0 Rs"
        }*/
        
        myJson["chart_data"] = jsonChartData
        //print("myJson \(myJson)")
        //print("jsonChartData \(jsonChartData)")
        var jsonPropertyDetails = myJson["property_detail"]
        if(isRegistrationSelection)
        {
            jsonPropertyDetails["registration_charges"].stringValue = propertyDetail[0].registrationCharges ?? "0"
        }else{
            jsonPropertyDetails["registration_charges"].stringValue = "0 %"
        }
        jsonPropertyDetails["project_id"].stringValue = self.ProjectId
        jsonPropertyDetails["properties_id"].stringValue = propertyId
        jsonPropertyDetails["floor_no"].stringValue = selectedFloor
        jsonPropertyDetails["property_area"].stringValue = propertyDetail[0].propertyArea ?? "0"
        
        jsonPropertyDetails["base_rate"].stringValue = self.baseRate
        jsonPropertyDetails["stamp_duty"].stringValue = propertyDetail[0].stampDuty ?? "0"
        jsonPropertyDetails["gst_on_base"].stringValue = propertyDetail[0].gstOnBase ?? "0"
        jsonPropertyDetails["gst_on_others"].stringValue = propertyDetail[0].gstOnOthers ?? "0"
        jsonPropertyDetails["property_possession_charge_total"].stringValue = propertyDetail[0].propertyPossessionChargeTotal ?? "0"
        myJson["property_detail"] = jsonPropertyDetails
        myJson["final_calculated_price"].stringValue = "\(final_calculated_price)"
        myJson["properties_id"].stringValue = propertyId
        var jsonOptionalcharges = myJson["optional_charges"].arrayValue
        //print("jsonOptionalcharges \(jsonOptionalcharges)")
        if jsonOptionalcharges.count == 0
        {
            myJson["optional_charges"] = []
        }
        else
        {
            //print("OptionalchargeCurrentIndex \(OptionalchargeCurrentIndex)")
            //print("PriceCalculatorResponse \(PriceCalculatorResponse)")
            let OptionalCharge = PriceCalculatorResponse[OptionalchargeCurrentIndex]
            //print("OptionalCharge \(OptionalCharge)")
            let OptionalData = OptionalCharge["Data"] as! NSArray
            //print("OptionalData \(OptionalData)")
            for index in stride(from: OptionalData.count-1, to: -1, by: -1) {
                let dic = OptionalData[index] as! NSDictionary
                let isSelect = dic["isSelected"] as? Bool
                //print(index)
                if(isSelect == false)
                {
                    jsonOptionalcharges.remove(at: index)
                }else{
                    jsonOptionalcharges[index]["serialNo"].stringValue = "\(100+index)"
                    jsonOptionalcharges[index]["isChecked"].stringValue = "true"
                }
            }
            myJson["optional_charges"].arrayObject = jsonOptionalcharges
        }
        
        myJson["showInputCredit"].stringValue = "\(self.isshowInputCredit)"
        myJson["gstInputCredit"].stringValue = self.gstInputCredit
        myJson["floor_choice"].dictionaryObject?.removeAll()
       // myJson["chart_data"].dictionaryObject?.removeAll()
        var jsonDiscount = myJson["discount"]
        if(isDiscountShow == true)
        {
            myJson["showDiscount"].stringValue = "true"
            jsonDiscount["disc_amount"].stringValue = discountCurrentValue
            myJson["discount"] = jsonDiscount
            print(myJson)
        }else{
            myJson["showDiscount"].stringValue = "false"
            jsonDiscount.dictionaryObject?.removeAll()
        }
        myJson["discount"] = jsonDiscount
        print("myJson \(myJson)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetRegistrationChargesManager().getDiscountData(json: myJson.rawString()!) { [weak self] RegistrationCharges in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard RegistrationCharges == nil else{
                    if(RegistrationCharges!.code == 1)
                    {
                        self!.final_calculated_price = Double(RegistrationCharges!.finalCalculatedPrice!)
                        print("final_calculated_price \(self!.final_calculated_price)")
                        print("chartData \(RegistrationCharges!.chartData)")
                        if let chartdata = RegistrationCharges!.chartData
                        {
                            self!.TempChartData = [chartdata]
                        }
                        
                        /*self!.propertyDetail[0].propertyPossessionChargeTotal = RegistrationCharges!.propertyDetail!.propertyPossessionChargeTotal ?? "0"
                        self!.propertyDetail[0].propertyLawnValueTotal = RegistrationCharges!.propertyDetail!.propertyLawnValueTotal!
                        self!.propertyDetail[0].propertyTerraceValueTotal = RegistrationCharges!.propertyDetail!.propertyTerraceValueTotal!
                        
                        self!.propertyDetail[0].baseRate = RegistrationCharges!.propertyDetail!.baseRate ?? "0"
                        self!.propertyDetail[0].propertyArea = RegistrationCharges!.propertyDetail!.propertyArea ?? "0"*/
                        if let baseRate = RegistrationCharges!.baseRate
                        {
                            self!.baseRate = baseRate
                        }
                        self!.baseCharges = self!.TempChartData[0].baseCharges ?? 0
                        self!.tblPriceCalculator.reloadData()
                        return                        
                    }else{
                        self!.makeToast(strMessage: RegistrationCharges!.message!)
                    }
                    self!.tblPriceCalculator.reloadData()
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
    }
    
    func GetCalculatData()
    {
        var myJson = jsonObj
       // print(myJson)
        var jsonPropertyDetails = myJson["property_detail"]
        if(isRegistrationSelection)
        {
            jsonPropertyDetails["registration_charges"].stringValue = propertyDetail[0].registrationCharges!
            
        }else{
            jsonPropertyDetails["registration_charges"].stringValue = "0 %"
        }
        jsonPropertyDetails["project_id"].stringValue = self.ProjectId
        jsonPropertyDetails["properties_id"].stringValue = propertyId
        jsonPropertyDetails["floor_no"].stringValue = selectedFloor
        jsonPropertyDetails["property_area"].stringValue = propertyDetail[0].propertyArea!
        jsonPropertyDetails["base_rate"].stringValue = self.baseRate
        jsonPropertyDetails["stamp_duty"].stringValue = propertyDetail[0].stampDuty!
        jsonPropertyDetails["gst_on_base"].stringValue = propertyDetail[0].gstOnBase!
        jsonPropertyDetails["gst_on_others"].stringValue = propertyDetail[0].gstOnOthers!
        jsonPropertyDetails["gst_on_others"].stringValue = propertyDetail[0].gstOnOthers!
        jsonPropertyDetails["property_possession_charge_total"].stringValue = propertyDetail[0].propertyPossessionChargeTotal ?? "0"
        myJson["property_detail"] = jsonPropertyDetails
        myJson["final_calculated_price"].stringValue = "\(final_calculated_price)"
        myJson["properties_id"].stringValue = propertyId
        var jsonOptionalcharges = myJson["optional_charges"].arrayValue
        if jsonOptionalcharges.count == 0
        {
            myJson["optional_charges"] = []
        }
        else
        {
            let OptionalCharge = PriceCalculatorResponse[OptionalchargeCurrentIndex]
            let OptionalData = OptionalCharge["Data"] as! NSArray
            for index in stride(from: OptionalData.count-1, to: -1, by: -1) {
                let dic = OptionalData[index] as! NSDictionary
                let isSelect = dic["isSelected"] as? Bool
                print(index)
                if(isSelect == false)
                {
                    jsonOptionalcharges.remove(at: index)
                }else{
                    jsonOptionalcharges[index]["serialNo"].stringValue = "\(100+index)"
                    jsonOptionalcharges[index]["isChecked"].stringValue = "true"
                }
            }
            myJson["optional_charges"].arrayObject = jsonOptionalcharges
        }
        myJson["showInputCredit"].stringValue = "\(self.isshowInputCredit)"
        myJson["gstInputCredit"].stringValue = self.gstInputCredit
        myJson["floor_choice"].dictionaryObject?.removeAll()
        myJson["chart_data"].dictionaryObject?.removeAll()
        var jsonDiscount = myJson["discount"]
        if(isDiscountShow == true)
        {
            myJson["showDiscount"].stringValue = "true"
            
            jsonDiscount["disc_amount"].stringValue = discountCurrentValue
            myJson["discount"] = jsonDiscount
            print(myJson)
        }else{
            myJson["showDiscount"].stringValue = "false"
            jsonDiscount.dictionaryObject?.removeAll()
        }
        myJson["discount"] = jsonDiscount
        print(myJson)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NetworkActivityManager.sharedInstance.busy = true
            startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
            GetRegistrationChargesManager().getCalculateData(json: myJson.rawString()!) { [weak self] RegistrationCharges in
                NetworkActivityManager.sharedInstance.busy = false
                self!.stopAnimating()
                guard RegistrationCharges == nil else{
                    if(RegistrationCharges!.code == 1)
                    {
                        self!.final_calculated_price = Double(RegistrationCharges!.finalCalculatedPrice!)
                        self!.TempChartData = [RegistrationCharges!.chartData!]
                        /*self!.propertyDetail[0].propertyPossessionChargeTotal = RegistrationCharges!.propertyDetail!.propertyPossessionChargeTotal ?? "0"
                         self!.propertyDetail[0].propertyLawnValueTotal = RegistrationCharges!.propertyDetail!.propertyLawnValueTotal!
                         self!.propertyDetail[0].propertyTerraceValueTotal = RegistrationCharges!.propertyDetail!.propertyTerraceValueTotal!
                         
                         self!.propertyDetail[0].baseRate = RegistrationCharges!.propertyDetail!.baseRate ?? "0"
                         self!.propertyDetail[0].propertyArea = RegistrationCharges!.propertyDetail!.propertyArea ?? "0"*/
                        if let baseRate = RegistrationCharges!.baseRate
                        {
                            self!.baseRate = baseRate
                        }
                        self!.baseCharges = self!.TempChartData[0].baseCharges ?? 0
                        self!.tblPriceCalculator.reloadData()
                        return
                        
                    }else{
                        self!.makeToast(strMessage: RegistrationCharges!.message!)
                    }
                    self!.tblPriceCalculator.reloadData()
                    return
                }
            }
        }
        else
        {
            makeToast(strMessage: NointernetConnection!)
        }
        
    }
    
    

}
extension PriceCAlculatorViewController: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == self.tblPriceCalculator)
        {
            return self.PriceCalculatorResponse.count
        }else{
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == self.tblPriceCalculator)
        {
            let value = self.PriceCalculatorResponse[section]
            let title = value["Title"] as? String
            if(title == "Header")
            {
                return 1
            }
            else if(title == "Madatory Charges")
            {
                let data = value["Data"] as? NSArray
                return data!.count
            }
            else if(title == "Optional Charges")
            {
                let data = value["Data"] as? NSArray
                return data!.count
            }
            else if(title == "Calculation")
            {
                let data = value["Data"] as? NSArray
                //return data!.count
                return 2
            }else{
                
                return 1
            }
        }else{
            print(NumberOfExtraTableCell)
            return NumberOfExtraTableCell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
   
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let value = self.PriceCalculatorResponse[section]
        let title = value["Title"] as? String
        if(title == "Header")
        {
             let cell = self.tblPriceCalculator.dequeueReusableCell(withIdentifier: "MendatoryChargesTableViewCell") as! MendatoryChargesTableViewCell
            cell.lblMadatoryCharges.text! = "Unit charge"
            return cell
        
        }
        else if(title == "Madatory Charges")
        {
            let cell = self.tblPriceCalculator.dequeueReusableCell(withIdentifier: "CalculationTableViewCell") as! CalculationTableViewCell
            cell.lblCalculation.text! = title!
            return cell
        }
        else if(title == "Optional Charges")
        {
            let cell = self.tblPriceCalculator.dequeueReusableCell(withIdentifier: "CalculationTableViewCell") as! CalculationTableViewCell
            cell.lblCalculation.text! = title!
            return cell
        }
        else if(title == "Calculation")
        {
            let cell = self.tblPriceCalculator.dequeueReusableCell(withIdentifier: "CalculationTableViewCell") as! CalculationTableViewCell
            cell.lblCalculation.text! = "Calculation"
            return cell
        }
        else{
            return UIView()
        }
       
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        /*if(tableView == self.tblPriceCalculator)
        {
            return 324.0
        }else{
            
            return 0
        }*/
        if(tableView == self.tblPriceCalculator)
        {
            let value = self.PriceCalculatorResponse[section]
            let title = value["Title"] as? String
            if(title == "Footer")
            {
                return 0
            }else{
                return 45.0
            }
            
        }else{
            
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        /*if(tableView == self.tblPriceCalculator)
        {
            return 339.0
        }else{
            
            return 0
        }*/
        return 1
    }
    /*func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = self.tblPriceCalculator.dequeueReusableCell(withIdentifier: "CalculatedPriceTableViewCell") as! CalculatedPriceTableViewCell
        return cell.contentView
        
    }*/
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.tblPriceCalculator)
        {
            let value = self.PriceCalculatorResponse[indexPath.section]
            let title = value["Title"] as? String
            if(title == "Header")
            {
                let cell = self.tblPriceCalculator.dequeueReusableCell(withIdentifier: "PriceHeaderTableViewCell") as! PriceHeaderTableViewCell
                if(isSales && self.BuilderName == "")
                {
                    cell.vwBuilder.isHidden = true
                }else{
                    cell.vwBuilder.isHidden = false
                }
                cell.lblBuilderNameValue.text! = self.BuilderName
                cell.lblProjectValue.text! = self.ProjectName
                let myString = self.UnitName
                var unitArray = myString.components(separatedBy: "/")
                print(self.UnitName)
                //propertyId = unitArray[1].trimmingCharacters(in: .whitespaces)
                cell.lblUnitValue.text! = unitArray[0]
                if(propertyDetail.count > 0)
                {
                    cell.lblProjectAreaValue.text! = self.propertyDetail[0].propertyArea!
                    if(self.propertyDetail[0].propertyTerraceValueTotal != nil && self.propertyDetail[0].propertyTerraceValueTotal != 0)
                    {
                        cell.VwTerraceCharge.isHidden = false
                        cell.lblTerraceChargeValue.text! = "\(getAmount(bal: self.propertyDetail[0].propertyTerraceValueTotal!))"
                    }else{
                        cell.VwTerraceCharge.isHidden = true
                    }
                    if(self.propertyDetail[0].propertyLawnValueTotal != nil && self.propertyDetail[0].propertyLawnValueTotal != 0)
                    {
                         cell.VwLawnCharge.isHidden = false
                        cell.lblLawnChargeValue.text! = "\(getAmount(bal: self.propertyDetail[0].propertyLawnValueTotal!))"
                    }else{
                        cell.VwLawnCharge.isHidden = true
                    }
                    print(self.baseRate)
                    if(self.baseRate != "" && self.baseRate != "0")
                    {
                         //cell.lblBaseRateValue.text! = self.baseRate
                        cell.lblBaseRateValue.text! = "₹ "+self.baseRate+"/\(mapping.string(forKey: "sq_ft_ket")!)"
                    }else{
                        //cell.lblBaseRateValue.text! = self.propertyDetail[0].baseRate!
                        cell.lblBaseRateValue.text! = self.baseRate
                        cell.lblBaseRateValue.text! = "₹ "+"0"+"/\(mapping.string(forKey: "sq_ft_ket")!)"
                    }
                    if(self.propertyDetail[0].propertyPossessionChargeTotal != "" && self.propertyDetail[0].propertyPossessionChargeTotal != "0" && self.propertyDetail[0].propertyPossessionChargeTotal != nil)
                    {
                        cell.lblVwPossessionChargesValue.isHidden = false
                        cell.lblVwPossessionChargesValue.text! = "\(getAmount(bal:Int(self.propertyDetail[0].propertyPossessionChargeTotal!)!))"
                    }else{
                        cell.VwPossessionCharges.isHidden = true
                    }
                    
                    cell.lblProjectAreaValue.text! = self.propertyDetail[0].propertyArea!+" \(mapping.string(forKey: "sq_ft_ket")!)"
                    if(self.propertyDetail[0].gstOnBase != "" && self.propertyDetail[0].gstOnBase != "0" && self.propertyDetail[0].gstOnBase != nil)
                    {
                         cell.vwGSTOnBase.isHidden = false
                        cell.lblGstOnBaseValue.text! = self.propertyDetail[0].gstOnBase!+" %"
                    }else{
                        cell.vwGSTOnBase.isHidden = true
                    }
                    
                    if(self.propertyDetail[0].gstOnOthers != "" && self.propertyDetail[0].gstOnOthers != "0" && self.propertyDetail[0].gstOnOthers != nil)
                    {
                        cell.lblGstOnOtherValue.text! = self.propertyDetail[0].gstOnOthers!+" %"
                    }else{
                        cell.vwGSTOnOther.isHidden = true
                    }
                    
                    if(self.isshowInputCredit)
                    {
                        if(self.gstInputCredit != "" && self.gstInputCredit != "0")
                        {
                            cell.lblGstInputCreditsValue.text! = self.gstInputCredit+" %"
                        }else{
                             cell.vwGSTInputCredit.isHidden = true
                        }
                        
                    }else{
                         cell.vwGSTInputCredit.isHidden = true
                    }
                    cell.lblRegistrationChargeValue.text! = self.propertyDetail[0].registrationCharges!
                    if(isRegistrationSelection)
                    {
                        let buttonImage = #imageLiteral(resourceName: "checkbox_checked_yellow")
                        cell.btnRegistrationSelection.setImage(buttonImage, for: .normal)
                    }else{
                        let buttonImage = #imageLiteral(resourceName: "checkbox_unchecked_yellow")
                        cell.btnRegistrationSelection.setImage(buttonImage, for: .normal)
                    }
                    
                    if(self.propertyDetail[0].stampDuty != "" && self.propertyDetail[0].stampDuty != "0" && self.propertyDetail[0].stampDuty != nil)
                    {
                         cell.lblStampDutyValue.text! = self.propertyDetail[0].stampDuty!
                    }else{
                        cell.vwStampDuty.isHidden = true
                    }
                    
                   
                }
                
                return cell
            }
            else if(title == "Madatory Charges")
            {
                let data = value["Data"] as? NSArray
                let cell = tableView.dequeueReusableCell(withIdentifier: "MandatoryValueTableViewCell") as! MandatoryValueTableViewCell
                cell.tag = indexPath.row
                cell.btnCheckUInCheck.isUserInteractionEnabled = false
                cell.btnCheckUncheck.isUserInteractionEnabled = false
                let buttonImage = #imageLiteral(resourceName: "checkbox_checked_red")
                cell.btnCheckUInCheck.setImage(buttonImage, for: .normal)
                //print("data \(data)")
                let datas = data![indexPath.row] as? NSDictionary
                var title = datas!["charge_title"] as! String
                var chargeValue = datas!["charge_value"] as?  String ?? "0"
                chargeValue = "\(getAmount(bal: Int(chargeValue)!))"
                let unit_type = datas!["unit_type"] as? String
                if(unit_type == "1")
                {
                    title = "\(title) "+"\(chargeValue) /"+" \(mapping.string(forKey: "sq_ft_ket")!)"
                }else{
                    title = "\(title) "+"\(chargeValue)"
                }
                cell.lblOption.text = title
                return cell
                
            }
            else if(title == "Optional Charges")
            {
                let data = value["Data"] as? NSArray
                let cell = tableView.dequeueReusableCell(withIdentifier: "MandatoryValueTableViewCell") as! MandatoryValueTableViewCell
                cell.tag = indexPath.row
                cell.btnCheckUInCheck.isUserInteractionEnabled = true
                cell.btnCheckUncheck.isUserInteractionEnabled = true
                //print("data \(data)")
                let datas = data![indexPath.row] as? NSDictionary
                var title = datas!["charge_title"] as! String
                var chargeValue = datas!["charge_value"] as?  String ?? "0"
                chargeValue = "\(getAmount(bal: Int(chargeValue)!))"
                let unit_type = datas!["unit_type"] as? String
                let isSelecte = datas!["isSelected"] as? Bool
                if(unit_type == "1")
                {
                    title = "\(title) "+"\(chargeValue) /"+" \(mapping.string(forKey: "sq_ft_ket")!)"
                }else{
                    title = "\(title) "+"\(chargeValue)"
                }
                cell.lblOption.text = title
                if(isSelecte)!
                {
                    let buttonImage = #imageLiteral(resourceName: "checkbox_checked_yellow")
                    cell.btnCheckUInCheck.setImage(buttonImage, for: .normal)
                }else{
                    let buttonImage = #imageLiteral(resourceName: "checkbox_unchecked_yellow")
                    cell.btnCheckUInCheck.setImage(buttonImage, for: .normal)
                }
                return cell
            }
            else if(title == "Calculation")
            {
                let data = value["Data"] as? NSArray
               // if(indexPath.row == data!.count-1)
                if(indexPath.row == 0)
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCalculationTableViewCell") as! OtherCalculationTableViewCell
                    cell.tag = indexPath.row
                    cell.CalculateTable.register(UINib(nibName: "CalculateTableViewCell", bundle: nil), forCellReuseIdentifier: "CalculateTableViewCell")
                    cell.CalculateTable.dataSource = self
                    cell.CalculateTable.delegate = self
                    /* DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                     
                     })*/
                    if(NumberOfExtraTableCell > 0)
                    {
                        cell.CalculateTable.backgroundColor = .white
                        
                        
                    }else{
                        cell.CalculateTable.backgroundColor = .clear
                    }
                    cell.CalculateTable.reloadData()
                    cell.OtherTableHeights.constant = cell.CalculateTable.contentSize.height
                    print("Cell Height : \(cell.OtherTableHeights.constant)")
                    
                    return cell
                    
                }
                /*else if(indexPath.row == data!.count-2)
                {
                    let datas = data![indexPath.row] as? NSDictionary
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FloorChoiceTableViewCell") as! FloorChoiceTableViewCell
                    cell.tag = indexPath.row
                    cell.lblFloorSelctionValue.text! = "\(datas!["floor_start"] ?? "0")" as! String
                    return cell
                    
                }*/else{
                    let datas = data![indexPath.row] as? NSDictionary
                    print("value-->\(value)")
                    print("row-->\(indexPath.row)")
                    print(datas)
                    let cell = tableView.dequeueReusableCell(withIdentifier: "FloorChoiceTableViewCell") as! FloorChoiceTableViewCell
                    if(NumberOfExtraTableCell > 0)
                    {
                        cell.FloorTopConstraint.constant = -0.3
                        
                    }else{
                        cell.FloorTopConstraint.constant = 4.0
                    }
                    if(OneTimeDDConfigure == false)
                    {
                        OneTimeDDConfigure = true
                         configDropdown(dropdown: FloorDD, sender: cell.lblFloorSelctionValue)
                        cell.lblFloorSelctionValue?.text = "\(theFloorList.first)"
                    }else{
                        if(self.selectFloor > 0)
                        {
                            cell.lblFloorSelctionValue.text! = "\(theFloorList[self.selectFloor])"
                        }else{
                            cell.lblFloorSelctionValue.text! = "\(theFloorList[0])"
                        }
                        
                    }
                    //let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountTableViewCell") as! DiscountTableViewCell
                    cell.tag = indexPath.row
                    if(isDiscountShow == true)
                    {
                        cell.viewDiscount.isHidden = false
                        cell.lblDiscount.isHidden = false
                        cell.txtDiscount.isHidden = false
                        cell.txtDiscount.delegate = self
                        cell.txtDiscount.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
                        self.textfieldIndexPath = indexPath as! IndexPath
                        print("section-",self.textfieldIndexPath!.section)
                         print("row-",self.textfieldIndexPath!.row)
                        cell.txtDiscount.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
                        
                        cell.DiscountViewHeights.constant = 40.0
                        cell.imgCheckUnCheck.image = #imageLiteral(resourceName: "checkbox_checked_yellow")
                        print(discountCurrentValue)
                        if(discountCurrentValue != "")
                        {
                            cell.txtDiscount.text = discountCurrentValue
//                            cell.txtDiscount.becomeFirstResponder()
                            
                        }else{
                            cell.txtDiscount.text = ""
//                            cell.txtDiscount.becomeFirstResponder()
                        }
                        if let types = datas!["disc_unit"]
                        {
                            let type = types as! String
                            if(type != "")
                            {
                                print(type)
                                if(type == "1")
                                {
                                    cell.lblSymbol.text = "₹"
                                }else if(type == "2")
                                {
                                    cell.lblSymbol.text = "%"
                                }else if(type == "3"){
                                    cell.lblSymbol.text = " ₹ / \(mapping.string(forKey: "sq_ft_ket")!)"
                                }else{
                                    cell.lblSymbol.text = ""
                                }
                            }else{
                                 cell.lblSymbol.text = ""
                            }
                        }else{
                            if(discountCurrentUnit == "1")
                            {
                                cell.lblSymbol.text = "₹"
                            }else if(discountCurrentUnit == "2")
                            {
                                cell.lblSymbol.text = "%"
                            }else if(discountCurrentUnit == "3"){
                                cell.lblSymbol.text = " ₹ / \(mapping.string(forKey: "sq_ft_ket")!)"
                            }else{
                                cell.lblSymbol.text = ""
                            }
                            
                        }
                        
                        
                    }else{
                        cell.viewDiscount.isHidden = true
                        cell.lblDiscount.isHidden = true
                        cell.txtDiscount.isHidden = true
                        cell.DiscountViewHeights.constant = 0.0
                        cell.imgCheckUnCheck.image = #imageLiteral(resourceName: "checkbox_unchecked_yellow")
                        if(discountCurrentValue != "")
                        {
                            cell.txtDiscount.text = discountCurrentValue
                        }else{
                            cell.txtDiscount.text = ""
                        }
                    }
                    return cell
                }
            }
            else{
                let cell = self.tblPriceCalculator.dequeueReusableCell(withIdentifier: "CalculatedPriceTableViewCell") as! CalculatedPriceTableViewCell
                
                if(self.TempChartData[0].baseCharges != nil)
                {
                     cell.lblBaseChargesValue.text! = "  \(getAmount(bal: self.TempChartData[0].baseCharges!))"
                }else{
                    cell.lblBaseChargesValue.text! = "  ₹ null"
                }
               
                if(self.TempChartData[0].gstTotal != nil)
                {
                    cell.lblGSTTotalValue.text! = "  \(getAmount(bal: self.TempChartData[0].gstTotal!))"
                }else{
                    cell.lblGSTTotalValue.text! = "  ₹ null"
                }
                if(self.chartData[0].additionalCharges != nil)
                {
                    cell.lblAdditionalChrgeValue.text! = "  \(getAmount(bal: self.chartData[0].additionalCharges!))"
                }else{
                    cell.lblAdditionalChrgeValue.text! = "  ₹ null"
                }
                
                //cell.lblStampRegistrationValue.text! = "  ₹ \(self.chartData[0].stampPlusRegistration!)"
                cell.btnViewOutlet.addTarget(self, action: #selector(btnViewAction), for: .touchUpInside)
                cell.btnShareOutlet.addTarget(self, action: #selector(btnShareAction), for: .touchUpInside)
                print()
                if(self.chartData[0].stampPlusRegistration != nil)
                {
                    
                    cell.lblStampRegistrationValue.text! = "  \(getAmount(bal: self.chartData[0].stampPlusRegistration!))"
                }else{
                    print(self.chartData[0].stampPlusRegistration as AnyObject)
                    cell.lblStampRegistrationValue.text! = "  ₹ null"
                }
                cell.lblCalculatedPrice.text = "\(converValue(val: (final_calculated_price)))"
                cell.lblFinalCalculatedPriceValue.text! = "  \(converValue(val: (final_calculated_price)))"
                return cell
            }
            
           /* if(indexPath.row == 0)
            {
                if(self.mandatoryCharges.count > 0)
                {
                    
                   let cell = tableView.dequeueReusableCell(withIdentifier: "MendatoryChargesTableViewCell") as! MendatoryChargesTableViewCell
                   cell.tag = indexPath.row
                   return cell
                    
                }else if(self.optionalCharges.count > 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "OptionalChargesTableViewCell") as! OptionalChargesTableViewCell
                    cell.tag = indexPath.row
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CalculationTableViewCell") as! CalculationTableViewCell
                    cell.tag = indexPath.row
                    return cell
                }
            }*/
            
            /*
            if(indexPath.row == 0)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MendatoryChargesTableViewCell") as! MendatoryChargesTableViewCell
                cell.tag = indexPath.row
                return cell
            }
            else if(indexPath.row == 1)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MandatoryValueTableViewCell") as! MandatoryValueTableViewCell
                cell.tag = indexPath.row
                return cell
            }
            else if(indexPath.row == 2)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OptionalChargesTableViewCell") as! OptionalChargesTableViewCell
                cell.tag = indexPath.row
                return cell
            }
            else if(indexPath.row == 3)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MandatoryValueTableViewCell") as! MandatoryValueTableViewCell
                cell.tag = indexPath.row
                return cell
            }
            else if(indexPath.row == 4)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CalculationTableViewCell") as! CalculationTableViewCell
                cell.tag = indexPath.row
                return cell
            }
            else if(indexPath.row == 5)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCalculationTableViewCell") as! OtherCalculationTableViewCell
                cell.tag = indexPath.row
                
                
                cell.CalculateTable.register(UINib(nibName: "CalculateTableViewCell", bundle: nil), forCellReuseIdentifier: "CalculateTableViewCell")
                cell.CalculateTable.dataSource = self
                cell.CalculateTable.delegate = self
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    cell.CalculateTable.reloadData()
                })
                
                cell.OtherTableHeights.constant = cell.CalculateTable.contentSize.height
                print("Cell Height : \(cell.OtherTableHeights.constant)")
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "FloorChoiceTableViewCell") as! FloorChoiceTableViewCell
                cell.tag = indexPath.row
                return cell
                
            }*/
        }else{
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalculateTableViewCell") as! CalculateTableViewCell
            cell.tag = indexPath.row
            let value = self.PriceCalculatorResponse[CalculationCurrentIndex]
            let data = value["Data"] as? NSArray
            //print(data!.count)
            //print(indexPath.row)
            let index = (data!.count-3)-indexPath.row
            let datas = data![index] as? NSDictionary
            //print(index)
            //print(datas)
            //print(NumberOfExtraTableCell)
            if(NumberOfExtraTableCell > 0)
            {
                cell.TitleView.borderColor = .darkGray
                cell.ValueView.borderColor = .darkGray
            }else{
                cell.TitleView.borderColor = .clear
                cell.ValueView.borderColor = .clear
            }
            let title = datas!["charge_title"] as! String
            var chargeValue = datas!["charge_value"] as! String
            let unit_type = datas!["unit_type"] as? String
            /*if(unit_type == "1")
            {
                
                chargeValue = "\(chargeValue)"+" ₹"
            }
            else if(unit_type == "2")
            {
                
                chargeValue = "\(chargeValue)"+" %"
            }
            else{
                chargeValue = "\(chargeValue)"+" ₹"
            }*/
            chargeValue = "₹ "+"\(chargeValue)"
            cell.OtherCalculatedTitle.text! = title
            cell.OtherCalculatedValue.text = chargeValue
            return cell
        }
        
    }
   
    
}

extension PriceCAlculatorViewController:CommonPickerDelegate
{
    func setValueOnReadValue(value: Bool, PdfLink: String) {
        print("value \(value)")
        isFormSubmit = value
        strPdfLink = PdfLink
    }
}

