//
//  DetailsVC.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 22/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var arrDetails:NSArray = NSArray()
    var firstLabel:UILabel!
    var selectedSection :Int!
    var cell:SectionCell = SectionCell()
    @IBOutlet weak var tableDetails:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedSection = -999
     self.navigationController?.isNavigationBarHidden = false
        if let navigationBar = self.navigationController?.navigationBar {
         let secondFrame = CGRect(x: 0, y: 0, width: navigationBar.frame.width, height: navigationBar.frame.height)
            firstLabel = UILabel(frame: secondFrame)
            firstLabel.text = "Inventory Details"
            firstLabel.textAlignment = NSTextAlignment.center
            firstLabel.textColor = UIColor(red:255.0/255.0, green: 192.0/255.0, blue: 0.0/255.0, alpha: 1.0)

             navigationBar.addSubview(firstLabel)
        }
        
        self.tableDetails.reloadData()
        // Do any additional setup after loading the view.
    }
    @objc func expandCell(sender:UIButton)  {
     //   let send = sender.tag
       
        if self.selectedSection == sender.tag{
            self.selectedSection = -999
          
        }else{
             self.selectedSection = sender.tag
        }
        tableDetails.reloadData()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        cell = tableView.dequeueReusableCell(withIdentifier: "SectionCell") as! SectionCell
        if(self.arrDetails.count>0){
            if let floorDict = self.arrDetails.object(at: section) as? NSDictionary{
                if  let floordata = floorDict.value(forKey: "floor_id") as? String {
                cell.lblTitle.text = "Floor: " +  floordata
                    cell.btnEnter.tag = section
                    cell.btnEnter.addTarget(self, action: #selector(self.expandCell(sender:)), for: .touchUpInside)
                    cell.btnEnter.isSelected = false
                    if (self.selectedSection == section){
                     cell.btnEnter.isSelected = !cell.btnEnter.isSelected
                    }else{
                       cell.btnEnter.isSelected = false
                    }
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 45
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
         if(self.arrDetails.count>0){
            if let dict = arrDetails[section] as? NSDictionary
            {
                if let arr = dict.value(forKey: "floor_data") as? NSArray {
                    if selectedSection == section{
                return arr.count + 1
                    }else{
                      return 0
                    }
                }
            }
        }
      return 0
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        return 45
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:DetailsCell = tableView.dequeueReusableCell(withIdentifier: "DetailsCell") as! DetailsCell
        if (indexPath.row == 0)
        {
           let cellOne:HeaderCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
            cellOne.backgroundColor = UIColor.white
            return cellOne
        }
        if(self.arrDetails.count>0){
            if let dict = arrDetails[indexPath.section] as? NSDictionary
            {
               if let arr = dict.value(forKey: "floor_data") as? NSArray {
                if let subDict = arr.object(at: indexPath.row - 1) as? NSDictionary{
                    cell.lblArea.text = subDict.value(forKey: "area") as? String
                    cell.lblFlatNo.text = subDict.value(forKey: "flat_no") as? String
                    cell.lblConfig.text = subDict.value(forKey: "config") as? String
                    cell.lblPosition.text = subDict.value(forKey: "position") as? String
                    if let status = subDict.value(forKey: "status_code") as? String {
                        if (status == "1")
                        {
                         cell.lblStatus.text = "Available"
                         cell.backgroundColor = UIColor(red:220.0/255.0, green: 238.0/255.0, blue: 214.0/255.0, alpha: 1.0)
                        }
                        if (status == "2")
                        {
                            cell.lblStatus.text = "Not Available"
                            cell.backgroundColor = UIColor(red:239.0/255.0, green: 219.0/255.0, blue: 220.0/255.0, alpha: 1.0)
                        }
                        if (status == "3")
                        {
                            cell.lblStatus.text = "On-Hold"
                            cell.backgroundColor = UIColor(red:249.0/255.0, green: 247.0/255.0, blue: 225.0/255.0, alpha: 1.0)
                        }
                        if (status == "4")
                        {
                            cell.lblStatus.text = "Not Available"
                            cell.backgroundColor = UIColor(red:239.0/255.0, green: 219.0/255.0, blue: 220.0/255.0, alpha: 1.0)
                            // cell.lblStatus.text = "Internal Hold"
                          //  cell.backgroundColor = UIColor(red:215.0/255.0, green: 234.0/255.0, blue: 246.0/255.0, alpha: 1.0)
                        }
                        
                    }
                   
                  }
                }
            }
        }
        
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
    return arrDetails.count
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//       if let navigationBar = self.navigationController?.navigationBar {
//        navigationBar.removeFromSuperview()
//        }
        firstLabel.removeFromSuperview()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
