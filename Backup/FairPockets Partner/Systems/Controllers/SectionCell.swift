//
//  SectionCell.swift
//  Fairpockets Partner
//
//  Created by Subhra Das on 22/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class SectionCell: UITableViewCell {
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var btnEnter:UIButton!
    @IBOutlet weak var img:UIImageView!
    var expandeStatus:Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
