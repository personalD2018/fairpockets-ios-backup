//
//  PDFViewController.swift
//  Builder-Broker Management
//
//  Created by DK on 04/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PDFViewController: UIViewController, UIWebViewDelegate,NVActivityIndicatorViewable  {

    @IBOutlet var pdfWebView: UIWebView!
    
    var urls = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationbarwithBackButton(titleText: mapping.string(forKey: "ViewPDF_key"))
        let url : URL! = URL(string: urls)
        pdfWebView.loadRequest(URLRequest(url: url))
        pdfWebView.isOpaque = false
        pdfWebView.backgroundColor = UIColor.clear
        pdfWebView.scalesPageToFit = true;
        pdfWebView.delegate = self
        startAnimating(Loadersize, message: lblLoading, type: NVActivityIndicatorType(rawValue:LoaderType))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func webViewDidFinishLoad(_ webView : UIWebView) {
        //Page is loaded do what you want
         self.stopAnimating()
    }
    
  
}
