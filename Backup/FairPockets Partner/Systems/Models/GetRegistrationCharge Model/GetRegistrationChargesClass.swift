//
//  GetRegistrationChargesClass.swift
//
//  Created by AB on 09/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GetRegistrationChargesClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let finalCalculatedPrice = "Final Calculated Price"
    static let message = "message"
    static let discountArray = "discountArray"
    static let baseRate = "base_rate"
    static let code = "code"
    static let chartData = "chart_data"
    static let propertyDetail = "property_detail"
  }

  // MARK: Properties
  public var finalCalculatedPrice: Float?
  public var message: String?
  public var discountArray: DiscountArray?
  public var baseRate: String?
  public var code: Int?
  public var chartData: ChartData?
  public var propertyDetail: PropertyDetail?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    finalCalculatedPrice <- map[SerializationKeys.finalCalculatedPrice]
    message <- map[SerializationKeys.message]
    discountArray <- map[SerializationKeys.discountArray]
    baseRate <- map[SerializationKeys.baseRate]
    code <- map[SerializationKeys.code]
    chartData <- map[SerializationKeys.chartData]
    propertyDetail <- map[SerializationKeys.propertyDetail]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = finalCalculatedPrice { dictionary[SerializationKeys.finalCalculatedPrice] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = discountArray { dictionary[SerializationKeys.discountArray] = value.dictionaryRepresentation() }
    if let value = baseRate { dictionary[SerializationKeys.baseRate] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = chartData { dictionary[SerializationKeys.chartData] = value.dictionaryRepresentation() }
    if let value = propertyDetail { dictionary[SerializationKeys.propertyDetail] = value.dictionaryRepresentation() }
    return dictionary
  }

}
