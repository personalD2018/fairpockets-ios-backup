//
//  ClientHistorydata.swift
//
//  Created by AB on 04/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ClientHistorydata: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let reminderDate = "reminderDate"
    static let name = "name"
    static let id = "id"
    static let created = "created"
    static let projectId = "project_id"
    static let pdfLink = "pdf_link"
    static let reminderText = "reminderText"
    static let clientId = "client_id"
    static let userId = "user_id"
    static let remarks = "remarks"
    //static let status = "status"
  }

  // MARK: Properties
  public var reminderDate: String?
  public var name: String?
  public var id: String?
  public var created: String?
  public var projectId: String?
  public var pdfLink: String?
  public var reminderText: String?
  public var clientId: String?
  public var userId: String?
  public var remarks: String?
//  public var status: String?
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    reminderDate <- map[SerializationKeys.reminderDate]
    name <- map[SerializationKeys.name]
    id <- map[SerializationKeys.id]
    created <- map[SerializationKeys.created]
    projectId <- map[SerializationKeys.projectId]
    pdfLink <- map[SerializationKeys.pdfLink]
    reminderText <- map[SerializationKeys.reminderText]
    clientId <- map[SerializationKeys.clientId]
    userId <- map[SerializationKeys.userId]
    remarks <- map[SerializationKeys.remarks]
   // status <- map[SerializationKeys.status]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = reminderDate { dictionary[SerializationKeys.reminderDate] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = projectId { dictionary[SerializationKeys.projectId] = value }
    if let value = pdfLink { dictionary[SerializationKeys.pdfLink] = value }
    if let value = reminderText { dictionary[SerializationKeys.reminderText] = value }
    if let value = clientId { dictionary[SerializationKeys.clientId] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = remarks { dictionary[SerializationKeys.remarks] = value }
   // if let value = status { dictionary[SerializationKeys.status] = value }
    return dictionary
  }

}
