//
//  Bulderdata.swift
//
//  Created by AB on 03/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Bulderdata: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let builderName = "builder_name"
    static let builderId = "builder_id"
  }

  // MARK: Properties
  public var builderName: String?
  public var builderId: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    builderName <- map[SerializationKeys.builderName]
    builderId <- map[SerializationKeys.builderId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = builderName { dictionary[SerializationKeys.builderName] = value }
    if let value = builderId { dictionary[SerializationKeys.builderId] = value }
    return dictionary
  }

}
