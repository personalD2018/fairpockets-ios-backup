//
//  GetBuilderListClass.swift
//
//  Created by AB on 03/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GetBuilderListClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let message = "message"
    static let code = "code"
    static let bulderdata = "data"
  }

  // MARK: Properties
  public var message: String?
  public var code: Int?
  public var bulderdata: [Bulderdata]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    message <- map[SerializationKeys.message]
    code <- map[SerializationKeys.code]
    bulderdata <- map[SerializationKeys.bulderdata]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = bulderdata { dictionary[SerializationKeys.bulderdata] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
