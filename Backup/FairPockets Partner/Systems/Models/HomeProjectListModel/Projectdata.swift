//
//  Projectdata.swift
//
//  Created by AB on 03/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Projectdata: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let projectName = "project_name"
    static let projectId = "project_id"
  }

  // MARK: Properties
  public var projectName: String?
  public var projectId: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    projectName <- map[SerializationKeys.projectName]
    projectId <- map[SerializationKeys.projectId]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = projectName { dictionary[SerializationKeys.projectName] = value }
    if let value = projectId { dictionary[SerializationKeys.projectId] = value }
    return dictionary
  }

}
