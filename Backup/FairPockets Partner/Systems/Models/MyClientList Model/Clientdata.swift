//
//  Clientdata.swift
//
//  Created by AB on 04/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Clientdata: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let reminderDate = "reminderDate"
    static let status = "status"
    static let name = "name"
    static let email = "email"
    static let id = "id"
    static let mobile = "mobile"
    static let created = "created"
    static let reminderText = "reminderText"
    static let userId = "user_id"
    static let remarks = "remarks"
  }

  // MARK: Properties
  public var reminderDate: String?
  public var status: String?
  public var name: String?
  public var email: String?
  public var id: String?
  public var mobile: String?
  public var created: String?
  public var reminderText: String?
  public var userId: String?
  public var remarks: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    reminderDate <- map[SerializationKeys.reminderDate]
    status <- map[SerializationKeys.status]
    name <- map[SerializationKeys.name]
    email <- map[SerializationKeys.email]
    id <- map[SerializationKeys.id]
    mobile <- map[SerializationKeys.mobile]
    created <- map[SerializationKeys.created]
    reminderText <- map[SerializationKeys.reminderText]
    userId <- map[SerializationKeys.userId]
    remarks <- map[SerializationKeys.remarks]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = reminderDate { dictionary[SerializationKeys.reminderDate] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = mobile { dictionary[SerializationKeys.mobile] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = reminderText { dictionary[SerializationKeys.reminderText] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = remarks { dictionary[SerializationKeys.remarks] = value }
    return dictionary
  }

}
