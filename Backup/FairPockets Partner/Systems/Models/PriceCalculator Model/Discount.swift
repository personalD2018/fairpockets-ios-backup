//
//  Discount.swift
//
//  Created by AB on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Discount: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let discAmount = "disc_amount"
    static let discUnit = "disc_unit"
  }

  // MARK: Properties
  public var discAmount: String?
  public var discUnit: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    discAmount <- map[SerializationKeys.discAmount]
    discUnit <- map[SerializationKeys.discUnit]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = discAmount { dictionary[SerializationKeys.discAmount] = value }
    if let value = discUnit { dictionary[SerializationKeys.discUnit] = value }
    return dictionary
  }

}
