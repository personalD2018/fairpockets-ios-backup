//
//  FloorChoice.swift
//
//  Created by AB on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class FloorChoice: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let endloop = "endloop"
    static let startloop = "startloop"
  }

  // MARK: Properties
  public var endloop: String?
  public var startloop: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    endloop <- map[SerializationKeys.endloop]
    startloop <- map[SerializationKeys.startloop]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = endloop { dictionary[SerializationKeys.endloop] = value }
    if let value = startloop { dictionary[SerializationKeys.startloop] = value }
    return dictionary
  }

}
