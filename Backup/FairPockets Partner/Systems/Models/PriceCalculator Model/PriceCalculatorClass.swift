//
//  PriceCalculatorClass.swift
//
//  Created by AB on 07/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class PriceCalculatorClass: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let gstInputCredit = "gstInputCredit"
    static let code = "code"
    static let floorChoice = "floor_choice"
    static let discount = "discount"
    static let showInputCredit = "showInputCredit"
    static let finalCalculatedPrice = "final_calculated_price"
    static let showDiscount = "showDiscount"
    static let message = "message"
    static let optionalCharges = "optional_charges"
    static let chartData = "chart_data"
    static let propertyDetail = "property_detail"
    static let mandatoryCharges = "mandatory_charges"
  }

  // MARK: Properties
  public var gstInputCredit: String?
  public var code: Int?
  public var floorChoice: FloorChoice?
  public var discount: Discount?
  public var showInputCredit: String?
  public var finalCalculatedPrice: Double?
  public var showDiscount: String?
  public var message: String?
  public var optionalCharges: [OptionalCharges]?
  public var chartData: ChartData?
  public var propertyDetail: PropertyDetail?
  public var mandatoryCharges: [MandatoryCharges]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    gstInputCredit <- map[SerializationKeys.gstInputCredit]
    code <- map[SerializationKeys.code]
    floorChoice <- map[SerializationKeys.floorChoice]
    discount <- map[SerializationKeys.discount]
    showInputCredit <- map[SerializationKeys.showInputCredit]
    finalCalculatedPrice <- map[SerializationKeys.finalCalculatedPrice]
    showDiscount <- map[SerializationKeys.showDiscount]
    message <- map[SerializationKeys.message]
    optionalCharges <- map[SerializationKeys.optionalCharges]
    chartData <- map[SerializationKeys.chartData]
    propertyDetail <- map[SerializationKeys.propertyDetail]
    mandatoryCharges <- map[SerializationKeys.mandatoryCharges]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = gstInputCredit { dictionary[SerializationKeys.gstInputCredit] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = floorChoice { dictionary[SerializationKeys.floorChoice] = value.dictionaryRepresentation() }
    if let value = discount { dictionary[SerializationKeys.discount] = value.dictionaryRepresentation() }
    if let value = showInputCredit { dictionary[SerializationKeys.showInputCredit] = value }
    if let value = finalCalculatedPrice { dictionary[SerializationKeys.finalCalculatedPrice] = value }
    if let value = showDiscount { dictionary[SerializationKeys.showDiscount] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = optionalCharges { dictionary[SerializationKeys.optionalCharges] = value.map { $0.dictionaryRepresentation() } }
    if let value = chartData { dictionary[SerializationKeys.chartData] = value.dictionaryRepresentation() }
    if let value = propertyDetail { dictionary[SerializationKeys.propertyDetail] = value.dictionaryRepresentation() }
    if let value = mandatoryCharges { dictionary[SerializationKeys.mandatoryCharges] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
