//
//  ImageGalleryCell.swift
//  Fairpockets Partner
//
//  Created by Pritam Dey on 27/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class ImageGalleryCell: UICollectionViewCell {
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var projectImageView: UIImageView!
    
}
