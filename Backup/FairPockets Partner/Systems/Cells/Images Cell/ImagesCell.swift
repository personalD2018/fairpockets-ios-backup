//
//  ImagesCell.swift
//  Fairpockets Partner
//
//  Created by Pritam Dey on 26/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON

class ImagesCell: UITableViewCell {
    @IBOutlet weak var lblCompanyName:UILabel!
    @IBOutlet weak var lblCompanyAddress:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func coffigureCell(myDate:NSDictionary, isVideo:Bool = false)
    {
        let json = JSON(myDate)
        lblCompanyName.text = json["builder_name"].stringValue
        lblCompanyAddress.text = json["project_name"].stringValue
        lblDate.text = json["UpdatedDate"].stringValue
        if isVideo {
            lblDate.text = json["created"].stringValue
        }        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
