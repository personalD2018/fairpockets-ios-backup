//
//  MandatoryValueTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 05/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class MandatoryValueTableViewCell: UITableViewCell {

    @IBOutlet var btnCheckUInCheck: UIButton!
    @IBOutlet var lblOption: UILabel!
    
    @IBOutlet var btnCheckUncheck: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnAddRemoveCharges(_ sender: Any) {
        if  let PriceCAlculatorVC =  self.viewNextPresentingViewController() as? PriceCAlculatorViewController
        {
            
            PriceCAlculatorVC.AddRemoveOptioanlCharges(index:self.tag)
            
        }
    }
}
