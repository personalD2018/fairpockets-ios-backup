//
//  FloorChoiceTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 07/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class FloorChoiceTableViewCell: UITableViewCell {

    @IBOutlet var lblFloorChoice: UILabel!
    @IBOutlet var lblFloorSelctionValue: UILabel!
    @IBOutlet var viewOther: UIView!
    @IBOutlet var imgCheckUnCheck: UIImageView!
    @IBOutlet var lblOther: UILabel!
    @IBOutlet var viewDiscount: UIView!
    
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var txtDiscount: UITextField!
    @IBOutlet var DiscountViewHeights: NSLayoutConstraint!
    
    @IBOutlet var lblSymbol: UILabel!
    
    @IBOutlet var FloorTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         lblFloorChoice.text = mapping.string(forKey: "FloorChoice_key")
         lblDiscount.text = mapping.string(forKey: "Discount_key")
         lblOther.text = mapping.string(forKey: "Other_key")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnOpenCloseDiscountAction(_ sender: Any) {
        
        if  let PriceCAlculatorVC =  self.viewNextPresentingViewController() as? PriceCAlculatorViewController
        {
            /*if PriceCAlculatorVC.isDiscountShow
             {
             PriceCAlculatorVC.isDiscountShow = false
             imgCheckUnCheck.image = #imageLiteral(resourceName: "ic_home_sidemenu")
             DiscountViewHeights.constant = 0.0
             }else{
             PriceCAlculatorVC.isDiscountShow = true
             imgCheckUnCheck.image = #imageLiteral(resourceName: "ic_my_client_sidemenu")
             DiscountViewHeights.constant = 40.0
             }*/
            PriceCAlculatorVC.HideShowDiscount()
            
        }
        
    }
    
    @IBAction func btnOpenCloseDropDown(_ sender: UIButton) {
        if  let PriceCAlculatorVC =  self.viewNextPresentingViewController() as? PriceCAlculatorViewController
        {
            PriceCAlculatorVC.ShowDropDown()
            
        }
    }
}
