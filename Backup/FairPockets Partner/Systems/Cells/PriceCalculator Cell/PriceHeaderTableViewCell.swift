//
//  PriceHeaderTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 05/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class PriceHeaderTableViewCell: UITableViewCell {
    @IBOutlet var lblUnitCharges: UILabel!
    
    
    
    @IBOutlet weak var vwBuilder: UIView!
    @IBOutlet var lblBuilder: UILabel!
    @IBOutlet var lblBuilderNameValue: UILabel!
    
    @IBOutlet weak var vwProject: UIView!
    @IBOutlet var lblProject: UILabel!
    @IBOutlet var lblProjectValue: UILabel!
    
    @IBOutlet weak var vwUnit: UIView!
    @IBOutlet var lblUnit: UILabel!
    @IBOutlet var lblUnitValue: UILabel!
    
    @IBOutlet weak var vwPropertyArea: UIView!
    @IBOutlet var lblProjectArea: UILabel!
    @IBOutlet var lblProjectAreaValue: UILabel!
    
    @IBOutlet var VwTerraceCharge: UIView!
    @IBOutlet var lblTerraceCharge: UILabel!
    @IBOutlet var lblTerraceChargeValue: UILabel!
    
    @IBOutlet var VwLawnCharge: UIView!
    @IBOutlet var lblLawnCharge: UILabel!
    @IBOutlet var lblLawnChargeValue: UILabel!
    
    
    @IBOutlet weak var vwBaseRate: UIView!
    @IBOutlet var lblBaseRate: UILabel!
    @IBOutlet var lblBaseRateValue: UILabel!
    
    @IBOutlet var VwPossessionCharges: UIView!
    @IBOutlet var lblVwPossessionCharges: UILabel!
    @IBOutlet var lblVwPossessionChargesValue: UILabel!
    
    
    @IBOutlet weak var vwGSTOnBase: UIView!
    @IBOutlet var lblGstOnBase: UILabel!
    @IBOutlet var lblGstOnBaseValue: UILabel!
    
    @IBOutlet weak var vwGSTOnOther: UIView!
    @IBOutlet var lblGstOnOther: UILabel!
    @IBOutlet var lblGstOnOtherValue: UILabel!
    
    @IBOutlet weak var vwGSTInputCredit: UIView!
    @IBOutlet var lblGstInputCredits: UILabel!
    @IBOutlet var lblGstInputCreditsValue: UILabel!
    
    
    @IBOutlet weak var vwRegistrationCharge: UIView!
    @IBOutlet var lblRegistrationCharge: UILabel!
    @IBOutlet var lblRegistrationChargeValue: UILabel!
    
    @IBOutlet var btnRegistrationSelection: UIButton!
    
    @IBOutlet weak var vwStampDuty: UIView!
    @IBOutlet var lblStampDuty: UILabel!
    @IBOutlet var lblStampDutyValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblBuilder.text = mapping.string(forKey: "Builder_key")
        lblProject.text = mapping.string(forKey: "Project_key")
        lblUnit.text = mapping.string(forKey: "Unit_key")
        lblProjectArea.text = mapping.string(forKey: "PropertyArea_key")
        lblTerraceCharge.text = mapping.string(forKey: "TerraceCharge_key")
        lblLawnCharge.text = mapping.string(forKey: "LawnCharge_key")
        lblBaseRate.text = mapping.string(forKey: "BaseRate_key")
        lblVwPossessionCharges.text = mapping.string(forKey: "PossessionCharges_key")
        lblGstOnBase.text = mapping.string(forKey: "GSTOnBase_key")
        lblGstOnOther.text = mapping.string(forKey: "GSTOnOthers_key")
        lblGstInputCredits.text = mapping.string(forKey: "GSTInputCredit_key")
        lblRegistrationCharge.text = mapping.string(forKey: "RegistrationCharges_key")
        lblStampDuty.text = mapping.string(forKey: "StampDuty_key")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - registartion IB Action
    
    @IBAction func btnRegistrationCheckMarkTapped(_ sender: UIButton) {
        if  let PriceCAlculatorVC =  self.viewNextPresentingViewController() as? PriceCAlculatorViewController
        {
            PriceCAlculatorVC.AddRemoveRegistrationCharges()
            
        }
    }
    
}
