//
//  CalculationTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 05/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class CalculationTableViewCell: UITableViewCell {
    @IBOutlet var lblCalculation: UILabel!    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblCalculation.text = mapping.string(forKey: "Calculation_key")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
