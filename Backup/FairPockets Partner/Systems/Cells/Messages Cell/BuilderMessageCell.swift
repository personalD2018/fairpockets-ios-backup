//
//  BuilderMessageCell.swift
//  Fairpockets Partner
//
//  Created by Pritam Dey on 26/07/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON

class BuilderMessageCell: UITableViewCell {
    @IBOutlet weak var lblBuilderName:UILabel!
    @IBOutlet weak var lblBuilderMessage:UILabel!
    
    @IBOutlet weak var lblProjectName:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblReadMore:UILabel!
    @IBOutlet weak var vwBackgrd:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func coffigureBuilderMessageCell(myDate:NSDictionary)
    {
        let json = JSON(myDate)
        lblBuilderName.text = json["builder_name"].stringValue
        lblProjectName.text = json["project_name"].stringValue
        lblBuilderMessage.text = json["message"].stringValue
        lblDate.text = json["created"].stringValue
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
