//
//  ClientListTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 04/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class ClientListTableViewCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblMobileNumber: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet weak var vwBackgrd:UIView!
    
    typealias _handler = () -> Void

    var callHandler:_handler?
    var deleteHandler: _handler?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(theModel:Clientdata)
    {
        lblName.text! = theModel.name!
        lblMobileNumber.text! = theModel.mobile!
        lblEmail.text! = theModel.email!
    }
    func registerHandler(forCall callHandler:@escaping _handler, forDelete deleteHandler:@escaping _handler) {
        self.callHandler = callHandler;
        self.deleteHandler = deleteHandler;
    }
    
    @IBAction func call(_ sender: Any) {
        if let handler = self.callHandler {
            handler();
        }
    }
    
    @IBAction func deleteRemark(_ sender: Any) {
        if let handler = self.deleteHandler {
            handler();
        }
    }
}
