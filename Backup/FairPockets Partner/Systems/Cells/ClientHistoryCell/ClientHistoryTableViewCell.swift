//
//  ClientHistoryTableViewCell.swift
//  Builder-Broker Management
//
//  Created by DK on 04/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class ClientHistoryTableViewCell: UITableViewCell {

    @IBOutlet var lblRemarkTitle: UILabel!
    @IBOutlet var lblRemainderDate: UILabel!
    @IBOutlet var lblRemainderText: UILabel!
    typealias _handler = () -> Void
    var deleteHandler: _handler?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(theModel:ClientHistorydata) {
        let remark = mapping.string(forKey: "Remark_key")
        let date = mapping.string(forKey: "ReminderDate_key")
        let time = mapping.string(forKey: "ReminderText_key")
        lblRemarkTitle.text! = remark!+" : "+theModel.remarks!
        lblRemainderDate.text! = date!+" : "+theModel.reminderDate!
        lblRemainderText.text! = time!+" : "+theModel.reminderText!
    }
    
    func registerHandler(forDelete deleteHandler:@escaping _handler) {
        self.deleteHandler = deleteHandler;
    }
    
    @IBAction func btnOpenPDFAction(_ sender: Any) {
       
        if self.viewNextPresentingViewController() != nil {
            
            if  let ClientHistoryVC =  self.viewNextPresentingViewController() as? ClientHistoryViewController {
                ClientHistoryVC.OpenPdfView(index: self.tag)
                
            }
        }
    }
    
    @IBAction func btnShareAction(_ sender: Any) {
        if self.viewNextPresentingViewController() != nil {
            if  let ClientHistoryVC =  self.viewNextPresentingViewController() as? ClientHistoryViewController {
                ClientHistoryVC.shareSocialMedia(index: self.tag)
                
            }
        }
    }
    
    @IBAction func handleDelete(_ sender: Any) {
        if let dltHndlr = self.deleteHandler {
            dltHndlr();
        }
    }
}
