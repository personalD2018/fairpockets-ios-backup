//
//  Constant.swift
//  PizzyBites
//
//  Created by Abhay on 03/06/17.
//  Copyright © 2017 Abhay. All rights reserved.
//

import Foundation
import UIKit

let Screen = UIScreen.main.bounds
var ScreenHight = Screen.height
var ScreenWidth = Screen.width
var ScreenX = Screen.origin.x
var ScreenY = Screen.origin.y

import UIKit


extension UIColor {
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

let kDefaultPrimaryColor = UIColor(rgb: 0x29333D)
let kDefaultViewBackgroundColor = UIColor(rgb: 0x29333D)
let kDefaultTextColor = UIColor(rgb: 0x0083bf)
let kDefaultDarkGrayColor = UIColor(rgb: 0xAAAAAA)
let kDefaultWhiteColor = UIColor(rgb: 0xFFFFFF)
let kDefaultBlackColor = UIColor(rgb: 0x000000)
let kDefaultYellowColor = UIColor(rgb: 0xFFC000)
let kDefaultDropDownColor = UIColor(rgb: 0xE0E0E0)
let kDefaultRedColor = UIColor(rgb: 0xD62540)

let kDefaultUnreadCellBGColor = UIColor(rgb: 0xE0E0E0);

let kDefaultSeparationLineColorPrimary = UIColor(rgb: 0xAAAAAA)
let kDefaultSeparationViewLineColorPrimary = UIColor(rgb: 0xdcdbdb)
let kDefaultSidemenuSelectedColor = UIColor(rgb: 0xD6EEF8)

let HelveticaNeue = "HelveticaNeue"
let kDefaultFontHelveticaNeueBold = "HelveticaNeue-Bold"
let kDefaultFontHelveticaNeueMedium = "HelveticaNeue-Medium"
let kDefaultFontHelveticaNeueThin = "HelveticaNeue-Thin"

let kDefaultFontAvenirLTStdBook = "AvenirLTStd-Book"
let kDefaultFontAvenirLTStdMedium = "AvenirLTStd-Medium"
let kDefaultFontAvenirLTStdLight = "AvenirLTStd-Light"
let kDefaultFontAvenirLTStdHeavy = "AvenirLTStd-Heavy"

let kDefaultFontRobotoCondensedRegular = "RobotoCondensed-Regular"
let kDefaultFontRobotoCondensedBold = "RobotoCondensed-Bold"


let kDefaultFontLatoBlack = "Lato-Black"
let kDefaultFontLatoBlackItalic = "Lato-BlackItalic"
let kDefaultFontLatoBold = "Lato-Bold"
let kDefaultFontLatoBoldItalic = "Lato-BoldItalic"
let kDefaultFontLatoHairLine = "Lato-Hairline"
let kDefaultFontLatoHairLineItalic = "Lato-HairlineItalic"
let kDefaultFontLatoItalic = "Lato-Italic"
let kDefaultFontLatoLight = "Lato-Light"
let kDefaultFontLatoLightItalic = "Lato-LightItalic"
let kDefaultFontLatoRegular = "Lato-Regular"


let kDefaultFontSizeVerySmall8=8.0
let kDefaultFontSizeVerySmall=10.0
let kDefaultFontSizeSmall11=11.0
let kDefaultFontSizeSmall=12.0
let kDefaultFontSizeMedium13=13.0
let kDefaultFontSizeMedium=14.0
let kDefaultFontSizeLarge15=15.0
let kDefaultFontSizeLarge=16.0
let kDefaultFontSizeExtraLarge=18.0
let kDefaultFontSizeExtraLarge20=20.0
let kDefaultFontSizeExtraLarge22=22.0
let kDefaultFontSizeExtraLarge25=25.0
let kDefaultFontSizeExtraLarge28=28.0
let kDefaultFontSizeExtraLarge30=30.0
let kDefaultFontSizeExtraLarge32=32.0
let kDefaultFontSizeExtraLarge36=36.0
let kDefaultFontSizeExtraLarge40=40.0
let kDefaultFontSizeExtraLarge150=150.0
let base = "http://www.fairpockets.com/"
let baseUserUrl = "http://www.fairpockets.com/servicesv1/"
let baseInvUrl = "https://www.fairpockets.com/servicesv1/"
let kPlaceholderImage = "placeholder_section_screen_small"
let userdefualts = UserDefaults.standard
var language = "english"
var lang = 1
var kBaseUserLang = "en"
let MainStoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
var dicoLocalisation = NSDictionary()
var LoaderType:Int = 29
let Loadersize = CGSize(width: 30, height: 30)
//var strLoader = localizedStringForKey(key: "loading")
var strLoader = "Loading"
var aBasePER_PAGE_IN_ITEM = 10
var UserIsLogin = false
var ScrenHeight = UIScreen.main.bounds.height
var ScrenWidth = UIScreen.main.bounds.width

let AppuserName = "Builder-Broker-Management"
let AppPassword = ""
var DeviceToken = UIDevice.current.identifierForVendor!.uuidString
let appDelegate = UIApplication.shared.delegate as? AppDelegate



let somethingWentWrong = mapping.string(forKey: "Something_went_wrong_key")
let NointernetConnection = mapping.string(forKey: "No_internet_connection_please_try_again_later_key")
let lblLoading = mapping.string(forKey: "Loading_key")
let somethingWentWrongTryAgain = mapping.string(forKey: "Something_went_wrong_try_again_key")


//#MARK:- Google constants
let TOKEN_NAME:String = "notification_token"
let TOKEN_ID:String = (userdefualts.object(forKey: TOKEN_NAME) == nil) ? "failed" : userdefualts.object(forKey: TOKEN_NAME)! as! String
var UserType = ""
var UserId = ""
var Builder_id = ""
var isSales = false

//#MARK:- APIName List

let aBasePostLoginURl = baseInvUrl+"validateMobile"
let aBasePostVerifyOTPURl = baseInvUrl+"verifyOtp"

let aBasePostBuilderListURl = baseUserUrl+"getBuildersByUserId"
let aBasePostProjectListURl = baseUserUrl+"getProjectsByUserId"
let aBasePostUnitListURl = baseUserUrl+"getUnitsByProjectId"
let aBasePostClientListURl = baseUserUrl+"getClientDataByUserId"
let aBasePostClientListDeleteURl = baseUserUrl+"deleteClientInClientList"
let aBasePostClientHistoryURl = baseUserUrl+"getClientDataByUserIdClientId"
let aBasePostClientHistoryRemarksDeleteURl = baseUserUrl+"deleteClientInClientHistoryList"
let aBasePostNotificationURl = baseUserUrl+"getReminderDataByUserId"
let aBasePostPriceCalculatorURl = baseUserUrl+"getCalculatorByProjectId"
let aBasePostSaveReminderURl = baseInvUrl+"postCalculatorDataSubmit"
let aBasePostGetByRegistrationURl = baseUserUrl+"getCalculatorDataByRegistration"
let aBasePostGetCalculatorDataURl = baseUserUrl+"getCalculatorData"
let aBasePostGetFloorDataURl = baseUserUrl+"getCalculatorDataByFloor"
let aBasePostGetDiscountDataURl = baseUserUrl+"getCalculatorDataByDiscount"
let aBasePostInvBuilderListURl = baseInvUrl+"getBuildersByUserId"
let aBasePostInvProjectListURl = baseInvUrl+"getprojectdropdownInv"
let aBasePostInvTowerListURl = baseInvUrl+"getTowerDropdownByUserIdInv"
let aBasePostInvWingListURl = baseInvUrl+"getWingDropdownByUserIdInv"
let aBasePostInvDetailsURl = baseInvUrl+"getInvDetails"
let aBasePostPriceListssURl = baseInvUrl+"getPriceListByUserId"
let aBasePostBrocureListsURl = baseInvUrl+"getBrocureListByUserId"
let aBasePostImagesListsURl = baseInvUrl+"getImagesListByUserId"
let aBasePostBuilderMessageListsURl = baseInvUrl+"getBuilderMessage"

let aBasePostprojectImagesURl = baseInvUrl+"getImagesListByProjectId"
let aBasePostMyClientNotification = baseInvUrl+"getClientNotificationsView"
let aBasePostMessageNotification = baseInvUrl+"getBuilderMessageNotificationsView"
let aBasePostMessageNotificationRead = baseInvUrl+"getBuilderMessageNotificationsRead1"
let aBasePostMessageRowRead = baseInvUrl+"builderMessageRowRead"
let aBasePostMyClientRowRead = baseInvUrl+"clientNotificationRowRead"

let aBasePostVideosListsURl = baseInvUrl+"getVideosListByUserId"

//new Scope
//let aBasePostVerifyOtpURl = baseInvUrl+"verifyOtp"
//let aBasePostValidateMobileURl = baseInvUrl+"validateMobile"
let aBasePostUserExistingStatusURl = baseInvUrl+"getUserExistingStatus"










