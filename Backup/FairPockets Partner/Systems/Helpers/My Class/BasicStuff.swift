//
//  BasicStuff.swift
//  HappyLiving
//
//  Created by Haresh on 11/20/17.
//  Copyright © 2017 Haresh. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toast_Swift
//import GoogleMobileAds
//import Firebase


//MARK : Store data





//MARK: - Language set up

let mapping:StringMapping = StringMapping.shared()

let Swedish = "sw"
let English = "en"

var isEnglish:Bool = true
var sideMenuObj = SideMenuViewController()
func setLangauge(language:String)
{
    
    if language == Swedish
    {
        isEnglish = false
        lang = 1
        userdefualts.set(Swedish, forKey: "language")
        userdefualts.set(1, forKey: "lang")
        userdefualts.synchronize()
    }
    else
    {
        isEnglish = true
        lang = 0
        userdefualts.set(English, forKey: "language")
        userdefualts.set(0, forKey: "lang")
        userdefualts.synchronize()
        
    }
    //print("Language - ",userdefualts.value(forKey: "language"));
    
    StringMapping.shared().setLanguage()
}




//#MARK:- Alert
func setAlert(msg:String)
{
    
    let alert = UIAlertView()
    alert.title = mapping.string(forKey: "BuilderBrokerMngmt_key")
    alert.message = msg
    alert.addButton(withTitle:mapping.string(forKey: "Ok_key"))
    alert.show()
}

extension UIViewController {
    func showAlert(message: String?) {
        let alert = UIAlertController(title: mapping.string(forKey: "BuilderBrokerMngmt_key"), message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: mapping.string(forKey: "Ok_key"), style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
           // self.navigationController?.popViewController(animated: true)
        }
       // let action = UIAlertAction(title: mapping.string(forKey: "Ok_key"), style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    func showAlertWithBack(message: String?) {
        let alert = UIAlertController(title: mapping.string(forKey: "BuilderBrokerMngmt_key"), message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: mapping.string(forKey: "Ok_key"), style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
             self.navigationController?.popViewController(animated: true)
        }
        // let action = UIAlertAction(title: mapping.string(forKey: "Ok_key"), style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

extension UIViewController
{
    
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    func setupNavigationBar(titleText:String)
    {        
        
        let revealButtonItem = UIBarButtonItem(image: UIImage(named: "ic_menu_header_white"), style: .plain, target: self, action: #selector(MenuOpenAction(sender:)))
        revealButtonItem.tintColor = .white
        self.navigationItem.leftBarButtonItem = revealButtonItem
        
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = kDefaultYellowColor
        HeaderView.font = UIFont(name: kDefaultFontRobotoCondensedRegular, size: CGFloat(16))

        self.navigationItem.titleView = HeaderView
 
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = kDefaultYellowColor
        self.navigationController?.navigationBar.barTintColor = kDefaultPrimaryColor
       
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name: kDefaultFontRobotoCondensedBold, size: CGFloat(kDefaultFontSizeLarge))!]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = kDefaultPrimaryColor
        }
    }
    func setupNavigationbarwithBackButtonAndMenuButton(titleText:String)
    {
        let revealButtonItem = UIBarButtonItem(image: UIImage(named: "ic_menu_header_white"), style: .plain, target: self, action: #selector(MenuOpenAction(sender:)))
        revealButtonItem.tintColor = .white
        self.navigationItem.leftBarButtonItem = revealButtonItem
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .plain, target: self, action: #selector(back))
        rightButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = kDefaultYellowColor
        HeaderView.font = UIFont(name: kDefaultFontRobotoCondensedRegular, size: CGFloat(16))
        
        self.navigationItem.titleView = HeaderView
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = kDefaultYellowColor
        self.navigationController?.navigationBar.barTintColor = kDefaultPrimaryColor
        
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name: kDefaultFontRobotoCondensedBold, size: CGFloat(kDefaultFontSizeLarge))!]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = kDefaultPrimaryColor
        }
    }
    func setupNavigationbarwithBackButton(titleText:String)
    {
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .plain, target: self, action: #selector(backAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton

        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = kDefaultYellowColor
        HeaderView.font = UIFont(name: kDefaultFontRobotoCondensedRegular, size: CGFloat(16))
        
        self.navigationItem.titleView = HeaderView
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = kDefaultYellowColor
        self.navigationController?.navigationBar.barTintColor = kDefaultPrimaryColor
        
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name: kDefaultFontRobotoCondensedBold, size: CGFloat(kDefaultFontSizeLarge))!]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = kDefaultPrimaryColor
        }
       
    }
    
    @objc func MenuOpenAction(sender:UIButton)
    {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
        sideMenuObj.viewWillAppear(true)
     
    }
    @objc func back()
    {
       
        let HomeVC = MainStoryBoard.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
        let navigationController = NavigationController(rootViewController: HomeVC)
        
        let mainViewController = MainViewController()
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: UInt(1))
        
        navigationController.isNavigationBarHidden = true
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
//        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
//        let nav = UINavigationController(rootViewController: homeViewController)
//        nav.navigationBar.isTranslucent = false
//        nav.navigationBar.tintColor = kDefaultYellowColor
//        nav.navigationBar.barTintColor = kDefaultPrimaryColor
//
//
//        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name: kDefaultFontRobotoCondensedBold, size: CGFloat(kDefaultFontSizeLarge))!]
//        nav.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        nav.navigationBar.shadowImage = UIImage()
//          nav.isNavigationBarHidden = true
//        appdelegate.window!.rootViewController = nav
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func backAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
  /*  func resizeImage(image:UIImage, targetSize:CGSize) -> UIImage? {
        
        let originalSize = image.size
        
        let widthRatio = targetSize.width / originalSize.width
        let heightRatio = targetSize.heb ight / originalSize.height
        
        let ratio = min(widthRatio, heightRatio)
        
        let newSize = CGSize(width: originalSize.width * ratio, height: originalSize.height * ratio)
        
        // preparing rect for new image size
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    */
    
    
    func setupNavigationBarWithBgGrediant()  {
        
//        var colors = [UIColor]()
//        
//        colors.append(UIColor(red: 47/255, green: 180/255, blue: 75/255, alpha: 1))
//        colors.append(UIColor(red: 74/255, green: 245/255, blue: 63/255, alpha: 1))
        
        /*self.navigationController?.navigationBar.setGradientBackground()
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 4.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.masksToBounds = false*/
        
        /*
         self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "bg_header_gradiant"), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.isTranslucent = true
         self.navigationController?.view.backgroundColor = .clear
         */
    }
   

    func RemoveAllLogoutData()
    {
        lang = 0
        DeviceToken = ""
        selectedSideMenuIndex = 0
        UserType = ""
        UserId = ""
        Builder_id = ""
        userdefualts.removeObject(forKey: "LoginData")
        
    }
    
   
    
    //MARK: - Valid Email
    
    func isValidEmail(emailAddressString:String) -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    
    //MARK: - Toaster Method

    func makeToast(strMessage : String?) {
        if let message = strMessage {
            self.view.makeToast(message, duration: Delay.long, position: .bottom)
        }
    }
    func makeToastWithBack(strMessage : String){
        
//        Toast(text: strMessage, delay: 0.0, duration: Delay.long).show()
        self.view.makeToast(strMessage, duration: Delay.long, position: .bottom)
        //showAlertWithBack(message: strMessage)
        
    }
    
    
    //MARK:- Date Formatter
    
    func stringTodate(Formatter:String,strDate:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        // dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //   dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
    func stringTodate(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        let date: Date = dateFormatterGet.date(from: strDate)!
        return dateFormatterPrint.string(from:date)
    }
    
    func strTodt(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        return dateFormatterPrint.date(from: strDate)!
    }
    
    func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let strdate: String = dateFormatterGet.string(from: Date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        let date: Date = dateFormatterPrint.date(from: strdate)!
        return dateFormatterPrint.string(from: date)
    }
    
     func getUserDetail(_ forKey: String) -> String
     {
        guard let userDetail = UserDefaults.standard.value(forKey: "UserDetails") as? Data else { return "" }
        let data = JSON(userDetail)
        return data[forKey].stringValue
     }
     
    func JSONtoString(arrJSON: [JSON]) -> String
    {
        let paramsString = JSON(arrJSON).rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
        return paramsString
    }
    
    
}


extension UIImage {
    
    //  MARK:-  convert the given UIView into a UIImage
    class func imageWithView(view:UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}
