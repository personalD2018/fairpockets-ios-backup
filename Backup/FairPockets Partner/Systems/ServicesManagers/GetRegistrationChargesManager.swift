//
//  GetRegistrationChargesManager.swift
//  Builder-Broker Management
//
//  Created by DK on 09/05/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import ObjectMapper


typealias GetRegistrationChargesResponse = (_ search: GetRegistrationChargesClass?) -> ()

/**
 *  This struct helps in Product relates CRUD operations. Mainly interacts with Network Resources.
 */
struct  GetRegistrationChargesManager {
    
    /**
     Lists products from Server DB.
     
     - parameter completion: returns `ProductResponse` which is having products' array if success otherwise `nil`
     */
    func getRegistrationCharges(json:String,completion: @escaping GetRegistrationChargesResponse)
    {
        let URLstr = "\(aBasePostGetByRegistrationURl)"
        print(URLstr)
        let url = URL(string: URLstr)!
        let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        Alamofire.request(request)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/html"])
            .responseSwiftyJSON {
                (response) in
                
                print(response)
                guard
                    let responseJSON = response.result.value,
                    let result = responseJSON.rawString()
                    
                    else {
                        print(response.result.error as Any)
                        completion(nil)
                        return
                }
                print(result)
                
                guard result=="null" else{
                    guard
                        let resultData = Mapper<GetRegistrationChargesClass>().map(JSONString: result)
                        //let resultData = Mapper<GetProductDisplayClass>().mapArray(JSONString: result)
                        else{
                            print(result as Any)
                            completion(nil)
                            return
                    }
                    
                    //  Maps JSON Array of products into Product Object's Array
                    //print(resultData)
                    completion(resultData)
                    return
                }
        }
        
        
    }
    
    func getCalculateData(json:String,completion: @escaping GetRegistrationChargesResponse)
    {
        let URLstr = "\(aBasePostGetCalculatorDataURl)"
        print(URLstr)
        let url = URL(string: URLstr)!
        let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        Alamofire.request(request)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/html"])
            .responseSwiftyJSON {
                (response) in
                
                print(response)
                guard
                    let responseJSON = response.result.value,
                    let result = responseJSON.rawString()
                    
                    else {
                        print(response.result.error as Any)
                        completion(nil)
                        return
                }
                print(result)
                
                guard result=="null" else{
                    guard
                        let resultData = Mapper<GetRegistrationChargesClass>().map(JSONString: result)
                        //let resultData = Mapper<GetProductDisplayClass>().mapArray(JSONString: result)
                        else{
                            print(result as Any)
                            completion(nil)
                            return
                    }
                    
                    //  Maps JSON Array of products into Product Object's Array
                    //print(resultData)
                    completion(resultData)
                    return
                }
        }
        
        
    }
    func getFloorWishData(json:String,completion: @escaping GetRegistrationChargesResponse)
    {
        let URLstr = "\(aBasePostGetFloorDataURl)"
        print(URLstr)
        let url = URL(string: URLstr)!
        let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        Alamofire.request(request)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/html"])
            .responseSwiftyJSON {
                (response) in
                
                print(response)
                guard
                    let responseJSON = response.result.value,
                    let result = responseJSON.rawString()
                    
                    else {
                        print(response.result.error as Any)
                        completion(nil)
                        return
                }
                print(result)
                
                guard result=="null" else{
                    guard
                        let resultData = Mapper<GetRegistrationChargesClass>().map(JSONString: result)
                        //let resultData = Mapper<GetProductDisplayClass>().mapArray(JSONString: result)
                        else{
                            print(result as Any)
                            completion(nil)
                            return
                    }
                    
                    //  Maps JSON Array of products into Product Object's Array
                    //print(resultData)
                    completion(resultData)
                    return
                }
        }
        
        
    }
    func getDiscountData(json:String,completion: @escaping GetRegistrationChargesResponse)
    {
        let URLstr = "\(aBasePostGetDiscountDataURl)"
        print(URLstr)
        let url = URL(string: URLstr)!
        let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        Alamofire.request(request)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["text/html"])
            .responseSwiftyJSON {
                (response) in
                
                print(response)
                guard
                    let responseJSON = response.result.value,
                    let result = responseJSON.rawString()
                    
                    else {
                        print(response.result.error as Any)
                        completion(nil)
                        return
                }
                print(result)
                
                guard result=="null" else{
                    guard
                        let resultData = Mapper<GetRegistrationChargesClass>().map(JSONString: result)
                        //let resultData = Mapper<GetProductDisplayClass>().mapArray(JSONString: result)
                        else{
                            print(result as Any)
                            completion(nil)
                            return
                    }
                    
                    //  Maps JSON Array of products into Product Object's Array
                    //print(resultData)
                    completion(resultData)
                    return
                }
        }
        
        
    }
    
    private func authHeader() -> [String : String] {
        return [
            // "X-Authorization": "\(kBaseApiKey)",
            //"Content-Type": "application/x-www-form-urlencoded",
            "Content-Type": "application/x-www-form-urlencoded",
            //"Accept-Language":"\(language)"
        ]
    }
    
}
